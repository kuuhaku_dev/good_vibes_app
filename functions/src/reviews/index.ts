import { db } from '../config';
import * as functions from 'firebase-functions';

export interface Review {
	uid: string;
	ownerUid: string;
	targetUid: string;
	productType: string;
	createdAt: Date;
	lastModified: Date;
	rating: number;
	title: string;
	text: string;
}

export const calculateAverageRating = functions.firestore
	.document('reviews/{reviewId}')
	.onWrite(async (change, context) => {
		// Get an object with the current document value.
		// If the document does not exist, it has been deleted.
		const document = change.after.exists ? change.after.data() : null;

		// Get an object with the previous document value (for update or delete)
		const oldDocument = change.before.exists ? change.before.data() : null;

		if (oldDocument === null) {
			return updateAverageRating(document as Review, 'create');
		} else if (document === null) {
			return updateAverageRating(oldDocument as Review, 'delete');
		} else {
			return updateAverageRating(document as Review, 'update', oldDocument as Review);
		}
	});

const updateAverageRating = async (
	review: Review,
	type: 'create' | 'update' | 'delete',
	oldReview?: Review,
): Promise<any> => {
	// retrieve target
	const targetDoc = await db.doc(`${review.productType}/${review.targetUid}`).get();
	const target = targetDoc.data();

	// get current target stats data
	const stats = target && target['stats'];
	const reviewsCount = (stats && Number(stats['reviewsCount'])) || 0;
	const totalRating = (stats && Number(stats['totalRating'])) || 0;
	const starsArr = Array(5).fill(0).map((v, i) => (stats && Number(stats[`star${i + 1}`])) || 0);

	switch (type) {
		case 'create':
			return targetDoc.ref.set(
				{
					stats: {
						reviewsCount: reviewsCount + 1,
						totalRating: totalRating + review.rating,
						averageRating: (totalRating + review.rating) / (reviewsCount + 1),
						[`star${review.rating}`]: starsArr[review.rating - 1] + 1,
					},
				},
				{ merge: true },
			);

		case 'delete':
			return targetDoc.ref.set(
				{
					stats: {
						reviewsCount: reviewsCount - 1,
						totalRating: totalRating - review.rating,
						averageRating: (totalRating - review.rating) / (reviewsCount - 1 || 1),
						[`star${review.rating}`]: starsArr[review.rating - 1] - 1,
					},
				},
				{ merge: true },
			);

		case 'update':
			if (!oldReview) return;
			return targetDoc.ref.set(
				{
					stats: {
						totalRating: totalRating - oldReview.rating + review.rating,
						averageRating: (totalRating - oldReview.rating + review.rating) / reviewsCount,
						[`star${oldReview.rating}`]: starsArr[oldReview.rating - 1] - 1,
						[`star${review.rating}`]: starsArr[review.rating - 1] + 1,
					},
				},
				{ merge: true },
			);
	}
};

export const storeLast5Reviews = functions.firestore.document('reviews/{reviewId}').onWrite(async (change, context) => {
	// Get an object with the current document value.
	// If the document does not exist, it has been deleted.
	const document = change.after.exists ? change.after.data() : null;

	// Get an object with the previous document value (for update or delete)
	const oldDocument = change.before.exists ? change.before.data() : null;

	if (oldDocument === null) {
		return updateLast5Reviews(document as Review, 'create');
	} else if (document === null) {
		return updateLast5Reviews(oldDocument as Review, 'delete');
	} else {
		return updateLast5Reviews(document as Review, 'update');
	}
});

const updateLast5Reviews = async (review: Review, type: 'create' | 'update' | 'delete'): Promise<any> => {
	// retrieve owner
	const ownerDoc = await db.doc(`users/${review.ownerUid}`).get();
	const owner = ownerDoc.data();

	// retrieve target
	const targetDoc = await db.doc(`${review.productType}/${review.targetUid}`).get();
	const target = targetDoc.data();

	const last5Reviews: Array<any> = (target && target['last5Reviews']) || [];

	switch (type) {
		case 'create':
			last5Reviews.unshift({
				uid: review.uid,
				rating: review.rating,
				createdAt: review.createdAt,
				title: review.title,
				text: review.text,
				name: owner && owner.name,
			});
			break;
		case 'delete':
			last5Reviews.splice(last5Reviews.findIndex((v) => v['uid'] === review.uid) || 0, 1);
			break;
		case 'update':
			const index = last5Reviews.findIndex((v) => v['uid'] === review.uid);
			last5Reviews.splice(index, 1, {
				uid: review.uid,
				rating: review.rating,
				createdAt: review.createdAt,
				title: review.title,
				text: review.text,
				name: owner && owner.name,
			});
			break;
	}

	if (last5Reviews.length >= 5) {
		last5Reviews.pop();
	}

	return targetDoc.ref.set({ last5Reviews: last5Reviews }, { merge: true });
};
