// Initialize Firebase Admin
import * as admin from 'firebase-admin';
admin.initializeApp();

// Initialize Firebase Cloud Messaging
export const fcm = admin.messaging();

// Initialize Cloud Firestore Database
export const db = admin.firestore();
export const Timestamp = admin.firestore.Timestamp;
const settings = { timestampsInSnapshots: true };
db.settings(settings);

// Initialize Storage
export const storage = admin.storage();
