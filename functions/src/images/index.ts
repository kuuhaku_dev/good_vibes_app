import * as functions from 'firebase-functions';
import * as fs from 'fs-extra';
import { tmpdir } from 'os';
import { dirname, join } from 'path';
import * as sharp from 'sharp';
import { storage } from '../config';
import { db } from './../config';

const BUCKET_NAME = 'quantico-uni.appspot.com';
const BASE_DOWNLOAD_URL = [ 'https://storage.googleapis.com', BUCKET_NAME ].join('/');
const HERO_PREFIX = 'main-';
const EXTRA_PREFIX = 'extra-';

export interface Event {
	uid: string;
	title: string;
	description: string;
	address: string;
	startDate: Date;
	endDate: string;
	latitude: number;
	longitude: number;
	workingTimes: string[];
	heroImage: string;
	images: string[];
	url: string;
	phone: string;
	email: string;
	rating: string;
	ownerUid: string;
}

export interface Service {
	uid: string;
}

export const saveExtraImageUrlToFirestore = functions.storage.object().onFinalize(async (object) => {
	const filePath = object.name || '';
	const fileName = filePath.split('/').pop() || '';
	const bucketDir = dirname(filePath); //images/events/{ID}

	const pathParts = bucketDir.split('/'); // [ 'images', 'events', {ID}, {fileName}]
	const productType = pathParts[0] === 'images' && pathParts[1];

	const productId = pathParts.pop() || ''; // only the ID

	// 1 - Check for filename prefix
	if (!fileName.startsWith(EXTRA_PREFIX)) {
		return false;
	}

	// 2 - get entity
	const snapshot = await db.doc(`${productType}/${productId}`).get();
	const doc = snapshot.data();
	let extraImages = doc && doc['extraImages'];
	extraImages = {
		...extraImages,
		[filePath]: true,
	};

	// 3 - Save extraImageUrl to firestore
	return await snapshot.ref.set({ extraImages: extraImages }, { merge: true });
});

export const saveHeroUrlToFirestore = functions.storage.object().onFinalize(async (object) => {
	const filePath = object.name || '';
	const fileName = filePath.split('/').pop() || '';
	const bucketDir = dirname(filePath); //images/events/{ID}

	const pathParts = bucketDir.split('/'); // [ 'images', 'events', {ID}, {fileName}]
	const productType = pathParts[0] === 'images' && pathParts[1];

	const productId = pathParts.pop() || ''; // only the ID

	// 1 - Check for filename prefix
	if (!fileName.startsWith(HERO_PREFIX)) {
		return false;
	}

	// 2 - set documentRef from product
	const docRef = db.doc(`${productType}/${productId}`);

	// 3 - Save heroUrl to firestore
	return await docRef.set({ heroImage: [ BASE_DOWNLOAD_URL, filePath ].join('/') }, { merge: true });
});

export const generateThumbs = functions.storage.object().onFinalize(async (object) => {
	const bucket = storage.bucket(object.bucket);
	const filePath = object.name || '';
	const fileName = filePath.split('/').pop() || '';
	const bucketDir = dirname(filePath);

	const workingDir = join(tmpdir(), 'thumbs');
	const tmpFilePath = join(workingDir, 'source.png');

	if (fileName.includes('thumb@') || !(object.contentType || '').includes('image')) {
		console.log('Exiting function');
		return false;
	}

	// 1. Ensure thumbnail dir exists
	await fs.ensureDir(workingDir);

	// 2. Download source file
	await bucket.file(filePath).download({
		destination: tmpFilePath,
	});
	// 3. Resize the images and define an array of upload promises
	const sizes = [ 64, 1024 ];

	const uploadPromises = sizes.map(async (size) => {
		const thumbName = `thumb@${size}_${fileName}`;
		const thumbPath = join(workingDir, thumbName);

		// Resize source image
		await sharp(tmpFilePath).resize(size, size).toFile(thumbPath);

		// Upload to GCS
		return bucket.upload(thumbPath, {
			destination: join(bucketDir, thumbName),
		});
	});

	// 4. Run the upload operations
	await Promise.all(uploadPromises);

	// 5. Cleanup remove the tmp/thumbs from the filesystem
	return fs.remove(workingDir);
});

export const deleteEventFiles = functions.firestore.document('events/{eventId}').onDelete(async (snapshot) => {
	const event: Event = snapshot.data() as Event;

	const bucket = storage.bucket(BUCKET_NAME);
	return bucket.deleteFiles({ prefix: `images/events/${event.uid}` });
});

export const deleteServiceFiles = functions.firestore.document('services/{serviceId}').onDelete(async (snapshot) => {
	const service: Service = snapshot.data() as Service;

	const bucket = storage.bucket(BUCKET_NAME);
	return bucket.deleteFiles({ prefix: `images/services/${service.uid}` });
});
