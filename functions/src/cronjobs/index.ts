import { db, Timestamp } from '../config';
import * as functions from 'firebase-functions';

// This will run everyday at 0:00
export const disablePastEvents = functions.pubsub.schedule('0 0 * * *').onRun(async (context) => {
	// Consistent timestamp
	const now = Timestamp.now();

	// Query all documents ready to perform
	const query = db.collection('events').where('startDate', '<=', now).where('isEnabled', '==', true);

	const events = await query.get();

	// Calls to execute concurrently
	const jobs: Promise<any>[] = [];

	events.forEach((snapshot) => {
		const job = snapshot.ref.update({ isEnabled: false });

		jobs.push(job);
	});

	// Execute all jobs concurrently
	return await Promise.all(jobs);
});
