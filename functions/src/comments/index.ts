import { db } from '../config';
import * as functions from 'firebase-functions';

export interface Comment {
	uid: string;
	ownerUid: string;
	targetUid: string;
	productType: string;
	createdAt: Date;
	lastModified: Date;
	title: string;
	text: string;
}

export const storeLast5Comments = functions.firestore
	.document('comments/{commentId}')
	.onWrite(async (change, context) => {
		// Get an object with the current document value.
		// If the document does not exist, it has been deleted.
		const document = change.after.exists ? change.after.data() : null;

		// Get an object with the previous document value (for update or delete)
		const oldDocument = change.before.exists ? change.before.data() : null;

		if (oldDocument === null) {
			return updateLast5Comments(document as Comment, 'create');
		} else if (document === null) {
			return updateLast5Comments(oldDocument as Comment, 'delete');
		} else {
			return updateLast5Comments(document as Comment, 'update');
		}
	});

const updateLast5Comments = async (comment: Comment, type: 'create' | 'update' | 'delete'): Promise<any> => {
	// retrieve owner
	const ownerDoc = await db.doc(`users/${comment.ownerUid}`).get();
	const owner = ownerDoc.data();

	// retrieve target
	const targetDoc = await db.doc(`${comment.productType}/${comment.targetUid}`).get();
	const target = targetDoc.data();

	const last5Comments: Array<any> = (target && target['last5Comments']) || [];

	switch (type) {
		case 'create':
			last5Comments.unshift({
				uid: comment.uid,
				createdAt: comment.createdAt,
				title: comment.title,
				text: comment.text,
				name: owner && owner.name,
			});
			break;
		case 'delete':
			last5Comments.splice(last5Comments.findIndex((v) => v['uid'] === comment.uid) || 0, 1);
			break;
		case 'update':
			const index = last5Comments.findIndex((v) => v['uid'] === comment.uid);
			last5Comments.splice(index, 1, {
				uid: comment.uid,
				createdAt: comment.createdAt,
				title: comment.title,
				text: comment.text,
				name: owner && owner.name,
			});
			break;
	}

	if (last5Comments.length >= 5) {
		last5Comments.pop();
	}

	return targetDoc.ref.set({ last5Comments: last5Comments }, { merge: true });
};
