import { fcm } from '../config';
import * as functions from 'firebase-functions';
import { messaging } from 'firebase-admin';

interface News {
	uid: string;
	title: string;
	content: string;
	imageUrl: string;
	isEnabled: boolean;
	createdAt: Date;
	updatedAt: Date;
	type: string;
	subtype: string;
}

export const notifyNewContent = functions.firestore.document('news/{newsId}').onWrite(async (change, context) => {
	// Get an object with the current document value.
	// If the document does not exist, it has been deleted.
	const document = change.after.exists ? change.after.data() : null;

	// Get an object with the previous document value (for update or delete)
	const oldDocument = change.before.exists ? change.before.data() : null;

	const news: News = document as News;
	const oldNews: News = oldDocument as News;

	if (oldNews !== null && news !== null && !oldNews.isEnabled && news.isEnabled) {
		const payload: messaging.MessagingPayload = {
			notification: {
				title: 'Novo conteúdo!',
				body: news.title,
				clickAction: 'FLUTTER_NOTIFICATION_CLICK',
			},
			data: {
				newsId: change.after.id,
			},
		};

		return fcm
			.sendToTopic('general', payload)
			.then(() => console.log(`Sent notification (topic general) - ${news.title}`));
	}

	return null;
});
