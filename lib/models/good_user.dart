import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

class GoodUser {
  static const String COL_PATH = "users";

  String uid;
  DateTime lastActivity; // maybe change this to datetime
  String name;
  String email;
  String photoUrl;
  String phone;
  String address;
  bool receiveNotifications;

  GoodUser({
    this.uid,
    this.lastActivity,
    this.name,
    this.email,
    this.photoUrl,
    this.phone,
    this.address,
    this.receiveNotifications = true,
  });

  factory GoodUser.fromMap(Map data) {
    if (data == null) {
      return GoodUser();
    }

    return GoodUser(
      uid: data["uid"] ?? '',
      lastActivity: data['lastActivity'] != null ? (data['lastActivity'] as Timestamp).toDate() : null,
      name: data["name"] ?? '',
      email: data["email"] ?? '',
      photoUrl: data["photoUrl"] ?? '',
      phone: data["phone"] ?? '',
      address: data["address"] ?? '',
      receiveNotifications: data["receiveNotifications"] ?? true,
    );
  }

  factory GoodUser.fromFirebaseUser(FirebaseUser firebaseUser) {
    if (firebaseUser == null) {
      return GoodUser();
    }

    return GoodUser(
      uid: firebaseUser.uid,
      name: firebaseUser.displayName ?? '',
      email: firebaseUser.email ?? '',
      photoUrl: firebaseUser.photoUrl ?? '',
      phone: firebaseUser.phoneNumber ?? '',
    );
  }

  Map<String, dynamic> toMap() {
    return {
      "uid": uid,
      "lastActivity": lastActivity,
      "name": name,
      "email": email,
      "photoUrl": photoUrl,
      "phone": phone,
      "address": address,
      "receiveNotifications": receiveNotifications,
    };
  }
}
