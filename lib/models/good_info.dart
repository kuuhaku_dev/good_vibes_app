import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:good_vibes_app/interfaces/interfaces.dart';
import 'package:good_vibes_app/models/models.dart';

class GoodInfo with ICommentable {
  static const String COL_PATH = 'news';

  String uid;
  String title;
  String content;
  String imageUrl;
  DateTime createdAt;
  DateTime updatedAt;
  String type;
  String subtype;

  // ICommentable
  List<GoodComment> last5Comments;

  GoodInfo({
    this.uid,
    this.title,
    this.content,
    this.imageUrl,
    this.createdAt,
    this.updatedAt,
    this.type,
    this.subtype = 'Geral',
    this.last5Comments,
  });

  factory GoodInfo.fromMap(Map data) {
    if (data == null) {
      return GoodInfo();
    }

    return GoodInfo(
      uid: data['uid'] ?? '',
      title: data['title'] ?? '',
      content: data['content'] ?? '',
      imageUrl: data['imageUrl'] ?? '',
      createdAt: data['createdAt'] != null ? (data['createdAt'] as Timestamp).toDate() : null,
      updatedAt: data['updatedAt'] != null ? (data['updatedAt'] as Timestamp).toDate() : null,
      type: data['type'] ?? '',
      subtype: data['subtype'] ?? '',
      last5Comments: (data['last5Comments'] as List ?? []).map((v) => GoodComment.fromMap(v)).toList(),
    );
  }
}

class GoodInfoType extends ITypeable {
  GoodInfoType({
    CategoryType type = CategoryType.NEWS,
    String title,
    dynamic position,
  }) : super(type: type, title: title, position: position);

  factory GoodInfoType.fromMap(Map data) {
    if (data == null) return GoodInfoType();
    return GoodInfoType(
      title: data['title'] ?? '',
      position: data['position'] ?? 0,
      type: CategoryType.values
          .firstWhere((v) => v.toString() == "CategoryType.${data['type']}", orElse: () => CategoryType.NEWS),
    );
  }
}
