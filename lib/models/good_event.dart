import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:good_vibes_app/interfaces/interfaces.dart';

class GoodEvent with IDescriptionable, IEnableable, IInformationable, IImageable, ILocationable {
  static const String COL_PATH = "events";

  String uid;
  DateTime endDate;
  List<String> workingTimes;
  double rating;
  String ownerUid;
  String type;

  // IEnableable
  bool isEnabled;

  // IImageable
  String heroImage;
  List<String> extraImages;

  // IDescriptionable
  String title;
  String description;
  String address;
  DateTime startDate;

  // IInformationable
  String url;
  String phone;
  String email;

  // ILocationable
  double latitude;
  double longitude;

  GoodEvent({
    this.uid,
    this.isEnabled = false,
    this.title,
    this.description,
    this.address,
    this.startDate,
    this.endDate,
    this.latitude,
    this.longitude,
    this.workingTimes,
    this.heroImage,
    this.extraImages,
    this.url,
    this.phone,
    this.email,
    this.rating,
    this.ownerUid,
    this.type,
  });

  factory GoodEvent.fromMap(Map data) {
    return GoodEvent(
      uid: data["uid"] ?? '',
      isEnabled: data["isEnabled"] ?? false,
      title: data["title"] ?? '',
      description: data["description"] ?? '',
      address: data["address"] ?? '',
      latitude: data["latitude"] ?? null,
      longitude: data["longitude"] ?? null,
      startDate: data['startDate'] != null ? (data['startDate'] as Timestamp).toDate() : null,
      endDate: data['endDate'] != null ? (data['endDate'] as Timestamp).toDate() : null,
      workingTimes: (data['workingTimes'] as List ?? []).map((v) => v as String).toList(),
      heroImage: data["heroImage"] ?? '',
      extraImages: (data['extraImages'] as Map ?? {}).keys.map((v) => v as String).toList(),
      url: data["url"] ?? '',
      phone: data["phone"] ?? '',
      email: data["email"] ?? '',
      rating: data["rating"] ?? 0.0,
      ownerUid: data["ownerUid"] ?? '',
      type: data["type"] ?? '',
    );
  }

  Map toMap() {
    return {
      "uid": uid,
      "isEnabled": isEnabled,
      "title": title,
      "description": description,
      "heroImage": heroImage,
      "address": address,
      "latitude": latitude,
      "longitude": longitude,
      "startDate": startDate,
      "endDate": endDate,
      "workingTimes": workingTimes,
      "url": url,
      "phone": phone,
      "email": email,
      "rating": rating,
      "ownerUid": ownerUid,
      "type": type,
    };
  }
}

class GoodEventType extends ITypeable {
  GoodEventType({
    CategoryType type = CategoryType.EVENT,
    String title,
    dynamic position,
  }) : super(type: type, title: title, position: position);

  factory GoodEventType.fromMap(Map data) {
    if (data == null) return GoodEventType();
    return GoodEventType(
      title: data['title'] ?? '',
      position: data['position'] ?? 0,
      type: CategoryType.values
          .firstWhere((v) => v.toString() == "CategoryType.${data['type']}", orElse: () => CategoryType.EVENT),
    );
  }
}
