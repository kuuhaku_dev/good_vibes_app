import 'package:cloud_firestore/cloud_firestore.dart';

class GoodReviewSummary {
  DateTime createdAt;
  String name;
  double rating;
  String text;
  String title;
  String targetUid;
  String ownerUid;
  String uid;
  String productType;

  GoodReviewSummary({
    this.createdAt,
    this.name,
    this.rating,
    this.text,
    this.title,
    this.targetUid,
    this.ownerUid,
    this.uid,
    this.productType,
  });

  factory GoodReviewSummary.fromMap(Map data) {
    if (data == null) {
      return GoodReviewSummary();
    }

    return GoodReviewSummary(
      createdAt: data['createdAt'] != null ? (data['createdAt'] as Timestamp).toDate() : null,
      name: data['name'] ?? '',
      rating: data['rating'].toDouble() ?? -1,
      title: data['title'] ?? '',
      text: data['text'] ?? '',
    );
  }

  factory GoodReviewSummary.fromReview(Map data) {
    if (data == null) {
      return GoodReviewSummary();
    }

    return GoodReviewSummary(
      uid: data['uid'] ?? '',
      ownerUid: data['ownerUid'] ?? '',
      name: data['name'] ?? '',
      targetUid: data['targetUid'] ?? '',
      productType: data['productType'] ?? '',
      createdAt: data['createdAt'],
      rating: data['rating'] ?? -1,
      title: data['title'] ?? '',
      text: data['text'] ?? '',
    );
  }
}

class GoodReview {
  static const String COL_PATH = 'reviews';
  static const String TYPE_EVENT = 'events';
  static const String TYPE_PRODUCT = 'news';
  static const String TYPE_SERVICE = 'services';

  String uid;
  String ownerUid;
  String name;
  String targetUid; // Service or event uid
  String productType;
  DateTime createdAt;
  DateTime lastModified;

  double rating;
  String title;
  String text;

  GoodReview({
    this.uid,
    this.ownerUid,
    this.name,
    this.targetUid,
    this.productType,
    this.createdAt,
    this.lastModified,
    this.rating,
    this.title,
    this.text,
  });

  factory GoodReview.fromMap(Map data) {
    if (data == null) {
      return GoodReview();
    }

    return GoodReview(
      uid: data['uid'] ?? '',
      ownerUid: data['ownerUid'] ?? '',
      name: data['name'] ?? '',
      targetUid: data['targetUid'] ?? '',
      productType: data['productType'] ?? '',
      createdAt: data['createdAt'] != null ? (data['createdAt'] as Timestamp).toDate() : null,
      lastModified: data['lastModified'] != null ? (data['lastModified'] as Timestamp).toDate() : null,
      rating: data['rating'] ?? -1,
      title: data['title'] ?? '',
      text: data['text'] ?? '',
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'uid': uid,
      'ownerUid': ownerUid,
      'name': name,
      'targetUid': targetUid,
      'productType': productType,
      'createdAt': createdAt,
      'lastModified': lastModified,
      'rating': rating,
      'title': title,
      'text': text,
    };
  }
}
