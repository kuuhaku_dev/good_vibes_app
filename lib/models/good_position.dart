import 'package:good_vibes_app/interfaces/interfaces.dart';

class GoodPosition with ILocationable {
  final double latitude;
  final double longitude;

  GoodPosition({this.latitude, this.longitude});
}
