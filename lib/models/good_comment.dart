import 'package:cloud_firestore/cloud_firestore.dart';

class GoodComment {
  static const String COL_PATH = 'comments';
  static const String TYPE_EVENT = 'events';
  static const String TYPE_BLOG = 'news';
  static const String TYPE_SERVICE = 'services';

  String uid;
  String ownerUid;
  String name;
  String targetUid; // Service or event uid
  String productType;
  DateTime createdAt;
  DateTime lastModified;

  String title;
  String text;

  GoodComment({
    this.uid,
    this.ownerUid,
    this.name,
    this.targetUid,
    this.productType,
    this.createdAt,
    this.lastModified,
    this.title,
    this.text,
  });

  factory GoodComment.fromMap(Map data) {
    if (data == null) {
      return GoodComment();
    }

    return GoodComment(
      uid: data['uid'] ?? '',
      ownerUid: data['ownerUid'] ?? '',
      name: data['name'] ?? '',
      targetUid: data['targetUid'] ?? '',
      productType: data['productType'] ?? '',
      createdAt: data['createdAt'] != null ? (data['createdAt'] as Timestamp).toDate() : null,
      lastModified: data['lastModified'] != null ? (data['lastModified'] as Timestamp).toDate() : null,
      title: data['title'] ?? '',
      text: data['text'] ?? '',
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'uid': uid,
      'ownerUid': ownerUid,
      'name': name,
      'targetUid': targetUid,
      'productType': productType,
      'createdAt': createdAt,
      'lastModified': lastModified,
      'title': title,
      'text': text,
    };
  }
}
