import 'package:cloud_firestore/cloud_firestore.dart';

class GoodActivity {
  static const String COL_PATH = "good_activity";
  static const String TYPE_CREATION = "CREATION";
  static const String TYPE_EDIT = "EDIT";
  static const String TYPE_DONATE = "DONATE";
  static const String TYPE_RATE = "RATE";

  String uid;
  String ownerUid;
  DateTime createdAt;
  String description;
  String type;

  GoodActivity({
    this.uid,
    this.ownerUid,
    this.createdAt,
    this.description,
    this.type,
  });

  factory GoodActivity.fromMap(Map data) {
    if (data == null) {
      return GoodActivity();
    }

    return GoodActivity(
      uid: data["uid"] ?? '',
      ownerUid: data["ownerUid"] ?? '',
      createdAt: data['createdAt'] != null ? (data['createdAt'] as Timestamp).toDate() : null,
      description: data["description"] ?? '',
      type: data["type"] ?? '',
    );
  }

  Map<String, dynamic> toMap() {
    return {
      "uid": uid,
      "ownerUid": ownerUid,
      "createdAt": createdAt,
      "description": description,
      "type": type,
    };
  }
}
