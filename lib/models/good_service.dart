import 'package:good_vibes_app/interfaces/interfaces.dart';
import 'package:good_vibes_app/models/good_review.dart';

class GoodService with IDescriptionable, IEnableable, ILocationable, IImageable, IInformationable, IReviewable {
  static const String COL_PATH = "services";

  String uid;
  List<String> workingTimes;
  String type;
  String ownerUid;

  // IEnableable
  bool isEnabled;

  // IImageable
  String heroImage;
  List<String> extraImages;

  // IDescriptionable
  String title;
  String description;
  String address;
  DateTime startDate;

  // IInformationable
  String url;
  String phone;
  String email;

  // IReviewable
  Map<String, dynamic> stats = STATS_INITIAL_MAP;
  List<GoodReviewSummary> last5Reviews;

  // ILocationable
  double latitude;
  double longitude;

  GoodService({
    this.uid,
    this.isEnabled = false,
    this.title,
    this.description,
    this.heroImage,
    this.address,
    this.latitude,
    this.longitude,
    this.workingTimes,
    this.url,
    this.phone,
    this.email,
    this.type,
    this.stats,
    this.last5Reviews,
    this.ownerUid,
    this.extraImages,
  });

  factory GoodService.fromMap(Map data) {
    return GoodService(
      uid: data["uid"] ?? '',
      isEnabled: data["isEnabled"] ?? false,
      title: data["title"] ?? '',
      description: data["description"] ?? '',
      heroImage: data["heroImage"] ?? '',
      address: data["address"] ?? '',
      latitude: data["latitude"] ?? null,
      longitude: data["longitude"] ?? null,
      workingTimes: (data['workingTimes'] as List ?? []).map((v) => v as String).toList(),
      url: data["url"] ?? '',
      phone: data["phone"] ?? '',
      email: data["email"] ?? '',
      type: data["type"] ?? '',
      stats: Map.from(data['stats'] ?? STATS_INITIAL_MAP),
      last5Reviews: (data['last5Reviews'] as List ?? []).map((v) => GoodReviewSummary.fromMap(v)).toList(),
      ownerUid: data["ownerUid"] ?? '',
      extraImages: (data['extraImages'] as Map ?? {}).keys.map((v) => v as String).toList(),
    );
  }

  Map toMap() {
    return {
      "uid": uid,
      "isEnabled": isEnabled,
      "title": title,
      "description": description,
      "heroImage": heroImage,
      "address": address,
      "latitude": latitude,
      "longitude": longitude,
      "workingTimes": workingTimes, //datetime.toIso8601String().substring(11,16),
      "url": url,
      "phone": phone,
      "email": email,
      "type": type,
      "ownerUid": ownerUid,
    };
  }
}

class GoodServiceType extends ITypeable {
  GoodServiceType({
    CategoryType type = CategoryType.SERVICE,
    String title,
    dynamic position,
  }) : super(type: type, title: title, position: position);

  factory GoodServiceType.fromMap(Map data) {
    if (data == null) return GoodServiceType();
    return GoodServiceType(
      title: data['title'] ?? '',
      position: data['position'] ?? 0,
      type: CategoryType.values
          .firstWhere((v) => v.toString() == "CategoryType.${data['type']}", orElse: () => CategoryType.SERVICE),
    );
  }
}
