import 'package:package_info/package_info.dart';

class GoodPackageInfo {
  String appName;
  String packageName;
  String version;
  String buildNumber;

  GoodPackageInfo({this.appName, this.packageName, this.version, this.buildNumber});

  static Future<GoodPackageInfo> getInfo() async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    return Future.value(GoodPackageInfo(
      appName: packageInfo.appName,
      packageName: packageInfo.packageName,
      version: packageInfo.version,
      buildNumber: packageInfo.buildNumber,
    ));
  }
}
