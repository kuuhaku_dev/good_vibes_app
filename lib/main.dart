import 'package:flutter/material.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:good_vibes_app/interfaces/interfaces.dart';
import 'package:good_vibes_app/interfaces/typeable.dart';
import 'package:good_vibes_app/models/models.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';
import 'package:jiffy/jiffy.dart';
import 'package:provider/provider.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'screens/screens.dart';
import 'services/services.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  Jiffy.locale('pt-br');
  initializeDateFormatting().then((_) => runApp(MyApp()));
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Intl.defaultLocale = 'pt_BR';

    return MultiProvider(
      providers: [
        FutureProvider<AppleSignInAvailable>.value(
          value: AppleSignInAvailable.check(),
        ),
        FutureProvider<GoodPackageInfo>.value(
          value: GoodPackageInfo.getInfo(),
        ),
        StreamProvider<GoodUser>.value(value: AuthService().user),
        StreamProvider<List<GoodEvent>>.value(
          value: Global.eventsRef.streamData(
            query: Global.eventsRef.ref.where('isEnabled', isEqualTo: true).orderBy('startDate'),
          ),
        ),
        StreamProvider<List<GoodService>>.value(
          value: Global.servicesRef.streamData(
            query: Global.servicesRef.ref.where('isEnabled', isEqualTo: true),
          ),
        ),
        StreamProvider<List<GoodInfo>>.value(
          value: Global.infosRef.streamData(
            query: Global.infosRef.ref.where('isEnabled', isEqualTo: true).orderBy('updatedAt', descending: true),
          ),
        ),
        StreamProvider<List<GoodInfoType>>.value(
          value: Global.infoTypesRef.streamData(
            query: Global.infoTypesRef.ref
                .where('type', isEqualTo: ITypeable.typeAsString(CategoryType.NEWS))
                .orderBy('position'),
          ),
        ),
        StreamProvider<List<GoodServiceType>>.value(
          value: Global.serviceTypesRef.streamData(
            query: Global.serviceTypesRef.ref
                .where('type', isEqualTo: ITypeable.typeAsString(CategoryType.SERVICE))
                .orderBy('position'),
          ),
        ),
        StreamProvider<List<GoodEventType>>.value(
          value: Global.eventTypesRef.streamData(
            query: Global.eventTypesRef.ref
                .where('type', isEqualTo: ITypeable.typeAsString(CategoryType.EVENT))
                .orderBy('position'),
          ),
        ),
        StreamProvider<ILocationable>.value(value: GeolocationService.instance.streamLocation)
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,

        // Firebase Analytics
        navigatorObservers: [
          FirebaseAnalyticsObserver(analytics: FirebaseAnalytics()),
        ],

        navigatorKey: NavigationService.instance.navigatorKey,

        // Named Routes
        initialRoute: 'registration',
        routes: {
          'registration': (context) => RegistrationNavigator(),
          '/': (context) => HomeScreen(),
        },

        // Theme
        theme: ThemeData(
          // Define the default brightness and colors.
          brightness: Brightness.light,
          primaryColor: const Color(0xFF4D34AB),
          accentColor: const Color(0xFFFE8B24),
          disabledColor: const Color(0x994D34AB),
          scaffoldBackgroundColor: const Color(0xFFF4F5F7),

          // Define the default font family.
          fontFamily: 'Avenir',

          // Define the default TextTheme. Use this to specify the default
          // text styling for headlines, titles, bodies of text, and more.
          textTheme: TextTheme(
            button: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
            subtitle: TextStyle(
              color: const Color(0xFF4D34AB),
            ),
          ),
        ),
      ),
    );
  }
}
