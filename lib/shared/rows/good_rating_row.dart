import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

class GoodRatingRow extends StatelessWidget {
  const GoodRatingRow({
    Key key,
    @required this.rating,
    @required this.total,
  }) : super(key: key);
  final double rating;
  final int total;

  _buildText(int total) {
    String title;

    if (total == 0) {
      title = 'Nenhuma avaliação';
    } else if (total == 1) {
      title = '1 Avaliação';
    } else {
      title = '$total Avaliações';
    }

    return Text(
      title,
      style: TextStyle(fontSize: 12, fontWeight: FontWeight.w300),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        RatingBarIndicator(
          rating: rating,
          itemBuilder: (context, index) => Icon(
            Icons.star,
            color: Theme.of(context).primaryColor,
          ),
          itemCount: 5,
          itemSize: 12.0,
        ),
        SizedBox(width: 8),
        _buildText(total),
      ],
    );
  }
}
