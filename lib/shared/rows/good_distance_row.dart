import 'dart:math';

import 'package:flutter/material.dart';
import 'package:good_vibes_app/interfaces/locationable.dart';
import 'package:provider/provider.dart';

class GoodDistanceRow extends StatelessWidget {
  const GoodDistanceRow({Key key, this.iLocationable}) : super(key: key);
  final ILocationable iLocationable;

  double _calculateDistance(lat1, lon1, lat2, lon2) {
    var p = 0.017453292519943295;
    var c = cos;
    var a = 0.5 - c((lat2 - lat1) * p) / 2 + c(lat1 * p) * c(lat2 * p) * (1 - c((lon2 - lon1) * p)) / 2;
    return 12742 * asin(sqrt(a));
  }

  @override
  Widget build(BuildContext context) {
    final currentLocation = Provider.of<ILocationable>(context);
    return (iLocationable?.position != null && currentLocation != null)
        ? Row(
            children: <Widget>[
              Container(
                padding: EdgeInsets.symmetric(horizontal: 4),
                decoration: BoxDecoration(
                  color: Color(0xFFEFEFEF),
                  border: Border.all(color: Color(0xFF473754)),
                  borderRadius: BorderRadius.all(Radius.circular(4)),
                ),
                child: Text(
                    '${_calculateDistance(currentLocation.latitude, currentLocation.longitude, iLocationable.latitude, iLocationable.longitude).toStringAsFixed(2)} Km',
                    style: TextStyle(fontSize: 10, fontWeight: FontWeight.w300)),
              ),
              SizedBox(width: 4),
              Text('da localização atual', style: TextStyle(fontSize: 12, fontWeight: FontWeight.w300))
            ],
          )
        : Text('Localização indisponível', style: TextStyle(fontSize: 12, fontWeight: FontWeight.w300));
  }
}
