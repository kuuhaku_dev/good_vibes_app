import 'package:flutter/material.dart';
import 'package:good_vibes_app/models/models.dart';
import 'package:good_vibes_app/services/services.dart';
import 'package:good_vibes_app/shared/shared.dart';

class UserForm extends StatefulWidget {
  UserForm({Key key, @required this.user}) : super(key: key);
  final GoodUser user;

  _UserFormState createState() => _UserFormState();
}

class _UserFormState extends State<UserForm> {
  final AuthService _auth = AuthService();
  final _formKey = GlobalKey<FormState>();

  String name;
  bool _autoValidate = false;
  bool _isLoading = false;

  void startLoading() => setState(() => _isLoading = true);
  void finishLoading() => setState(() => _isLoading = false);

  @override
  Widget build(BuildContext context) {
    final user = widget.user;
    return Form(
      key: _formKey,
      autovalidate: _autoValidate,
      child: GestureDetector(
        onTap: () => FocusScope.of(context).requestFocus(new FocusNode()),
        child: ListView(
          padding: EdgeInsets.symmetric(vertical: 24.0),
          children: <Widget>[
            Text(
              'DADOS PESSOAIS',
              style: Theme.of(context).textTheme.subtitle,
            ),
            TextFormField(
              decoration: const InputDecoration(labelText: 'Nome'),
              onSaved: (String value) {
                name = value;
              },
              initialValue: user.name,
              textInputAction: TextInputAction.unspecified,
              validator: _validateName,
            ),
            Padding(
                padding: const EdgeInsets.symmetric(vertical: 24.0),
                child: GoodProgressButton(
                  isLoading: _isLoading,
                  onPressed: _validateInputs,
                  text: 'SALVAR',
                )),
          ],
        ),
      ),
    );
  }

  String _validateName(String value) {
    if (value.isEmpty) {
      return "Insira seu nome";
    }

    return null;
  }

  void _validateInputs() async {
    final form = _formKey.currentState;
    FocusScope.of(context).requestFocus(new FocusNode());
    if (form.validate()) {
      // Text forms was validated.
      form.save();
      try {
        startLoading();
        final user = widget.user;
        Document userRef = Document<GoodUser>(path: '${GoodUser.COL_PATH}/${user.uid}');

        await userRef.upsert({'name': name});
        Scaffold.of(context).showSnackBar(SnackBar(
          content: Text('Dados atualizados com sucesso'),
        ));
      } catch (error) {
        NotificationService.error(context, error.code, error.message);
      }
      finishLoading();
    } else {
      setState(() => _autoValidate = true);
    }
  }
}
