import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:good_vibes_app/models/models.dart';
import 'package:good_vibes_app/services/file_storage.dart';
import 'package:good_vibes_app/services/services.dart';
import 'package:good_vibes_app/shared/cards/good_map_card.dart';
import 'package:good_vibes_app/shared/shared.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:image_picker/image_picker.dart';
import 'package:multi_image_picker/multi_image_picker.dart';
import 'package:provider/provider.dart';

class EventForm extends StatefulWidget {
  EventForm({Key key, this.event}) : super(key: key);
  final GoodEvent event;

  _EventFormState createState() => _EventFormState();
}

class _EventFormState extends State<EventForm> {
  final Collection<GoodEvent> _eventsCollection = Collection<GoodEvent>(path: GoodEvent.COL_PATH);
  final _formKey = GlobalKey<FormState>();

  String title;
  String url;
  String phone;
  String email;
  DateTime startDate;
  String address;
  String description;
  LatLng location;
  File heroImage;
  String type;

  bool _autoValidate = false;
  bool _isLoading = false;

  List<Asset> extraImages = List<Asset>();

  void startLoading() => setState(() => _isLoading = true);
  void finishLoading() => setState(() => _isLoading = false);

  @override
  void initState() {
    super.initState();
    startDate = widget.event?.startDate;
    if (widget.event?.latitude != null && widget.event?.longitude != null) {
      location = LatLng(widget.event.latitude, widget.event.longitude);
    }
  }

  void _onSelect(List assets) => setState(() => extraImages = assets);

  Widget _buildImagePreview() {
    if (heroImage != null) {
      return Image.file(
        heroImage,
        fit: BoxFit.fitWidth,
      );
    } else if (widget.event?.heroImage != null) {
      return Image.network(
        widget.event.heroImage,
        fit: BoxFit.fitWidth,
      );
    } else {
      return Container();
    }
  }

  @override
  Widget build(BuildContext context) {
    final event = widget.event;

    return Form(
      key: _formKey,
      autovalidate: _autoValidate,
      child: GestureDetector(
        onTap: () => FocusScope.of(context).requestFocus(new FocusNode()),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              _buildImagePreview(),
              GoodProfileSection(
                children: <Widget>[
                  Container(
                    color: Colors.white,
                    padding: EdgeInsets.symmetric(vertical: 16, horizontal: 16),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Expanded(
                          child: TextFormField(
                            decoration: InputDecoration(
                              hintText: 'Título do evento',
                              hintStyle: TextStyle(fontSize: 18),
                              border: InputBorder.none,
                            ),
                            onSaved: (String value) {
                              title = value;
                            },
                            initialValue: event?.title ?? '',
                            textInputAction: TextInputAction.unspecified,
                            validator: _validateTitle,
                          ),
                        ),
                        IconButton(
                          icon: Icon(
                            FontAwesomeIcons.images,
                            size: 24,
                            color: Theme.of(context).primaryColor,
                          ),
                          onPressed: () async {
                            File selectedImage = await ImagePicker.pickImage(source: ImageSource.gallery);
                            setState(() {
                              heroImage = selectedImage;
                            });
                          },
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 24,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16),
                child: GoodEventTypeFormField(
                  onSaved: (String value) {
                    type = value;
                  },
                  initialValue: '',
                  validator: _validateEventType,
                  autovalidate: _autoValidate,
                ),
              ),
              SizedBox(
                height: 24,
              ),
              GoodProfileSection(
                hasIndentedSeparator: false,
                children: <Widget>[
                  ListTile(
                    title: TextFormField(
                      decoration: InputDecoration(
                        hintText: 'Website',
                        border: InputBorder.none,
                      ),
                      onSaved: (String value) {
                        url = value;
                      },
                      initialValue: event?.url ?? '',
                      textInputAction: TextInputAction.unspecified,
                    ),
                  ),
                  ListTile(
                    title: TextFormField(
                      decoration: InputDecoration(
                        hintText: 'Telefone',
                        border: InputBorder.none,
                      ),
                      onSaved: (String value) {
                        phone = value;
                      },
                      initialValue: event?.phone ?? '',
                      keyboardType: TextInputType.phone,
                      textInputAction: TextInputAction.unspecified,
                    ),
                  ),
                  ListTile(
                    title: TextFormField(
                      decoration: InputDecoration(
                        hintText: 'Email',
                        border: InputBorder.none,
                      ),
                      onSaved: (String value) {
                        email = value;
                      },
                      initialValue: event?.email ?? '',
                      keyboardType: TextInputType.emailAddress,
                      textInputAction: TextInputAction.unspecified,
                    ),
                  ),
                  GoodDateFormField(
                    onSaved: (DateTime value) {
                      startDate = value;
                    },
                    initialValue: startDate,
                    validator: _validateDate,
                    autovalidate: _autoValidate,
                  ),
                  ListTile(
                    title: TextFormField(
                      decoration: InputDecoration(
                        hintText: 'Endereço',
                        border: InputBorder.none,
                      ),
                      onSaved: (String value) {
                        address = value;
                      },
                      initialValue: event?.address ?? '',
                      validator: _validateAddress,
                      textInputAction: TextInputAction.unspecified,
                    ),
                  ),
                  Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      GoodMapCard(iLocationable: widget.event, position: location),
                      ListTile(
                        title: Text('Localização'),
                        trailing: Icon(Icons.chevron_right),
                        onTap: () async {
                          LatLng selectedLocation = await Navigator.of(context).push(
                            MaterialPageRoute(
                              builder: (_) => GoodLocationPickerScreen(savedPosition: location),
                              fullscreenDialog: true,
                            ),
                          );
                          if (selectedLocation != null) {
                            setState(() {
                              location = selectedLocation;
                            });
                          }
                        },
                      ),
                    ],
                  ),
                  ListTile(
                    title: TextFormField(
                      decoration: InputDecoration(
                        hintText: 'Mais informações',
                        border: InputBorder.none,
                      ),
                      onSaved: (String value) {
                        description = value;
                      },
                      initialValue: event?.description ?? '',
                      maxLines: 5,
                      textInputAction: TextInputAction.unspecified,
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 24,
              ),
              GoodExtraImagePicker(
                onSelect: _onSelect,
                iImageable: event,
              ),
              SizedBox(height: 48),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 64),
                child: GoodProgressButton(
                  isLoading: _isLoading,
                  onPressed: _validateInputs,
                  text: event != null ? 'SALVAR' : 'CRIAR',
                ),
              ),
              SizedBox(height: 48),
            ],
          ),
        ),
      ),
    );
  }

  String _validateTitle(String value) {
    if (value.isEmpty) {
      return "Insira um título";
    }

    return null;
  }

  String _validateAddress(String value) {
    if (value.isEmpty) {
      return "Insira um endereço";
    }

    return null;
  }

  String _validateDate(DateTime value) {
    if (value == null) {
      return "Insira uma data";
    }

    return null;
  }

  String _validateEventType(String value) {
    if (value.isEmpty) {
      return "Insira um tipo";
    }

    return null;
  }

  void _validateInputs() async {
    final form = _formKey.currentState;
    FocusScope.of(context).requestFocus(new FocusNode());
    if (form.validate()) {
      form.save();
      try {
        startLoading();
        if (!AuthService.isUserRegistered(context)) {
          return;
        }
        GoodUser user = Provider.of<GoodUser>(context);

        final GoodEvent originalEvent = widget.event;

        final GoodEvent eventToSave = GoodEvent(
          uid: originalEvent?.uid,
          ownerUid: user.uid ?? originalEvent?.ownerUid,
          heroImage: originalEvent?.heroImage,
          title: title ?? originalEvent?.title,
          description: description ?? originalEvent.description,
          url: url ?? originalEvent.url,
          phone: phone ?? originalEvent.phone,
          email: email ?? originalEvent.email,
          startDate: startDate,
          address: address,
          latitude: location?.latitude,
          longitude: location?.longitude,
          type: type,
        );

        var uid = await _eventsCollection.upsert(eventToSave.toMap());

        final uploads = List<Future>();

        if (heroImage != null) {
          var uploadService = UploaderService(file: heroImage);
          final future = uploadService.uploadFileFuture(
              "images/events/$uid/${Global.HERO_PREFIX}${DateTime.now().millisecondsSinceEpoch.toString()}");
          uploads.add(future);
        }

        extraImages.forEach((asset) async {
          ByteData byteData = await asset.getByteData();
          List<int> imageData = byteData.buffer.asUint8List();
          UploaderService uploadService = UploaderService(imageData: imageData);
          uploads.add(uploadService.uploadFileFuture("images/events/$uid/${Global.EXTRA_PREFIX}${asset.name}"));
        });

        await Future.wait(uploads);

        Navigator.of(context).pop();
        NotificationService.showSnackBar('Item enviado para análise');
      } catch (error) {
        NotificationService.error(context, error.code, error.message);
      }
      finishLoading();
    } else {
      setState(() => _autoValidate = true);
    }
  }
}
