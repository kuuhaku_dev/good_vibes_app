import 'package:flutter/material.dart';
import 'package:flutter_auth_buttons/flutter_auth_buttons.dart';
import 'package:good_vibes_app/models/models.dart';
import 'package:good_vibes_app/services/services.dart';
import 'package:good_vibes_app/shared/shared.dart';
import 'package:provider/provider.dart';

class SignUpForm extends StatefulWidget {
  SignUpForm({Key key}) : super(key: key);

  _SignUpFormState createState() => _SignUpFormState();
}

class _SignUpFormState extends State<SignUpForm> {
  final AuthService _auth = AuthService();
  final _formKey = GlobalKey<FormState>();

  String name;
  String email;
  String password;
  bool _autoValidate = false;
  bool _isLoading = false;

  void startLoading() => setState(() => _isLoading = true);
  void finishLoading() => setState(() => _isLoading = false);

  @override
  Widget build(BuildContext context) {
    final appleSignInAvailable = Provider.of<AppleSignInAvailable>(context);
    return Form(
      key: _formKey,
      autovalidate: _autoValidate,
      child: GestureDetector(
        onTap: () => FocusScope.of(context).requestFocus(new FocusNode()),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              SizedBox(height: 24),
              Text(
                'DADOS PESSOAIS',
                style: Theme.of(context).textTheme.subtitle,
              ),
              TextFormField(
                decoration: const InputDecoration(labelText: 'Nome'),
                onSaved: (String value) {
                  name = value;
                },
                textInputAction: TextInputAction.unspecified,
                validator: _validateName,
              ),
              TextFormField(
                decoration: const InputDecoration(labelText: 'Email'),
                onSaved: (String value) {
                  email = value;
                },
                validator: _validateEmail,
                keyboardType: TextInputType.emailAddress,
              ),
              TextFormField(
                decoration: const InputDecoration(labelText: 'Senha'),
                onSaved: (String value) {
                  password = value;
                },
                validator: _validatePassword,
                obscureText: true,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 24.0),
                child: GoodProgressButton(
                  isLoading: _isLoading,
                  onPressed: _validateInputs,
                  text: 'CADASTRAR',
                ),
              ),
              Divider(),
              SizedBox(height: 24),
              Center(child: Text('Ou se preferir...')),
              SizedBox(height: 8),
              Center(
                child: GoogleSignInButton(
                  onPressed: () async {
                    try {
                      var newUser = await _auth.googleSignIn();
                      if (newUser != null) {
                        NavigationService.instance.resetToRoot();
                      }
                    } catch (error) {
                      NotificationService.error(context, error.code, error.message);
                    }
                  },
                  darkMode: false, // default: false
                ),
              ),
              if (appleSignInAvailable.isAvailable)
                Center(
                  child: AppleSignInButton(
                    style: AppleButtonStyle.black,
                    onPressed: () async {
                      try {
                        var newUser = await _auth.appleSignIn();
                        if (newUser != null) {
                          NavigationService.instance.resetToRoot();
                        }
                      } catch (error) {
                        NotificationService.error(context, error.code, error.message);
                      }
                    },
                  ),
                ),
              SizedBox(height: 24),
            ],
          ),
        ),
      ),
    );
  }

  String _validateEmail(String value) {
    if (value.isEmpty) {
      // The form is empty
      return "Insira seu email";
    }
    // This is just a regular expression for email addresses
    String p = "[a-zA-Z0-9\+\.\_\%\-\+]{1,256}" +
        "\\@" +
        "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
        "(" +
        "\\." +
        "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
        ")+";
    RegExp regExp = new RegExp(p);

    if (regExp.hasMatch(value)) {
      // So, the email is valid
      return null;
    }

    // The pattern of the email didn't match the regex above.
    return 'Email inválido';
  }

  String _validateName(String value) {
    if (value.isEmpty) {
      return "Insira seu nome";
    }

    return null;
  }

  String _validatePassword(String value) {
    if (value.isEmpty) {
      return "Crie uma senha";
    }

    return null;
  }

  void _validateInputs() async {
    final form = _formKey.currentState;
    FocusScope.of(context).requestFocus(new FocusNode());
    if (form.validate()) {
      // Text forms was validated.
      form.save();
      try {
        startLoading();
        GoodUser user = GoodUser(name: name, email: email);
        var newUser = await _auth.signUp(user, password);
        if (newUser != null) {
          NavigationService.instance.resetToRoot();
        }
      } catch (error) {
        NotificationService.error(context, error.code, error.message);
      }
      finishLoading();
    } else {
      setState(() => _autoValidate = true);
    }
  }
}
