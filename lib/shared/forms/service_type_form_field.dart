import 'package:flutter/material.dart';
import 'package:good_vibes_app/models/models.dart';
import 'package:provider/provider.dart';

class GoodServiceTypeFormField extends FormField<String> {
  GoodServiceTypeFormField({
    FormFieldSetter<String> onSaved,
    FormFieldValidator<String> validator,
    String initialValue,
    bool autovalidate = false,
  }) : super(
          onSaved: onSaved,
          validator: validator,
          initialValue: initialValue,
          autovalidate: autovalidate,
          builder: (FormFieldState<String> state) {
            final serviceTypes = Provider.of<List<GoodServiceType>>(state.context);
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Wrap(
                  children: List<Widget>.generate(
                    serviceTypes.length,
                    (int index) {
                      return ChoiceChip(
                        label: Text(serviceTypes[index].title),
                        selected: state.value == serviceTypes[index].title,
                        onSelected: (bool selected) {
                          if (selected == true) state.didChange(serviceTypes[index].title);
                        },
                      );
                    },
                  ).toList(),
                ),
                state.hasError
                    ? Padding(
                        padding: EdgeInsets.only(top: 8),
                        child: Text(
                          state.errorText,
                          style: TextStyle(color: Theme.of(state.context).errorColor, fontSize: 12),
                        ))
                    : Container()
              ],
            );
          },
        );
}
