import 'package:flutter/material.dart';
import 'package:good_vibes_app/models/models.dart';
import 'package:provider/provider.dart';

class GoodEventTypeFormField extends FormField<String> {
  GoodEventTypeFormField({
    FormFieldSetter<String> onSaved,
    FormFieldValidator<String> validator,
    String initialValue,
    bool autovalidate = false,
  }) : super(
          onSaved: onSaved,
          validator: validator,
          initialValue: initialValue,
          autovalidate: autovalidate,
          builder: (FormFieldState<String> state) {
            final types = Provider.of<List<GoodEventType>>(state.context);
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Wrap(
                  children: List<Widget>.generate(
                    types.length,
                    (int index) {
                      return ChoiceChip(
                        label: Text(types[index].title),
                        selected: state.value == types[index].title,
                        onSelected: (bool selected) {
                          if (selected == true) state.didChange(types[index].title);
                        },
                      );
                    },
                  ).toList(),
                ),
                state.hasError
                    ? Padding(
                        padding: EdgeInsets.only(top: 8),
                        child: Text(
                          state.errorText,
                          style: TextStyle(color: Theme.of(state.context).errorColor, fontSize: 12),
                        ))
                    : Container()
              ],
            );
          },
        );
}
