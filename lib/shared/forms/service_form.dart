import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:good_vibes_app/models/models.dart';
import 'package:good_vibes_app/services/file_storage.dart';
import 'package:good_vibes_app/services/services.dart';
import 'package:good_vibes_app/shared/cards/good_map_card.dart';
import 'package:good_vibes_app/shared/shared.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:image_picker/image_picker.dart';
import 'package:multi_image_picker/multi_image_picker.dart';
import 'package:provider/provider.dart';

class ServiceForm extends StatefulWidget {
  ServiceForm({Key key, this.service}) : super(key: key);
  final GoodService service;

  _ServiceFormState createState() => _ServiceFormState();
}

class _ServiceFormState extends State<ServiceForm> {
  final Collection<GoodService> _servicesCollection = Collection<GoodService>(path: GoodService.COL_PATH);
  final _formKey = GlobalKey<FormState>();

  String title;
  String url;
  String phone;
  String email;
  String address;
  String description;
  LatLng location;
  File heroImage;
  String serviceType;

  bool _autoValidate = false;
  bool _isLoading = false;

  List<Asset> extraImages = List<Asset>();

  void startLoading() => setState(() => _isLoading = true);
  void finishLoading() => setState(() => _isLoading = false);

  @override
  void initState() {
    super.initState();
    if (widget.service?.latitude != null && widget.service?.longitude != null) {
      location = LatLng(widget.service.latitude, widget.service.longitude);
    }
  }

  void _onSelect(List assets) => setState(() => extraImages = assets);

  Widget _buildImagePreview() {
    if (heroImage != null) {
      return Image.file(
        heroImage,
        fit: BoxFit.fitWidth,
      );
    } else if (widget.service?.heroImage != null) {
      return Image.network(
        widget.service.heroImage,
        fit: BoxFit.fitWidth,
      );
    } else {
      return Container();
    }
  }

  @override
  Widget build(BuildContext context) {
    final service = widget.service;

    return Form(
      key: _formKey,
      autovalidate: _autoValidate,
      child: GestureDetector(
        onTap: () => FocusScope.of(context).requestFocus(new FocusNode()),
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              _buildImagePreview(),
              GoodProfileSection(
                children: <Widget>[
                  Container(
                    color: Colors.white,
                    padding: EdgeInsets.symmetric(vertical: 16, horizontal: 16),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Expanded(
                          child: TextFormField(
                            decoration: InputDecoration(
                              hintText: 'Título',
                              hintStyle: TextStyle(fontSize: 18),
                              border: InputBorder.none,
                            ),
                            onSaved: (String value) {
                              title = value;
                            },
                            initialValue: service?.title ?? '',
                            textInputAction: TextInputAction.unspecified,
                            validator: _validateTitle,
                          ),
                        ),
                        IconButton(
                          icon: Icon(
                            FontAwesomeIcons.images,
                            size: 24,
                            color: Theme.of(context).primaryColor,
                          ),
                          onPressed: () async {
                            File selectedImage = await ImagePicker.pickImage(source: ImageSource.gallery);
                            setState(() {
                              heroImage = selectedImage;
                            });
                          },
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 24,
              ),
              GoodServiceTypeFormField(
                onSaved: (String value) {
                  serviceType = value;
                },
                initialValue: service?.type ?? '',
                validator: _validateServiceType,
                autovalidate: _autoValidate,
              ),
              SizedBox(
                height: 24,
              ),
              GoodProfileSection(
                hasIndentedSeparator: false,
                children: <Widget>[
                  ListTile(
                    title: TextFormField(
                      decoration: InputDecoration(
                        hintText: 'Website',
                        border: InputBorder.none,
                      ),
                      onSaved: (String value) {
                        url = value;
                      },
                      initialValue: service?.url ?? '',
                      textInputAction: TextInputAction.unspecified,
                    ),
                  ),
                  ListTile(
                    title: TextFormField(
                      decoration: InputDecoration(
                        hintText: 'Telefone',
                        border: InputBorder.none,
                      ),
                      onSaved: (String value) {
                        phone = value;
                      },
                      initialValue: service?.phone ?? '',
                      keyboardType: TextInputType.phone,
                      textInputAction: TextInputAction.unspecified,
                    ),
                  ),
                  ListTile(
                    title: TextFormField(
                      decoration: InputDecoration(
                        hintText: 'Email',
                        border: InputBorder.none,
                      ),
                      onSaved: (String value) {
                        email = value;
                      },
                      initialValue: service?.email ?? '',
                      keyboardType: TextInputType.emailAddress,
                      textInputAction: TextInputAction.unspecified,
                    ),
                  ),
                  ListTile(
                    title: TextFormField(
                      decoration: InputDecoration(
                        hintText: 'Endereço',
                        border: InputBorder.none,
                      ),
                      onSaved: (String value) {
                        address = value;
                      },
                      initialValue: service?.address ?? '',
                      validator: _validateAddress,
                      textInputAction: TextInputAction.unspecified,
                    ),
                  ),
                  Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      GoodMapCard(iLocationable: widget.service, position: location),
                      ListTile(
                        title: Text('Localização'),
                        trailing: Icon(Icons.chevron_right),
                        onTap: () async {
                          LatLng selectedLocation = await Navigator.of(context).push(
                            MaterialPageRoute(
                              builder: (_) => GoodLocationPickerScreen(savedPosition: location),
                              fullscreenDialog: true,
                            ),
                          );
                          if (selectedLocation != null) {
                            setState(() {
                              location = selectedLocation;
                            });
                          }
                        },
                      ),
                    ],
                  ),
                  ListTile(
                    title: TextFormField(
                      decoration: InputDecoration(
                        hintText: 'Mais informações',
                        border: InputBorder.none,
                      ),
                      onSaved: (String value) {
                        description = value;
                      },
                      initialValue: service?.description ?? '',
                      maxLines: 5,
                      textInputAction: TextInputAction.unspecified,
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 24,
              ),
              GoodExtraImagePicker(
                onSelect: _onSelect,
                iImageable: service,
              ),
              SizedBox(height: 48),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 64),
                child: GoodProgressButton(
                  isLoading: _isLoading,
                  onPressed: _validateInputs,
                  text: service != null ? 'SALVAR' : 'CRIAR',
                ),
              ),
              SizedBox(height: 48),
            ],
          ),
        ),
      ),
    );
  }

  String _validateTitle(String value) {
    if (value.isEmpty) {
      return "Insira um título";
    }

    return null;
  }

  String _validateAddress(String value) {
    if (value.isEmpty) {
      return "Insira um endereço";
    }

    return null;
  }

  String _validateServiceType(String value) {
    if (value.isEmpty) {
      return "Insira um tipo";
    }

    return null;
  }

  void _validateInputs() async {
    final form = _formKey.currentState;
    FocusScope.of(context).requestFocus(new FocusNode());
    if (form.validate()) {
      form.save();
      try {
        startLoading();
        if (!AuthService.isUserRegistered(context)) {
          return;
        }
        GoodUser user = Provider.of<GoodUser>(context);

        final GoodService originalService = widget.service;

        final GoodService serviceToSave = GoodService(
          uid: originalService?.uid,
          type: serviceType,
          ownerUid: user.uid ?? originalService?.ownerUid,
          heroImage: originalService?.heroImage,
          title: title ?? originalService?.title,
          description: description ?? originalService.description,
          url: url ?? originalService.url,
          phone: phone ?? originalService.phone,
          email: email ?? originalService.email,
          address: address,
          latitude: location?.latitude,
          longitude: location?.longitude,
        );

        var uid = await _servicesCollection.upsert(serviceToSave.toMap());

        final uploads = List<Future>();

        if (heroImage != null) {
          var uploadService = UploaderService(file: heroImage);
          final future = uploadService.uploadFileFuture(
              "images/services/$uid/${Global.HERO_PREFIX}${DateTime.now().millisecondsSinceEpoch.toString()}");
          uploads.add(future);
        }

        extraImages.forEach((asset) async {
          ByteData byteData = await asset.getByteData();
          List<int> imageData = byteData.buffer.asUint8List();
          UploaderService uploadService = UploaderService(imageData: imageData);
          uploads.add(uploadService.uploadFileFuture("images/services/$uid/${Global.EXTRA_PREFIX}${asset.name}"));
        });

        await Future.wait(uploads);

        Navigator.of(context).pop();
        NotificationService.showSnackBar('Item enviado para análise');
      } catch (error) {
        NotificationService.error(context, error.code, error.message);
      }
      finishLoading();
    } else {
      setState(() => _autoValidate = true);
    }
  }
}
