import 'package:flutter/material.dart';
import 'package:good_vibes_app/shared/shared.dart';
import 'package:intl/intl.dart';

class GoodDateFormField extends FormField<DateTime> {
  GoodDateFormField({
    FormFieldSetter<DateTime> onSaved,
    FormFieldValidator<DateTime> validator,
    DateTime initialValue,
    bool autovalidate = false,
  }) : super(
          onSaved: onSaved,
          validator: validator,
          initialValue: initialValue,
          autovalidate: autovalidate,
          builder: (FormFieldState<DateTime> state) {
            return ListTile(
              title: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  state.value == null
                      ? Text('Data e hora')
                      : Text("${DateFormat.MMMd().format(state.value)} às ${DateFormat.Hm().format(state.value)}"),
                  state.hasError
                      ? Padding(
                          padding: EdgeInsets.only(top: 8),
                          child: Text(
                            state.errorText,
                            style: TextStyle(color: Theme.of(state.context).errorColor, fontSize: 12),
                          ))
                      : Container()
                ],
              ),
              trailing: Icon(Icons.chevron_right),
              onTap: () async {
                var selectedStartDate = await Navigator.of(state.context).push(
                  MaterialPageRoute(
                    builder: (_) => GoodDatepickerScreen(startDate: state.value),
                    fullscreenDialog: true,
                  ),
                );
                if (selectedStartDate != null) state.didChange(selectedStartDate);
              },
            );
          },
        );
}
