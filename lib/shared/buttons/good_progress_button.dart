import 'package:flutter/material.dart';

class GoodProgressButton extends StatelessWidget {
  const GoodProgressButton({Key key, @required this.text, @required this.onPressed, this.isLoading: false})
      : super(key: key);
  final bool isLoading;
  final Function onPressed;
  final String text;

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      child: setUpButtonChild(context),
      onPressed: onPressed,
      color: Theme.of(context).primaryColor,
      minWidth: MediaQuery.of(context).size.width,
      padding: EdgeInsets.symmetric(vertical: 16.0),
      shape: StadiumBorder(),
    );
  }

  Widget setUpButtonChild(BuildContext context) {
    if (isLoading) {
      return CircularProgressIndicator(
        valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
      );
    } else {
      return new Text(
        text,
        style: Theme.of(context).textTheme.button.merge(TextStyle(color: Colors.white)),
      );
    }
  }
}
