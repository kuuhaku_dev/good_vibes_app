import 'package:flutter/material.dart';

class GoodOutlineButton extends StatelessWidget {
  const GoodOutlineButton({Key key, @required this.title, @required this.onPressed}) : super(key: key);
  final String title;
  final Function onPressed;

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      shape: StadiumBorder(
        side: BorderSide(
          width: 4,
          color: Theme.of(context).primaryColor,
        ),
      ),
      height: 64,
      child: Text(
        title.toUpperCase(),
        style: TextStyle(
          fontSize: 14,
          fontWeight: FontWeight.w900,
        ),
      ),
      onPressed: onPressed,
    );
  }
}
