import 'package:flutter/material.dart';
import 'package:good_vibes_app/interfaces/interfaces.dart';
import 'package:good_vibes_app/shared/shared.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class GoodMapNearbyCard extends StatefulWidget {
  GoodMapNearbyCard({Key key, @required this.currentLocation, this.iLocationables}) : super(key: key);
  final ILocationable currentLocation;
  final List<ILocationable> iLocationables;

  @override
  _GoodMapNearbyCardState createState() => _GoodMapNearbyCardState();
}

class _GoodMapNearbyCardState extends State<GoodMapNearbyCard> {
  GoogleMapController controller;
  Map<MarkerId, Marker> markers = <MarkerId, Marker>{};

  Widget _buildMap() {
    if (widget.currentLocation != null || widget.iLocationables != null) {
      if (controller != null) {
        _setMarkers();
      }

      return ClipRRect(
        borderRadius: BorderRadius.circular(8),
        child: Container(
          height: 125,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
          ),
          child: GoogleMap(
            initialCameraPosition: CameraPosition(
              target: LatLng(widget.currentLocation.latitude, widget.currentLocation.longitude),
              zoom: 13,
            ),
            onMapCreated: _onMapCreated,
            onTap: (LatLng latLng) => Navigator.of(context).push(
              MaterialPageRoute(
                builder: (_) => GoodMapScreen(
                  iLocationables: widget.iLocationables,
                ),
              ),
            ),
            rotateGesturesEnabled: false,
            scrollGesturesEnabled: false,
            tiltGesturesEnabled: false,
            zoomGesturesEnabled: false,
            trafficEnabled: false,
            mapType: MapType.normal,
            markers: Set<Marker>.of(markers.values),
          ),
        ),
      );
    } else {
      return Container();
    }
  }

  void _setMarkers() {
    if (widget.currentLocation != null || widget.iLocationables != null) {
      Map<MarkerId, Marker> newMarkers = <MarkerId, Marker>{};
      widget.iLocationables?.forEach((iLocationable) {
        LatLng latlng = iLocationable.position;
        if (latlng == null) {
          return;
        }
        MarkerId markerId = MarkerId("${latlng.latitude.toString()}:${latlng.longitude.toString()}");

        Marker marker = Marker(
          markerId: markerId,
          position: latlng,
        );

        newMarkers[markerId] = marker;
      });

      setState(() {
        markers = newMarkers;
      });
    }
  }

  void _onMapCreated(GoogleMapController controller) {
    this.controller = controller;
    _setMarkers();
  }

  @override
  Widget build(BuildContext context) {
    final currentLocation = widget.currentLocation;
    return currentLocation != null ? _buildMap() : Container();
  }
}
