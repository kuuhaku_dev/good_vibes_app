import 'package:flutter/material.dart';

class GoodDonationCard extends StatelessWidget {
  const GoodDonationCard({Key key, @required this.description, @required this.price, @required this.onTap})
      : super(key: key);
  final String description;
  final String price;
  final Function onTap;

  @override
  Widget build(BuildContext context) {
    final double height = 283;

    return Container(
      height: height,
      color: Colors.white,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Image.asset(
            'assets/img/event1.png',
            height: 184,
            fit: BoxFit.cover,
          ),
          Padding(
            padding: EdgeInsets.fromLTRB(16, 8, 16, 16),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Expanded(
                      child: Text(
                        description.toUpperCase(),
                        softWrap: true,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w900,
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 8),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      price,
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                        color: Colors.green,
                      ),
                    ),
                    FlatButton(child: Text('Doar'), onPressed: onTap),
                  ],
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
