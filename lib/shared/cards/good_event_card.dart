import 'package:flutter/material.dart';
import 'package:good_vibes_app/models/models.dart';
import 'package:good_vibes_app/screens/screens.dart';
import 'package:good_vibes_app/services/services.dart';
import 'package:good_vibes_app/shared/shared.dart';
import 'package:intl/intl.dart';

class GoodEventCard extends StatelessWidget {
  const GoodEventCard({Key key, @required this.event, this.isEditable = false}) : super(key: key);
  final GoodEvent event;
  final bool isEditable;

  static const double _IMAGE_HEIGHT = 184;

  Widget _buildEditButton() {
    if (isEditable) {
      return Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          event.isEnabled
              ? Text(
                  'ATIVO',
                  style: TextStyle(fontWeight: FontWeight.bold, color: Colors.green[200]),
                )
              : Text(
                  'INATIVO',
                  style: TextStyle(fontWeight: FontWeight.bold, color: Colors.redAccent[200]),
                ),
          IconButton(
            icon: Icon(Icons.edit),
            onPressed: () =>
                NavigationService.instance.push(MaterialPageRoute(builder: (_) => EventEditScreen(event: event))),
          ),
        ],
      );
    } else {
      return Container();
    }
  }

  @override
  Widget build(BuildContext context) {
    final double height = isEditable ? 334 : 300;

    return GestureDetector(
      onTap: () => NavigationService.instance.push(MaterialPageRoute(builder: (_) => EventScreen(event: event))),
      child: Container(
        height: height,
        color: Colors.white,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            GoodCachedImage(
              imageUrl: event.heroImageThumb,
              height: _IMAGE_HEIGHT,
            ),
            Padding(
              padding: EdgeInsets.fromLTRB(16, 8, 16, 16),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Expanded(
                        flex: 4,
                        child: Text(
                          event.title.toUpperCase(),
                          softWrap: true,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.w900,
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Text(
                          DateFormat.Md().format(event.startDate),
                          overflow: TextOverflow.ellipsis,
                          textAlign: TextAlign.right,
                          style: TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.w900,
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 8),
                  Text(
                    event.address ?? '',
                    style: TextStyle(
                      fontSize: 12,
                      fontWeight: FontWeight.w300,
                    ),
                  ),
                  SizedBox(height: 8),
                  Text(
                    event.description,
                    overflow: TextOverflow.ellipsis,
                    maxLines: 2,
                  ),
                  _buildEditButton(),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
