import 'package:flutter/material.dart';
import 'package:good_vibes_app/interfaces/interfaces.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class GoodMapCard extends StatefulWidget {
  GoodMapCard({Key key, @required this.iLocationable, this.position}) : super(key: key);
  final ILocationable iLocationable;
  final LatLng position;

  @override
  _GoodMapCardState createState() => _GoodMapCardState();
}

class _GoodMapCardState extends State<GoodMapCard> {
  GoogleMapController controller;
  Map<MarkerId, Marker> markers = <MarkerId, Marker>{};

  Widget _buildMap() {
    if (widget.iLocationable?.position != null || widget.position != null) {
      if (controller != null) {
        _setInitialMarker();
      }

      return Padding(
        padding: const EdgeInsets.all(16.0),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(8),
          child: Container(
            height: 145,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8),
            ),
            child: GoogleMap(
              initialCameraPosition: CameraPosition(
                target: widget.position ?? widget.iLocationable?.position,
                zoom: 15,
              ),
              onMapCreated: _onMapCreated,
              rotateGesturesEnabled: false,
              scrollGesturesEnabled: false,
              tiltGesturesEnabled: false,
              zoomGesturesEnabled: false,
              trafficEnabled: false,
              mapType: MapType.normal,
              markers: Set<Marker>.of(markers.values),
            ),
          ),
        ),
      );
    } else {
      return Container();
    }
  }

  void _setInitialMarker() {
    if (widget.iLocationable?.position != null || widget.position != null) {
      final MarkerId markerId = MarkerId('0');

      final Marker marker = Marker(
        markerId: markerId,
        position: widget.position ?? widget.iLocationable?.position,
      );

      setState(() {
        markers[markerId] = marker;
      });
    }
  }

  void _onMapCreated(GoogleMapController controller) {
    this.controller = controller;
    _setInitialMarker();
  }

  @override
  Widget build(BuildContext context) {
    return _buildMap();
  }
}
