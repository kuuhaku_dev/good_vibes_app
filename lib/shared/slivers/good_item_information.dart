import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:good_vibes_app/interfaces/interfaces.dart';
import 'package:good_vibes_app/services/notification.dart';
import 'package:good_vibes_app/shared/shared.dart';
import 'package:url_launcher/url_launcher.dart';

class GoodItemInformation extends StatelessWidget {
  const GoodItemInformation({Key key, @required this.iInformationable, this.iLocationable}) : super(key: key);
  final IInformationable iInformationable;
  final ILocationable iLocationable;

  List<Widget> _buildTilesArray(BuildContext context) {
    List<Widget> tiles = [];

    if (iInformationable.url.isNotEmpty) {
      tiles.add(GoodInformationTile(
        iconData: FontAwesomeIcons.desktop,
        title: iInformationable.url,
        onTap: () {
          final url = iInformationable.url;
          url.startsWith(new RegExp(r'^http(s?):\/\/'))
              ? _launchURL(context, url)
              : _launchURL(context, "https://$url");
        },
      ));
    }

    if (iInformationable.phone.isNotEmpty) {
      tiles.add(GoodInformationTile(
        iconData: FontAwesomeIcons.phone,
        title: iInformationable.phone,
        onTap: () => _launchURL(context, "tel:${iInformationable.phone}"),
        isBoldTitle: true,
        hasChevron: false,
      ));
    }

    if (iInformationable.email.isNotEmpty) {
      tiles.add(GoodInformationTile(
        iconData: FontAwesomeIcons.envelope,
        title: iInformationable.email,
        onTap: () => _launchURL(context, "mailto:${iInformationable.email}"),
      ));
    }

    if (iInformationable.address.isNotEmpty) {
      tiles.add(GoodInformationTile(
        iconData: Icons.location_on,
        iLocationable: iLocationable,
        title: iInformationable.address,
        hasChevron: false,
        onTap: () {},
      ));
    }

    if (tiles.isEmpty) {
      tiles.add(Text('Nenhuma informação foi adicionada.'));
    }

    return tiles;
  }

  _launchURL(BuildContext context, String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      NotificationService.error(context, 'Erro', 'Não foi possível abrir este link');
    }
  }

  @override
  Widget build(BuildContext context) {
    final infoTiles = _buildTilesArray(context);
    return Padding(
      padding: EdgeInsets.all(16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Text('Informações gerais', style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
          SizedBox(height: 8),
          ListView.separated(
            physics: NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: infoTiles.length,
            itemBuilder: (context, index) => infoTiles[index],
            separatorBuilder: (context, index) => Divider(),
          )
        ],
      ),
    );
  }
}
