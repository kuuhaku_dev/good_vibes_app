import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:good_vibes_app/interfaces/interfaces.dart';
import 'package:good_vibes_app/models/models.dart';
import 'package:good_vibes_app/services/auth.dart';
import 'package:good_vibes_app/shared/shared.dart';

class GoodCommentList extends StatelessWidget {
  const GoodCommentList({Key key, @required this.iCommentable}) : super(key: key);
  final ICommentable iCommentable;

  @override
  Widget build(BuildContext context) {
    final length = iCommentable?.last5Comments?.length ?? 0;
    return SafeArea(
      top: false,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Row(
            children: <Widget>[
              Text(
                'Comentários',
                style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                ),
              ),
              Spacer(),
              if (length > 0)
                GestureDetector(
                  onTap: () => Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (_) => CommentsScreen(
                        targetUid: iCommentable.uid,
                      ),
                    ),
                  ),
                  child: Text(
                    'Ver todos',
                    style: TextStyle(
                      fontSize: 12,
                      fontWeight: FontWeight.bold,
                      color: Theme.of(context).primaryColor,
                    ),
                  ),
                ),
            ],
          ),
          SizedBox(height: 8),
          length != 0
              ? ListView.separated(
                  padding: EdgeInsets.all(0),
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  itemCount: length,
                  itemBuilder: (BuildContext context, int index) =>
                      GoodCommentTile(data: iCommentable.last5Comments[index]),
                  separatorBuilder: (BuildContext context, int index) => Divider(),
                )
              : Text(
                  'Ainda não foi feito nenhum comentário',
                  style: TextStyle(
                    fontWeight: FontWeight.w300,
                    fontSize: 12,
                  ),
                ),
          Divider(),
          FlatButton(
            child: Row(
              children: <Widget>[
                Icon(
                  FontAwesomeIcons.featherAlt,
                  size: 16,
                  color: Theme.of(context).primaryColor,
                ),
                SizedBox(width: 8),
                Text(
                  'Fazer comentário',
                  style: TextStyle(
                    fontSize: 16,
                    color: Theme.of(context).primaryColor,
                  ),
                ),
              ],
            ),
            onPressed: () {
              if (AuthService.isUserRegistered(context)) {
                Navigator.of(context).push(MaterialPageRoute(
                  builder: (_) => GoodCommentScreen(
                    targetUid: iCommentable.uid,
                    productType: GoodComment.TYPE_BLOG,
                  ),
                  fullscreenDialog: true,
                ));
              }
            },
          ),
        ],
      ),
    );
  }
}
