import 'package:flutter/material.dart';
import 'package:good_vibes_app/interfaces/interfaces.dart';
import 'package:good_vibes_app/shared/shared.dart';

class GoodItemPhotosSliverList extends StatelessWidget {
  const GoodItemPhotosSliverList({Key key, this.tabKey, this.iImageable}) : super(key: key);
  final Key tabKey;
  final IImageable iImageable;

  @override
  Widget build(BuildContext context) {
    bool hasPhotos = iImageable?.extraImages?.isNotEmpty ?? false;
    return SliverList(
      key: tabKey,
      delegate: SliverChildListDelegate(
        [
          Padding(
            padding: EdgeInsets.only(top: 8, bottom: 24),
            child: SafeArea(
              top: false,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      SizedBox(width: 16),
                      Text(
                        'Fotos',
                        style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      Spacer(),
                      if (hasPhotos)
                        GestureDetector(
                          onTap: () => Navigator.of(context).push(
                            MaterialPageRoute(
                              builder: (_) => GoodGallery(
                                iImageable: iImageable,
                              ),
                            ),
                          ),
                          child: Text(
                            'Ver todos',
                            style: TextStyle(
                              fontSize: 12,
                              fontWeight: FontWeight.bold,
                              color: Theme.of(context).primaryColor,
                            ),
                          ),
                        ),
                      if (hasPhotos) SizedBox(width: 16),
                    ],
                  ),
                  SizedBox(height: 16),
                  hasPhotos
                      ? GoodExtraImageHorizontalScroll(iImageable: iImageable)
                      : Row(
                          children: <Widget>[
                            SizedBox(width: 16),
                            Text(
                              'Nenhuma foto disponível',
                              style: TextStyle(
                                fontWeight: FontWeight.w300,
                                fontSize: 12,
                              ),
                            ),
                          ],
                        ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
