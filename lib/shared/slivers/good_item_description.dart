import 'package:flutter/material.dart';
import 'package:good_vibes_app/interfaces/interfaces.dart';
import 'package:intl/intl.dart';

class GoodItemDescription extends StatelessWidget {
  const GoodItemDescription({
    Key key,
    @required this.item,
  }) : super(key: key);
  final IDescriptionable item;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Text(item.title.isNotEmpty ? item.title : 'Loja',
              style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold)),
          SizedBox(height: 8),
          Row(
            children: <Widget>[
              Icon(Icons.location_on, size: 16),
              SizedBox(width: 4),
              Text(item.address.isNotEmpty ? item.address : 'Nenhum endereço registrado',
                  style: TextStyle(fontSize: 12, fontWeight: FontWeight.w300)),
            ],
          ),
          SizedBox(height: 4),
          item.startDate != null
              ? Row(
                  children: <Widget>[
                    Icon(Icons.calendar_today, size: 16),
                    SizedBox(width: 4),
                    Text('${DateFormat.MMMMd().format(item.startDate)} às ${DateFormat.Hm().format(item.startDate)}',
                        style: TextStyle(fontSize: 12, fontWeight: FontWeight.w300)),
                  ],
                )
              : Container(),
          SizedBox(height: 8),
          Text(
            item.description.isNotEmpty ? item.description : 'Nenhuma descrição',
            style: TextStyle(fontSize: 16, fontWeight: FontWeight.w300),
          )
        ],
      ),
    );
  }
}
