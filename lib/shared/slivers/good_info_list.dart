import 'package:flutter/material.dart';
import 'package:good_vibes_app/models/models.dart';
import 'package:good_vibes_app/shared/shared.dart';

class GoodInfoList extends StatelessWidget {
  const GoodInfoList({
    Key key,
    @required this.infos,
  }) : super(key: key);

  final List<GoodInfo> infos;

  @override
  Widget build(BuildContext context) {
    return infos.isNotEmpty
        ? Stack(
            children: <Widget>[
              ListView.builder(
                padding: EdgeInsets.all(0),
                itemCount: infos.length,
                itemBuilder: (context, index) => GoodInfoListTile(
                  info: infos[index],
                  currentRanking: index,
                  infosLength: infos.length,
                ),
              ),
            ],
          )
        : Container(
            height: MediaQuery.of(context).size.height - 250 - kBottomNavigationBarHeight,
            child: Center(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Text('Não publicamos nenhuma notícia'),
                  SizedBox(height: 16),
                  Text('Você será informado quando publicarmos 😃'),
                ],
              ),
            ),
          );
  }
}
