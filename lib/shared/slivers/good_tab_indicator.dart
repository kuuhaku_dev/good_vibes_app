import 'package:flutter/material.dart';

class GoodTabIndicator extends Decoration {
  final Color color;

  GoodTabIndicator({this.color = Colors.blue});

  @override
  _CustomPainter createBoxPainter([onChanged]) {
    return new _CustomPainter(this, onChanged);
  }
}

class _CustomPainter extends BoxPainter {
  _CustomPainter(this.decoration, VoidCallback onChanged)
      : assert(decoration != null),
        super(onChanged);

  final GoodTabIndicator decoration;

  @override
  void paint(Canvas canvas, Offset offset, ImageConfiguration configuration) {
    assert(configuration != null);
    assert(configuration.size != null);

    final Rect rect = Offset(offset.dx, configuration.size.height / 2 + 10) & Size(configuration.size.width, 4);

    final Paint paint = Paint();
    paint.color = decoration.color;
    paint.style = PaintingStyle.fill;
    canvas.drawRRect(RRect.fromRectAndRadius(rect, Radius.circular(2)), paint);
  }
}
