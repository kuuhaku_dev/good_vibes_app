import 'package:flutter/material.dart';
import 'package:good_vibes_app/interfaces/interfaces.dart';
import 'package:good_vibes_app/models/models.dart';
import 'package:good_vibes_app/shared/shared.dart';

class GoodEventList extends StatelessWidget {
  const GoodEventList({
    Key key,
    @required this.data,
    this.currentLocation,
  }) : super(key: key);

  final List<GoodEvent> data;
  final ILocationable currentLocation;

  @override
  Widget build(BuildContext context) {
    return data.isNotEmpty
        ? SingleChildScrollView(
            child: Column(children: [
              SizedBox(height: 16),
              if (currentLocation != null)
                Container(
                  margin: EdgeInsets.zero,
                  color: Colors.white,
                  child: Container(
                    padding: EdgeInsets.all(16),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          'Explore os arredores',
                          style: TextStyle(
                            fontSize: 24,
                            fontWeight: FontWeight.w900,
                          ),
                        ),
                        SizedBox(height: 16),
                        GoodMapNearbyCard(
                          currentLocation: currentLocation,
                          iLocationables: data,
                        ),
                      ],
                    ),
                  ),
                ),
              if (currentLocation != null) SizedBox(height: 16),
              GoodEventCardSection(events: data),
            ]),
          )
        : Container(
            height: MediaQuery.of(context).size.height - 250 - kBottomNavigationBarHeight,
            child: Center(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Text('Essa categoria está vazia'),
                  SizedBox(height: 16),
                  Text('Ajude-nos adicionando novos itens 😃'),
                ],
              ),
            ),
          );
  }
}
