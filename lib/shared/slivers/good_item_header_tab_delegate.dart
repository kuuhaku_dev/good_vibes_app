import 'package:flutter/material.dart';
import 'package:good_vibes_app/shared/shared.dart';

class ItemHeaderTabDelegate extends SliverPersistentHeaderDelegate {
  ItemHeaderTabDelegate({Key key, this.onTap, this.hasReviews = false});

  final double height = 64;
  final Function onTap;
  final bool hasReviews;

  @override
  double get maxExtent => height;

  @override
  double get minExtent => height;

  @override
  Widget build(BuildContext context, double shrinkOffset, bool overlapsContent) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border(
          bottom: BorderSide(color: Colors.grey),
        ),
      ),
      height: height,
      child: _EventTabBar(
        onTap: onTap,
        hasReviews: hasReviews,
      ),
    );
  }

  @override
  bool shouldRebuild(SliverPersistentHeaderDelegate oldDelegate) {
    return false;
  }
}

class _EventTabBar extends StatefulWidget {
  _EventTabBar({Key key, this.onTap, this.hasReviews = false}) : super(key: key);
  final Function onTap;
  final bool hasReviews;

  @override
  _EventTabBarState createState() => _EventTabBarState();
}

class _EventTabBarState extends State<_EventTabBar> with SingleTickerProviderStateMixin {
  TabController tabController;

  @override
  void initState() {
    super.initState();
    int length = widget.hasReviews ? 3 : 2;
    tabController = TabController(length: length, vsync: this);
  }

  @override
  void dispose() {
    tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final double fontSize = widget.hasReviews ? 13 : 16;
    final TextStyle tabTextStyle = TextStyle(fontSize: fontSize, fontWeight: FontWeight.w900);

    return TabBar(
      controller: tabController,
      labelColor: Theme.of(context).primaryColor,
      unselectedLabelColor: Theme.of(context).primaryColor.withOpacity(0.3),
      indicatorSize: TabBarIndicatorSize.label,
      indicator: GoodTabIndicator(color: Theme.of(context).primaryColor),
      onTap: (int index) {
        if (widget.onTap != null) {
          widget.onTap(index);
        }
      },
      tabs: <Widget>[
        Text('Visão Geral', style: tabTextStyle),
        Text('Fotos', style: tabTextStyle),
        if (widget.hasReviews) Text('Avaliações', style: tabTextStyle),
      ],
    );
  }
}
