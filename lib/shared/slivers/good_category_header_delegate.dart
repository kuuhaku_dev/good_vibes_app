import 'package:flutter/material.dart';

class GoodCategoryHeaderDelegate extends SliverPersistentHeaderDelegate {
  GoodCategoryHeaderDelegate({
    this.context,
    this.collapsedHeight,
    this.expandedHeight,
    this.paddingTop,
    this.coverImgUrl,
    this.title,
    this.subtitle,
    this.showSubheader = true,
  });

  final BuildContext context;
  final double collapsedHeight;
  final double expandedHeight;
  final double paddingTop;
  final String coverImgUrl;
  final String title;
  final String subtitle;
  final bool showSubheader;

  @override
  double get maxExtent => this.expandedHeight;

  @override
  double get minExtent => this.collapsedHeight + this.paddingTop;

  @override
  bool shouldRebuild(SliverPersistentHeaderDelegate oldDelegate) {
    return true;
  }

  @override
  Widget build(BuildContext context, double shrinkOffset, bool overlapsContent) {
    final offsetPercentage = shrinkOffset / (maxExtent - minExtent);
    final transparency = offsetPercentage > 1.0 ? 1.0 : offsetPercentage;

    return Container(
      height: this.maxExtent,
      width: MediaQuery.of(context).size.width,
      child: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Container(child: Image.asset(coverImgUrl, fit: BoxFit.cover)),
          SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                SizedBox(height: expandedHeight / 2 - 32),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                  decoration: BoxDecoration(
                    color: Theme.of(context).primaryColor,
                    borderRadius: BorderRadius.circular(32),
                  ),
                  child: Text(
                    title,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 24,
                      fontWeight: FontWeight.w500,
                      color: Colors.white,
                    ),
                  ),
                ),
                SizedBox(height: 24),
                if (showSubheader)
                  Opacity(
                    opacity: 0.7,
                    child: Container(
                      padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                      decoration: BoxDecoration(
                        color: Theme.of(context).primaryColor,
                        borderRadius: BorderRadius.circular(32),
                      ),
                      child: Text(
                        subtitle ?? 'Veja o que há por perto',
                        style: TextStyle(
                          fontSize: 12,
                          fontWeight: FontWeight.w300,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
              ],
            ),
          ),
          Positioned(
            left: 0,
            right: 0,
            top: 0,
            child: Container(
              color: Color.lerp(Colors.transparent, Theme.of(context).primaryColor, transparency),
              child: SafeArea(
                bottom: false,
                child: Container(
                  height: this.collapsedHeight,
                  alignment: Alignment.center,
                  child: Text(
                    this.title,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.w500,
                      color: Color.lerp(Colors.transparent, Colors.white, transparency),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
