import 'package:flutter/material.dart';
import 'package:good_vibes_app/interfaces/interfaces.dart';
import 'package:good_vibes_app/shared/shared.dart';

class GoodItemGeneralSliverList extends StatelessWidget {
  const GoodItemGeneralSliverList(
      {Key key, this.tabKey, @required this.iDescriptionable, @required this.iInformationable, this.iLocationable})
      : super(key: key);
  final Key tabKey;
  final IDescriptionable iDescriptionable;
  final IInformationable iInformationable;
  final ILocationable iLocationable;

  @override
  Widget build(BuildContext context) {
    return SliverList(
      key: tabKey,
      delegate: SliverChildListDelegate(
        [
          GoodItemDescription(item: iDescriptionable),
          GoodItemInformation(
            iInformationable: iInformationable,
            iLocationable: iLocationable,
          ),
        ],
      ),
    );
  }
}
