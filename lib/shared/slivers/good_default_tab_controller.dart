import 'package:flutter/material.dart';
import 'package:good_vibes_app/interfaces/interfaces.dart';
import 'package:good_vibes_app/shared/shared.dart';

class GoodDefaultTabController extends StatelessWidget {
  const GoodDefaultTabController({
    Key key,
    @required this.data,
    @required this.types,
    @required this.builder,
    @required this.title,
    @required this.coverImgUrl,
    this.subtitle,
    this.showSubheader = true,
  }) : super(key: key);

  final String title;
  final String subtitle;
  final String coverImgUrl;
  final bool showSubheader;
  final List data;
  final List<ITypeable> types;
  final Widget Function(int index) builder;

  @override
  Widget build(BuildContext context) {
    final TextStyle tabTextStyle = TextStyle(fontSize: 16, fontWeight: FontWeight.w900);

    return DefaultTabController(
      length: types.length,
      child: NestedScrollView(
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          return <Widget>[
            SliverPersistentHeader(
              pinned: true,
              delegate: GoodCategoryHeaderDelegate(
                title: title,
                subtitle: subtitle,
                coverImgUrl: coverImgUrl,
                collapsedHeight: kToolbarHeight,
                expandedHeight: 250,
                paddingTop: MediaQuery.of(context).padding.top,
                context: context,
                showSubheader: showSubheader,
              ),
            ),
            SliverPersistentHeader(
              pinned: true,
              delegate: SliverAppBarDelegate(
                TabBar(
                  isScrollable: true,
                  labelColor: Theme.of(context).primaryColor,
                  unselectedLabelColor: Theme.of(context).primaryColor.withOpacity(0.3),
                  indicatorSize: TabBarIndicatorSize.label,
                  indicator: GoodTabIndicator(color: Theme.of(context).primaryColor),
                  tabs: List.generate(
                    types.length,
                    (int index) => Tab(
                      child: Text(
                        types[index].title,
                        style: tabTextStyle,
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ];
        },
        body: TabBarView(
          children: List.generate(types.length, builder),
        ),
      ),
    );
  }
}
