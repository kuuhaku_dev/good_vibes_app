import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:good_vibes_app/interfaces/interfaces.dart';
import 'package:good_vibes_app/models/models.dart';
import 'package:good_vibes_app/services/auth.dart';
import 'package:good_vibes_app/shared/shared.dart';

class GoodItemReviewsSliverList extends StatelessWidget {
  const GoodItemReviewsSliverList({Key key, this.tabKey, @required this.iReviewable, @required this.service})
      : super(key: key);
  final Key tabKey;
  final IReviewable iReviewable;
  final GoodService service;

  @override
  Widget build(BuildContext context) {
    return SliverList(
      key: tabKey,
      delegate: SliverChildListDelegate(
        [
          Padding(
            padding: EdgeInsets.all(16),
            child: SafeArea(
              top: false,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Text(
                        'Avaliações',
                        style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      Spacer(),
                      if (iReviewable.reviewsCount > 0)
                        GestureDetector(
                          onTap: () => Navigator.of(context).push(
                            MaterialPageRoute(
                              builder: (_) => ReviewsScreen(
                                targetUid: iReviewable.uid,
                              ),
                            ),
                          ),
                          child: Text(
                            'Ver todos',
                            style: TextStyle(
                              fontSize: 12,
                              fontWeight: FontWeight.bold,
                              color: Theme.of(context).primaryColor,
                            ),
                          ),
                        ),
                    ],
                  ),
                  SizedBox(height: 8),
                  GoodRatingRow(rating: iReviewable.averageRating, total: iReviewable.reviewsCount),
                  GoodRankingText(),
                  SizedBox(height: 8),
                  Text(
                    'Pontuação',
                    style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  SizedBox(height: 8),
                  GoodDetailedRatings(iReviewable: iReviewable),
                  SizedBox(height: 8),
                  Divider(),
                  ListView.separated(
                    padding: EdgeInsets.all(0),
                    physics: NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemCount: iReviewable.last5Reviews.length,
                    itemBuilder: (BuildContext context, int index) =>
                        GoodReviewTile(item: iReviewable.last5Reviews[index]),
                    separatorBuilder: (BuildContext context, int index) => Divider(),
                  ),
                  Divider(),
                  FlatButton(
                    child: Row(
                      children: <Widget>[
                        Icon(
                          FontAwesomeIcons.featherAlt,
                          size: 16,
                          color: Theme.of(context).primaryColor,
                        ),
                        SizedBox(width: 8),
                        Text(
                          'Fazer avaliação',
                          style: TextStyle(
                            fontSize: 16,
                            color: Theme.of(context).primaryColor,
                          ),
                        ),
                      ],
                    ),
                    onPressed: () {
                      if (AuthService.isUserRegistered(context)) {
                        Navigator.of(context).push(MaterialPageRoute(
                          builder: (_) => GoodReviewScreen(
                            targetUid: service.uid,
                            productType: GoodReview.TYPE_SERVICE,
                          ),
                          fullscreenDialog: true,
                        ));
                      }
                    },
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
