import 'package:flutter/material.dart';
import 'package:good_vibes_app/models/models.dart';
import 'package:good_vibes_app/shared/shared.dart';

class GoodUserServiceList extends StatelessWidget {
  const GoodUserServiceList({
    Key key,
    @required this.services,
  }) : super(key: key);

  final List<GoodService> services;

  @override
  Widget build(BuildContext context) {
    return services.isNotEmpty
        ? ListView.builder(
            padding: EdgeInsets.all(0),
            itemCount: services.length,
            itemBuilder: (context, index) => GoodServiceListTile(
              service: services[index],
              currentRanking: index,
              servicesLength: services.length,
              isEditable: true,
            ),
          )
        : Container(
            height: MediaQuery.of(context).size.height - 250 - kBottomNavigationBarHeight,
            child: Center(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Text('Nenhum serviço cadastrado'),
                  SizedBox(height: 16),
                  Text('Crie-os na aba de serviços 😃'),
                ],
              ),
            ),
          );
  }
}
