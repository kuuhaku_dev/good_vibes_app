import 'package:flutter/material.dart';
import 'package:good_vibes_app/models/models.dart';
import 'package:good_vibes_app/shared/shared.dart';

class GoodServiceList extends StatelessWidget {
  const GoodServiceList({
    Key key,
    @required this.services,
  }) : super(key: key);

  final List<GoodService> services;

  Positioned _buildOptionsMenu(BuildContext context) {
    final double width = 120;
    final textStyle = TextStyle(fontSize: 16);

    return Positioned(
      bottom: 16,
      left: MediaQuery.of(context).size.width / 2 - width / 2,
      child: Container(
        height: 36,
        width: width,
        decoration: BoxDecoration(
          color: Colors.white,
          border: Border.all(color: Theme.of(context).primaryColor, width: 2),
          borderRadius: BorderRadius.all(Radius.circular(32)),
        ),
        child: Row(
          children: <Widget>[
            Expanded(
              child: ClipRRect(
                borderRadius: BorderRadius.horizontal(left: Radius.circular(32), right: Radius.circular(32)),
                child: MaterialButton(
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Icon(Icons.map),
                      SizedBox(width: 8),
                      Text('Mapa', style: textStyle),
                    ],
                  ),
                  onPressed: () => Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (_) => GoodMapScreen(
                        iLocationables: services,
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return services.isNotEmpty
        ? Stack(
            children: <Widget>[
              ListView.builder(
                padding: EdgeInsets.all(0),
                itemCount: services.length,
                itemBuilder: (context, index) => GoodServiceListTile(
                  service: services[index],
                  currentRanking: index,
                  servicesLength: services.length,
                ),
              ),
              _buildOptionsMenu(context)
            ],
          )
        : Container(
            height: MediaQuery.of(context).size.height - 250 - kBottomNavigationBarHeight,
            child: Center(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Text('Essa categoria está vazia'),
                  SizedBox(height: 16),
                  Text('Ajude-nos adicionando novos itens 😃'),
                ],
              ),
            ),
          );
  }
}
