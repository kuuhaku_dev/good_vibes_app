import 'package:flutter/material.dart';
import 'package:good_vibes_app/models/models.dart';
import 'package:good_vibes_app/screens/screens.dart';
import 'package:good_vibes_app/services/services.dart';

class GoodUserTile extends StatelessWidget {
  const GoodUserTile({Key key, @required this.user}) : super(key: key);
  final GoodUser user;

  String get initials {
    if (user != null && user.name.isNotEmpty) {
      List<String> nameParts = user.name.split(' ');
      String firstInitial = nameParts.first[0];
      String lastInitial = nameParts.last[0];
      return "$firstInitial$lastInitial";
    } else {
      return 'A';
    }
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        if (AuthService.isUserRegistered(context)) {
          Navigator.of(context).push(MaterialPageRoute(builder: (_) => UserDetailsScreen()));
        }
      },
      child: Container(
        height: 80,
        decoration: BoxDecoration(
          color: Colors.white,
          border: Border(
            top: BorderSide(color: Colors.grey),
            bottom: BorderSide(color: Colors.grey),
          ),
        ),
        child: Row(
          children: <Widget>[
            Container(
              width: 56,
              child: CircleAvatar(
                radius: 16,
                child: (user != null && user.photoUrl.isNotEmpty)
                    ? ClipRRect(borderRadius: BorderRadius.circular(16), child: Image.network(user.photoUrl))
                    : Text(initials),
              ),
            ),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    user?.name ?? 'Nome',
                    style: Theme.of(context).textTheme.subhead,
                  ),
                  Text(
                    'Alterar meus dados pessoais',
                    style: Theme.of(context).textTheme.body1.copyWith(color: Theme.of(context).textTheme.caption.color),
                  ),
                ],
              ),
            ),
            Icon(Icons.chevron_right),
            SizedBox(width: 8),
          ],
        ),
      ),
    );
  }
}
