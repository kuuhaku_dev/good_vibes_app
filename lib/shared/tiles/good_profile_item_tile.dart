import 'package:flutter/material.dart';

class GoodProfileItemTile extends StatelessWidget {
  const GoodProfileItemTile({Key key, this.leading, this.title, this.subtitle, this.trailing, this.onTap})
      : super(key: key);
  final Widget leading;
  final String title;
  final String subtitle;
  final Widget trailing;
  final Function onTap;

  Widget _buildTitle(BuildContext context) {
    if (title != null) {
      return Text(
        title,
        style: Theme.of(context).textTheme.subhead,
      );
    } else {
      return Container();
    }
  }

  Widget _buildSubtitle(BuildContext context) {
    if (subtitle != null) {
      return Text(
        subtitle,
        style: Theme.of(context).textTheme.body1.copyWith(color: Theme.of(context).textTheme.caption.color),
      );
    } else {
      return Container();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 48,
      child: Material(
        child: InkWell(
          onTap: onTap,
          child: Row(
            children: <Widget>[
              Container(
                width: 56,
                child: leading != null ? leading : Container(),
              ),
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    _buildTitle(context),
                    _buildSubtitle(context),
                  ],
                ),
              ),
              Container(
                child: trailing,
              ),
              SizedBox(width: 8),
            ],
          ),
        ),
      ),
    );
  }
}
