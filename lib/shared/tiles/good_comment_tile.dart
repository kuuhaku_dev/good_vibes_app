import 'package:flutter/material.dart';
import 'package:good_vibes_app/models/models.dart';
import 'package:good_vibes_app/services/services.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class GoodCommentTile extends StatelessWidget {
  const GoodCommentTile({Key key, @required this.data}) : super(key: key);
  final GoodComment data;

  @override
  Widget build(BuildContext context) {
    final GoodUser user = Provider.of<GoodUser>(context);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Row(
          children: <Widget>[
            Text(
              data.title,
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            Spacer(),
            if (user != null && user.uid == data.ownerUid)
              GestureDetector(
                onTap: () async {
                  if (await NotificationService.confirm(
                      context, 'Apagar comentário', 'Deseja realmente apagar esse comentário?')) {
                    Document commentRef = Document<GoodComment>(path: '${GoodComment.COL_PATH}/${data.uid}');
                    commentRef.delete();
                    NotificationService.showSnackBar('Seu comentário foi apagado');
                  }
                },
                child: Text(
                  'Apagar',
                  style: TextStyle(
                    fontSize: 12,
                    fontWeight: FontWeight.bold,
                    color: Theme.of(context).errorColor,
                  ),
                ),
              ),
          ],
        ),
        SizedBox(height: 8),
        Text(data.text),
        SizedBox(height: 16),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(
              data.name,
              style: TextStyle(fontSize: 12, fontWeight: FontWeight.w300),
            ),
            Text(
              DateFormat.yMMMMd().format(data.createdAt),
              style: TextStyle(fontSize: 12, fontWeight: FontWeight.w300),
            ),
          ],
        ),
      ],
    );
  }
}
