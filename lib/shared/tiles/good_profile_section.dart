import 'package:flutter/material.dart';

class GoodProfileSection extends StatelessWidget {
  const GoodProfileSection({Key key, this.children = const <Widget>[], this.hasIndentedSeparator = true})
      : super(key: key);

  final List<Widget> children;
  final bool hasIndentedSeparator;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border(
          top: BorderSide(color: Colors.grey),
          bottom: BorderSide(color: Colors.grey),
        ),
      ),
      child: ListView.separated(
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        itemCount: children.length,
        itemBuilder: (BuildContext context, int index) {
          return children[index];
        },
        separatorBuilder: (BuildContext context, int index) {
          return Divider(
            height: 1,
            thickness: 1,
            indent: hasIndentedSeparator ? 56 : 0,
          );
        },
      ),
      // child: Column(
      //   children: children,
      // ),
    );
  }
}
