import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:good_vibes_app/interfaces/interfaces.dart';
import 'package:good_vibes_app/shared/cards/good_map_card.dart';

class GoodInformationTile extends StatelessWidget {
  const GoodInformationTile({
    Key key,
    @required this.iconData,
    @required this.title,
    @required this.onTap,
    this.hasChevron = true,
    this.isBoldTitle = false,
    this.iLocationable,
  }) : super(key: key);

  final VoidCallback onTap;
  final IconData iconData;
  final String title;
  final bool isBoldTitle;
  final bool hasChevron;
  final ILocationable iLocationable;

  @override
  Widget build(BuildContext context) {
    final TextStyle titleStyle =
        isBoldTitle ? TextStyle(fontWeight: FontWeight.bold, color: Theme.of(context).primaryColor) : TextStyle();

    return GestureDetector(
      onTap: onTap,
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: 8.0),
        child: Column(
          children: <Widget>[
            GoodMapCard(iLocationable: iLocationable),
            Row(
              children: <Widget>[
                Icon(
                  iconData,
                  color: Theme.of(context).primaryColor,
                ),
                SizedBox(width: 16),
                Expanded(
                  child: Text(
                    title,
                    style: titleStyle,
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
                hasChevron ? Icon(FontAwesomeIcons.chevronRight, color: Theme.of(context).primaryColor) : Container(),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
