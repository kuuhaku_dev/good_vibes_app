import 'package:flutter/material.dart';
import 'package:good_vibes_app/models/models.dart';
import 'package:good_vibes_app/screens/screens.dart';
import 'package:good_vibes_app/shared/shared.dart';
import 'package:jiffy/jiffy.dart';

class GoodInfoListTile extends StatelessWidget {
  const GoodInfoListTile({Key key, @required this.info, this.currentRanking, this.infosLength}) : super(key: key);
  final GoodInfo info;
  final int currentRanking;
  final int infosLength;

  static const double _IMAGE_HEIGHT = 78;
  static const double _IMAGE_WIDTH = 96;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Navigator.of(context).push(MaterialPageRoute(builder: (_) => InfoScreen(info: info))),
      child: Column(
        children: <Widget>[
          Container(
            padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
            color: Colors.white,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                ClipRRect(
                  borderRadius: BorderRadius.circular(8),
                  child: GoodCachedImage(
                    imageUrl: info.imageUrl,
                    height: _IMAGE_HEIGHT,
                    width: _IMAGE_WIDTH,
                  ),
                ),
                SizedBox(width: 16),
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Text(
                            info.subtype ?? 'Geral',
                            style: TextStyle(
                              color: Theme.of(context).primaryColor,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          Spacer(),
                          Text(
                            Jiffy(info.updatedAt).fromNow(),
                            style: Theme.of(context).textTheme.caption,
                          ),
                          Icon(Icons.chevron_right, color: Theme.of(context).textTheme.caption.color),
                        ],
                      ),
                      Text(
                        info.title,
                        maxLines: 3,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(fontSize: 16, fontWeight: FontWeight.w700),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
          Container(
            color: Colors.white,
            child: Divider(
              height: 1,
              indent: 16,
              endIndent: 16,
            ),
          ),
        ],
      ),
    );
  }
}
