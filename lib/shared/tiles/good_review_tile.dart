import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:good_vibes_app/models/models.dart';
import 'package:good_vibes_app/services/services.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class GoodReviewTile extends StatelessWidget {
  const GoodReviewTile({Key key, @required this.item}) : super(key: key);
  final GoodReviewSummary item;

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<GoodUser>(context);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Row(
          children: <Widget>[
            RatingBarIndicator(
              rating: item.rating,
              itemBuilder: (context, index) => Icon(
                Icons.star,
                color: Theme.of(context).primaryColor,
              ),
              itemCount: 5,
              itemSize: 12.0,
            ),
            SizedBox(width: 8),
            Text(
              item.title,
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            Spacer(),
            if (user != null && user.uid == item.ownerUid)
              GestureDetector(
                onTap: () async {
                  if (await NotificationService.confirm(
                      context, 'Apagar avaliação', 'Deseja realmente apagar essa avaliação?')) {
                    Document reviewRef =
                        Document<GoodComment>(path: '${GoodReview.COL_PATH}/${item.ownerUid}_${item.targetUid}');
                    reviewRef.delete();
                    NotificationService.showSnackBar('Sua avaliação foi apagada');
                  }
                },
                child: Text(
                  'Apagar',
                  style: TextStyle(
                    fontSize: 12,
                    fontWeight: FontWeight.bold,
                    color: Theme.of(context).errorColor,
                  ),
                ),
              ),
          ],
        ),
        SizedBox(height: 8),
        Text(item.text),
        SizedBox(height: 16),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(
              item.name,
              style: TextStyle(fontSize: 12, fontWeight: FontWeight.w300),
            ),
            Text(
              DateFormat.yMMMMd().format(item.createdAt),
              style: TextStyle(fontSize: 12, fontWeight: FontWeight.w300),
            ),
          ],
        ),
      ],
    );
  }
}
