import 'package:flutter/material.dart';
import 'package:good_vibes_app/models/models.dart';
import 'package:good_vibes_app/shared/shared.dart';

class GoodEventCardSection extends StatelessWidget {
  const GoodEventCardSection({Key key, @required this.events, this.isScrollable = false, this.isEditable = false})
      : super(key: key);
  final List<GoodEvent> events;
  final bool isScrollable;
  final bool isEditable;

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      physics: isScrollable ? AlwaysScrollableScrollPhysics() : NeverScrollableScrollPhysics(),
      shrinkWrap: isScrollable ? false : true,
      itemCount: events?.length ?? 0,
      itemBuilder: (context, index) => GoodEventCard(event: events[index], isEditable: isEditable),
      separatorBuilder: (context, index) => SizedBox(height: 16),
    );
  }
}
