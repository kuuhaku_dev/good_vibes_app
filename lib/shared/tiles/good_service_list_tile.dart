import 'package:flutter/material.dart';
import 'package:good_vibes_app/models/models.dart';
import 'package:good_vibes_app/screens/screens.dart';
import 'package:good_vibes_app/services/services.dart';
import 'package:good_vibes_app/shared/shared.dart';

class GoodServiceListTile extends StatelessWidget {
  const GoodServiceListTile({Key key, @required this.service, this.currentRanking, this.servicesLength, this.isEditable=false})
      : super(key: key);
  final GoodService service;
  final int currentRanking;
  final int servicesLength;
  final bool isEditable;

  static const double _IMAGE_HEIGHT = 78;
  static const double _IMAGE_WIDTH = 96;

  Widget _buildEditRow() {
    if (isEditable) {
      return Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          service.isEnabled
              ? Text(
                  'ATIVO',
                  style: TextStyle(fontWeight: FontWeight.bold, color: Colors.green[200]),
                )
              : Text(
                  'INATIVO',
                  style: TextStyle(fontWeight: FontWeight.bold, color: Colors.redAccent[200]),
                ),
          IconButton(
            icon: Icon(Icons.edit),
            onPressed: () => NavigationService.instance.push(MaterialPageRoute(builder: (_) => ServiceEditScreen(service: service))),
          ),
        ],
      );
    } else {
      return Container();
    }
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Navigator.of(context).push(MaterialPageRoute(builder: (_) => ServiceScreen(service: service))),
      child: Column(
        children: <Widget>[
          Container(
            padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
            color: Colors.white,
            child: Row(
              children: <Widget>[
                ClipRRect(
                  borderRadius: BorderRadius.circular(8),
                  child: GoodCachedImage(
                    imageUrl: service.heroImage,
                    height: _IMAGE_HEIGHT,
                    width: _IMAGE_WIDTH,
                  ),
                ),
                SizedBox(width: 16),
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        service.title,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(fontSize: 16, fontWeight: FontWeight.w700),
                      ),
                      GoodRatingRow(
                          rating: service.stats['averageRating'].toDouble(), total: service.stats['reviewsCount']),
                      GoodRankingText(
                        currentRanking: currentRanking + 1,
                        servicesLength: servicesLength,
                        typeLabel: service.type?.toLowerCase(),
                      ),
                      SizedBox(height: 2),
                      GoodDistanceRow(iLocationable: service),
                      _buildEditRow(),
                    ],
                  ),
                )
              ],
            ),
          ),
          Container(
            color: Colors.white,
            child: Divider(
              height: 1,
              indent: 16,
              endIndent: 16,
            ),
          ),
        ],
      ),
    );
  }
}
