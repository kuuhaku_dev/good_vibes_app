import 'package:flutter/material.dart';

class GoodRankingText extends StatelessWidget {
  const GoodRankingText({Key key, this.currentRanking, this.servicesLength, this.typeLabel = ''}) : super(key: key);
  final int currentRanking;
  final int servicesLength;
  final String typeLabel;

  String get _text => (currentRanking == null || servicesLength == null)
      ? 'Ranqueamento indisponível'
      : 'Nº $currentRanking de $servicesLength em $typeLabel';

  @override
  Widget build(BuildContext context) {
    return Text(
      _text,
      style: TextStyle(fontSize: 12, fontWeight: FontWeight.w300),
    );
  }
}
