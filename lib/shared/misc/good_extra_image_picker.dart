import 'package:flutter/material.dart';
import 'package:good_vibes_app/interfaces/interfaces.dart';
import 'package:good_vibes_app/shared/shared.dart';
import 'package:multi_image_picker/multi_image_picker.dart';

class GoodExtraImagePicker extends StatefulWidget {
  const GoodExtraImagePicker({Key key, this.onSelect, this.iImageable}) : super(key: key);
  final Function(List<Asset> extraImages) onSelect;
  final IImageable iImageable;

  @override
  _GoodExtraImagePickerState createState() => _GoodExtraImagePickerState();
}

class _GoodExtraImagePickerState extends State<GoodExtraImagePicker> {
  List<Asset> extraImages = List<Asset>();

  static const double _SIZE = 96;
  static const double _PADDING = 16;

  Future<void> loadAssets() async {
    List<Asset> resultList;

    try {
      resultList = await MultiImagePicker.pickImages(
        selectedAssets: extraImages,
        maxImages: 10,
      );
    } catch (e) {
      print(e.message);
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      extraImages = resultList ?? List<Asset>();
      widget.onSelect(extraImages);
    });
  }

  @override
  Widget build(BuildContext context) {
    final iImageable = widget.iImageable;
    return GoodProfileSection(
      children: <Widget>[
        iImageable != null && iImageable.extraImagesUrls.length != 0
            ? GoodExtraImageHorizontalScroll(iImageable: iImageable)
            : Container(
                height: _SIZE,
                child: ListView.separated(
                  padding: EdgeInsets.all(_PADDING),
                  physics: BouncingScrollPhysics(),
                  scrollDirection: Axis.horizontal,
                  itemCount: extraImages.length + 1,
                  itemBuilder: (BuildContext context, int index) {
                    if (index == 0) {
                      return GestureDetector(
                        onTap: loadAssets,
                        child: Container(
                          height: _SIZE - _PADDING * 2,
                          width: _SIZE - _PADDING * 2,
                          color: Colors.grey[200],
                          child: Icon(
                            Icons.add_a_photo,
                            size: 32,
                          ),
                        ),
                      );
                    } else {
                      Asset asset = extraImages[index - 1];
                      return AssetThumb(
                        asset: asset,
                        width: (_SIZE - _PADDING * 2).toInt(),
                        height: (_SIZE - _PADDING * 2).toInt(),
                      );
                    }
                  },
                  separatorBuilder: (BuildContext context, int index) {
                    return SizedBox(
                      width: _PADDING,
                    );
                  },
                ),
              ),
      ],
    );
  }
}
