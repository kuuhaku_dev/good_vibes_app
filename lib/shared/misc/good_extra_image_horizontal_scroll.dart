import 'package:flutter/material.dart';
import 'package:good_vibes_app/interfaces/interfaces.dart';

class GoodExtraImageHorizontalScroll extends StatelessWidget {
  const GoodExtraImageHorizontalScroll({
    Key key,
    @required this.iImageable,
  }) : super(key: key);

  final IImageable iImageable;

  static const double _SIZE = 96;
  static const double _PADDING = 16;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: _SIZE,
      child: ListView.separated(
        padding: EdgeInsets.all(_PADDING),
        physics: BouncingScrollPhysics(),
        scrollDirection: Axis.horizontal,
        itemCount: iImageable?.extraImagesThumbs?.length ?? 0,
        itemBuilder: (BuildContext context, int index) {
          final url = iImageable.extraImagesThumbs[index];
          return Image.network(url);
        },
        separatorBuilder: (BuildContext context, int index) {
          return SizedBox(width: _PADDING);
        },
      ),
    );
  }
}
