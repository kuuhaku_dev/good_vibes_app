import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class GoodCachedImage extends StatelessWidget {
  final double height;
  final double width;

  const GoodCachedImage({
    Key key,
    @required this.imageUrl,
    this.height,
    this.width,
  }) : super(key: key);

  final String imageUrl;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      width: width,
      child: CachedNetworkImage(
        imageUrl: imageUrl,
        placeholder: (context, url) => Container(
          height: height,
          width: width,
          child: Center(
            child: CircularProgressIndicator(),
          ),
        ),
        errorWidget: (context, url, error) => Container(
          height: height,
          width: width,
          color: Theme.of(context).primaryColor,
          child: Center(
            child: Image.asset(
              'assets/icon/icon-colored.png',
              height: height * 0.75,
            ),
          ),
        ),
        fit: BoxFit.cover,
        alignment: Alignment.center,
      ),
    );
  }
}
