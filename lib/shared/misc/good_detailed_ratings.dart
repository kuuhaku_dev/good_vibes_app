import 'package:flutter/material.dart';
import 'package:good_vibes_app/interfaces/interfaces.dart';

class GoodDetailedRatings extends StatelessWidget {
  const GoodDetailedRatings({Key key, @required this.iReviewable}) : super(key: key);
  final IReviewable iReviewable;

  Widget _buildRatingRow(BuildContext context, String title, double percentage, int total) {
    return Row(
      children: <Widget>[
        Text(title),
        Spacer(),
        Container(
          width: MediaQuery.of(context).size.width * 0.45,
          child: LinearProgressIndicator(
            backgroundColor: Colors.grey[300],
            valueColor: AlwaysStoppedAnimation<Color>(Theme.of(context).primaryColor),
            value: percentage,
          ),
        ),
        SizedBox(width: 8),
        Container(
          width: MediaQuery.of(context).size.width * 0.1,
          child: Text('$total'),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    final double reviewsCount = iReviewable.reviewsCount == 0 ? 1.0 : iReviewable.reviewsCount.toDouble();
    return Container(
      child: Column(
        children: <Widget>[
          _buildRatingRow(context, 'Fantástico', (iReviewable.stats['star5'] ?? 0).toDouble() / reviewsCount,
              iReviewable.stats['star5'] ?? 0),
          _buildRatingRow(context, 'Muito bom', (iReviewable.stats['star4'] ?? 0).toDouble() / reviewsCount,
              iReviewable.stats['star4'] ?? 0),
          _buildRatingRow(context, 'Bom', (iReviewable.stats['star3'] ?? 0).toDouble() / reviewsCount,
              iReviewable.stats['star3'] ?? 0),
          _buildRatingRow(context, 'Razoável', (iReviewable.stats['star2'] ?? 0).toDouble() / reviewsCount,
              iReviewable.stats['star2'] ?? 0),
          _buildRatingRow(context, 'Precisa melhorar', (iReviewable.stats['star1'] ?? 0).toDouble() / reviewsCount,
              iReviewable.stats['star1'] ?? 0),
        ],
      ),
    );
  }
}
