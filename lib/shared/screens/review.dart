import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:good_vibes_app/models/models.dart';
import 'package:good_vibes_app/services/services.dart';
import 'package:provider/provider.dart';

class GoodReviewScreen extends StatefulWidget {
  GoodReviewScreen({Key key, @required this.targetUid, @required this.productType}) : super(key: key);
  final String targetUid;
  final String productType;

  @override
  GoodReviewScreenState createState() => GoodReviewScreenState();
}

class GoodReviewScreenState extends State<GoodReviewScreen> {
  final _formKey = GlobalKey<FormState>();

  double rating = 0;
  String title = '';
  String text = '';
  bool _autoValidate = false;
  bool _isLoading = false;

  void startLoading() => setState(() => _isLoading = true);
  void finishLoading() => setState(() => _isLoading = false);

  Text buildRatingText() {
    String prefix = 'Você classificou:';
    TextStyle style = TextStyle(color: Theme.of(context).primaryColor, fontWeight: FontWeight.w500);
    switch (rating.floor()) {
      case 1:
        return Text('$prefix Precisa melhorar', style: style);
      case 2:
        return Text('$prefix Razoável', style: style);
      case 3:
        return Text('$prefix Bom', style: style);
      case 4:
        return Text('$prefix Muito bom', style: style);
      case 5:
        return Text('$prefix Fantástico', style: style);
      default:
        return Text('Toque em um círculo para pontuar');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.close),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text('Nova Avaliação'),
        actions: <Widget>[
          FlatButton(
            textColor: Colors.white,
            shape: CircleBorder(side: BorderSide(color: Colors.transparent)),
            child: Text('Enviar'),
            onPressed: _validateInputs,
          )
        ],
      ),
      body: Form(
        key: _formKey,
        child: ListView(
          children: <Widget>[
            Container(
              color: Colors.grey[300],
              padding: EdgeInsets.all(8),
              child: Column(
                children: <Widget>[
                  buildRatingText(),
                  SizedBox(height: 8),
                  RatingBar(
                    initialRating: 0,
                    direction: Axis.horizontal,
                    allowHalfRating: false,
                    itemCount: 5,
                    itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
                    itemBuilder: (context, _) => Icon(
                      Icons.star,
                      color: Theme.of(context).primaryColor,
                    ),
                    onRatingUpdate: (value) {
                      setState(() {
                        rating = value;
                      });
                    },
                  ),
                ],
              ),
            ),
            Divider(
              height: 1,
              thickness: 1,
            ),
            ListTile(
              title: TextFormField(
                decoration: InputDecoration(
                  hintText: 'Título',
                  border: InputBorder.none,
                ),
                onSaved: (String value) {
                  title = value;
                },
                validator: _validateTitle,
                textInputAction: TextInputAction.unspecified,
              ),
            ),
            Divider(height: 1),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16),
              child: TextFormField(
                decoration: InputDecoration(
                  hintText: 'Avaliação',
                  border: InputBorder.none,
                  counter: Text('${text.length} (mínimo de 20)'),
                ),
                onChanged: (value) {
                  setState(() => text = value);
                },
                onSaved: (String value) {
                  text = value;
                },
                maxLines: 5,
                initialValue: '',
                validator: _validateText,
                keyboardType: TextInputType.multiline,
                textInputAction: TextInputAction.unspecified,
              ),
            ),
          ],
        ),
      ),
    );
  }

  String _validateTitle(String value) {
    if (value.isEmpty) {
      return "Insira um título";
    }

    return null;
  }

  String _validateText(String value) {
    if (value.isEmpty) {
      return "Dê sua avaliação";
    }

    if (value.length < 20) {
      return '';
    }

    return null;
  }

  void _validateInputs() async {
    final form = _formKey.currentState;
    FocusScope.of(context).requestFocus(new FocusNode());
    if (form.validate()) {
      // Text forms was validated.
      form.save();
      try {
        startLoading();
        if (!AuthService.isUserRegistered(context)) {
          return;
        }
        GoodUser user = Provider.of<GoodUser>(context);

        GoodReview reviewToSave = GoodReview(
          ownerUid: user.uid,
          targetUid: widget.targetUid,
          name: user.name,
          productType: widget.productType,
          createdAt: DateTime.now(),
          lastModified: DateTime.now(),
          rating: rating,
          title: title,
          text: text,
        );

        final _document = Document(path: '${GoodReview.COL_PATH}/${reviewToSave.ownerUid}_${reviewToSave.targetUid}');
        await _document.upsert(reviewToSave.toMap());

        Navigator.of(context).pop();
      } catch (error) {
        NotificationService.error(context, error.code, error.message);
      }
      finishLoading();
    } else {
      setState(() => _autoValidate = true);
    }
  }
}
