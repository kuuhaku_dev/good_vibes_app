import 'package:flutter/material.dart';
import 'package:good_vibes_app/models/models.dart';
import 'package:good_vibes_app/services/services.dart';
import 'package:good_vibes_app/shared/shared.dart';

class CommentsScreen extends StatelessWidget {
  const CommentsScreen({Key key, @required this.targetUid}) : super(key: key);
  final String targetUid;

  @override
  Widget build(BuildContext context) {
    final commentsRef = Collection<GoodComment>(path: GoodComment.COL_PATH);

    return Scaffold(
      appBar: AppBar(
        title: Text('Comentários'),
      ),
      body: StreamBuilder(
        stream: commentsRef.streamData(query: commentsRef.ref.where('targetUid', isEqualTo: targetUid)),
        builder: (BuildContext context, AsyncSnapshot<List<GoodComment>> snapshot) {
          if (snapshot.hasData) {
            final comments = snapshot.data;

            if (comments.isEmpty) {
              return Center(child: Text('Não há comentários'));
            }

            return ListView.separated(
              padding: EdgeInsets.symmetric(horizontal: 16, vertical: 24),
              itemCount: comments?.length ?? 0,
              itemBuilder: (BuildContext context, int index) {
                return GoodCommentTile(data: comments[index]);
              },
              separatorBuilder: (BuildContext context, int index) => Divider(),
            );
          } else {
            return Center(child: CircularProgressIndicator());
          }
        },
      ),
    );
  }
}
