import 'package:flutter/material.dart';
import 'package:good_vibes_app/models/models.dart';
import 'package:good_vibes_app/services/services.dart';
import 'package:good_vibes_app/shared/shared.dart';

class ReviewsScreen extends StatelessWidget {
  const ReviewsScreen({Key key, @required this.targetUid}) : super(key: key);
  final String targetUid;

  @override
  Widget build(BuildContext context) {
    final reviewsRef = Collection<GoodReview>(path: GoodReview.COL_PATH);

    return Scaffold(
      appBar: AppBar(
        title: Text('Avaliações'),
      ),
      body: StreamBuilder(
        stream: reviewsRef.streamData(query: reviewsRef.ref.where('targetUid', isEqualTo: targetUid)),
        builder: (BuildContext context, AsyncSnapshot<List<GoodReview>> snapshot) {
          if (snapshot.hasData) {
            final reviews = snapshot.data;

            if (reviews.isEmpty) {
              return Center(child: Text('Não há avaliações'));
            }

            return ListView.separated(
              padding: EdgeInsets.symmetric(horizontal: 16, vertical: 24),
              itemCount: reviews?.length ?? 0,
              itemBuilder: (BuildContext context, int index) {
                return GoodReviewTile(item: GoodReviewSummary.fromReview(reviews[index].toMap()));
              },
              separatorBuilder: (BuildContext context, int index) => Divider(),
            );
          } else {
            return Center(child: CircularProgressIndicator());
          }
        },
      ),
    );
  }
}
