import 'package:flutter/material.dart';
import 'package:good_vibes_app/models/models.dart';
import 'package:good_vibes_app/services/services.dart';
import 'package:provider/provider.dart';

class GoodCommentScreen extends StatefulWidget {
  GoodCommentScreen({Key key, @required this.targetUid, @required this.productType}) : super(key: key);
  final String targetUid;
  final String productType;

  @override
  GoodCommentScreenState createState() => GoodCommentScreenState();
}

class GoodCommentScreenState extends State<GoodCommentScreen> {
  final _formKey = GlobalKey<FormState>();

  String title = '';
  String text = '';
  bool _autoValidate = false;
  bool _isLoading = false;

  void startLoading() => setState(() => _isLoading = true);
  void finishLoading() => setState(() => _isLoading = false);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.close),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text('Novo Comentário'),
        actions: <Widget>[
          FlatButton(
            textColor: Colors.white,
            shape: CircleBorder(side: BorderSide(color: Colors.transparent)),
            child: Text('Enviar'),
            onPressed: _validateInputs,
          )
        ],
      ),
      body: Form(
        key: _formKey,
        child: ListView(
          children: <Widget>[
            ListTile(
              title: TextFormField(
                decoration: InputDecoration(
                  hintText: 'Título',
                  border: InputBorder.none,
                ),
                onSaved: (String value) {
                  title = value;
                },
                validator: _validateTitle,
                textInputAction: TextInputAction.unspecified,
              ),
            ),
            Divider(height: 1),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16),
              child: TextFormField(
                decoration: InputDecoration(
                  hintText: 'Comentátio',
                  border: InputBorder.none,
                ),
                onChanged: (value) {
                  setState(() => text = value);
                },
                onSaved: (String value) {
                  text = value;
                },
                maxLines: 5,
                initialValue: '',
                validator: _validateText,
                keyboardType: TextInputType.multiline,
                textInputAction: TextInputAction.unspecified,
              ),
            ),
          ],
        ),
      ),
    );
  }

  String _validateTitle(String value) {
    if (value.isEmpty) {
      return "Insira um título";
    }

    return null;
  }

  String _validateText(String value) {
    if (value.isEmpty) {
      return "Faça seu comentário";
    }

    return null;
  }

  void _validateInputs() async {
    final form = _formKey.currentState;
    FocusScope.of(context).requestFocus(new FocusNode());
    if (form.validate()) {
      // Text forms was validated.
      form.save();
      try {
        startLoading();
        if (!AuthService.isUserRegistered(context)) {
          return;
        }
        GoodUser user = Provider.of<GoodUser>(context);

        GoodComment commentToSave = GoodComment(
          ownerUid: user.uid,
          targetUid: widget.targetUid,
          name: user.name,
          productType: widget.productType,
          createdAt: DateTime.now(),
          lastModified: DateTime.now(),
          title: title,
          text: text,
        );

        final _collection = Collection(path: GoodComment.COL_PATH);
        await _collection.upsert(commentToSave.toMap());

        Navigator.of(context).pop();
      } catch (error) {
        NotificationService.error(context, error.code, error.message);
      }
      finishLoading();
    } else {
      setState(() => _autoValidate = true);
    }
  }
}
