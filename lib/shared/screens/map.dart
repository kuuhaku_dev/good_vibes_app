import 'dart:async';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:good_vibes_app/interfaces/interfaces.dart';
import 'package:good_vibes_app/models/models.dart';
import 'package:good_vibes_app/screens/screens.dart';
import 'package:good_vibes_app/services/services.dart';
import 'package:good_vibes_app/shared/shared.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:provider/provider.dart';

class PointObject {
  final Widget child;
  final LatLng location;

  PointObject({this.child, this.location});
}

class GoodMapScreen extends StatefulWidget {
  GoodMapScreen({Key key, this.iLocationables}) : super(key: key);
  final List<ILocationable> iLocationables;

  @override
  _GoodMapScreenState createState() => _GoodMapScreenState();
}

class _GoodMapScreenState extends State<GoodMapScreen> {
  StreamSubscription _mapIdleSubscription;
  InfoWidgetRoute _infoWidgetRoute;
  GoogleMapController _mapController;

  Map<MarkerId, Marker> markers = <MarkerId, Marker>{};

  PointObject point = PointObject();

  _initLocation() async {
    if (widget.iLocationables != null) {
      Map<MarkerId, Marker> newMarkers = <MarkerId, Marker>{};
      widget.iLocationables.forEach((iLocationable) {
        LatLng latlng = iLocationable.position;
        if (latlng == null) {
          return;
        }
        MarkerId markerId = MarkerId("${latlng.latitude.toString()}:${latlng.longitude.toString()}");

        Marker marker = Marker(
          markerId: markerId,
          position: latlng,
          onTap: () => _onTap(PointObject(
            child: GestureDetector(
              onTap: () {
                if (iLocationable is GoodService) {
                  NavigationService.instance
                      .push(MaterialPageRoute(builder: (_) => ServiceScreen(service: iLocationable)));
                } else if (iLocationable is GoodEvent) {
                  NavigationService.instance.push(MaterialPageRoute(builder: (_) => EventScreen(event: iLocationable)));
                } else {
                  log('TODO: Create navigation to ' + iLocationable.toString());
                }
              },
              child: Text(iLocationable.title),
            ),
            location: latlng,
          )),
        );

        newMarkers[markerId] = marker;
      });

      setState(() {
        markers = newMarkers;
      });
    }

    ILocationable currentLocation =
        Provider.of<ILocationable>(context) ?? await GeolocationService.instance.getCurrentLocation();

    _mapController.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
      target: LatLng(currentLocation.latitude, currentLocation.longitude),
      zoom: 12,
    )));
  }

  /// now my _onTap Method. First it creates the Info Widget Route and then
  /// animates the Camera twice:
  /// First to a place near the marker, then to the marker.
  /// This is done to ensure that onCameraMove is always called

  _onTap(PointObject point) async {
    final RenderBox renderBox = context.findRenderObject();
    Rect _itemRect = renderBox.localToGlobal(Offset.zero) & renderBox.size;

    _infoWidgetRoute = InfoWidgetRoute(
      child: point.child,
      buildContext: context,
      textStyle: const TextStyle(
        fontSize: 14,
        color: Colors.black,
      ),
      mapsWidgetSize: _itemRect,
    );

    await _mapController.animateCamera(
      CameraUpdate.newCameraPosition(
        CameraPosition(
          target: LatLng(
            point.location.latitude - 0.0001,
            point.location.longitude,
          ),
          zoom: 15,
        ),
      ),
    );
    await _mapController.animateCamera(
      CameraUpdate.newCameraPosition(
        CameraPosition(
          target: LatLng(
            point.location.latitude,
            point.location.longitude,
          ),
          zoom: 15,
        ),
      ),
    );
  }

  void _onMapCreated(GoogleMapController controller) {
    this._mapController = controller;
    _initLocation();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.close),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text('Próximo de você'),
      ),
      body: GoogleMap(
        initialCameraPosition: CameraPosition(
          target: LatLng(-15.79, -47.88),
          zoom: 12,
        ),
        onMapCreated: _onMapCreated,
        mapType: MapType.normal,
        myLocationEnabled: true,
        myLocationButtonEnabled: true,
        compassEnabled: true,
        markers: Set<Marker>.of(markers.values),

        /// This fakes the onMapIdle, as the googleMaps on Map Idle does not always work
        /// (see: https://github.com/flutter/flutter/issues/37682)
        /// When the Map Idles and a _infoWidgetRoute exists, it gets displayed.
        onCameraMove: (newPosition) {
          _mapIdleSubscription?.cancel();
          _mapIdleSubscription = Future.delayed(Duration(milliseconds: 150)).asStream().listen((_) {
            if (_infoWidgetRoute != null) {
              Navigator.of(context, rootNavigator: true).push(_infoWidgetRoute).then<void>(
                (newValue) {
                  _infoWidgetRoute = null;
                },
              );
            }
          });
        },
      ),
    );
  }
}
