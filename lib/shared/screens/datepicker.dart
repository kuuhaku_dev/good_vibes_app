import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:table_calendar/table_calendar.dart';

class GoodDatepickerScreen extends StatefulWidget {
  GoodDatepickerScreen({Key key, this.startDate}) : super(key: key);
  final DateTime startDate;

  @override
  _GoodDatepickerScreenState createState() => _GoodDatepickerScreenState();
}

class _GoodDatepickerScreenState extends State<GoodDatepickerScreen> {
  CalendarController _calendarController;
  DateTime _startDate;
  static const int MINUTE_INTERVAL = 5;

  @override
  void initState() {
    super.initState();
    _startDate = widget.startDate ?? DateTime.now();
    _calendarController = CalendarController();

    if (_startDate.minute % MINUTE_INTERVAL != 0) {
      int remainder = _startDate.minute % MINUTE_INTERVAL;
      _startDate = new DateTime(
          _startDate.year, _startDate.month, _startDate.day, _startDate.hour, _startDate.minute - remainder);
    }
  }

  @override
  void dispose() {
    _calendarController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final DateTime yesterday = DateTime.now().subtract(Duration(days: 1));
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.close),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text('Data e hora'),
        actions: <Widget>[
          FlatButton(
            textColor: Colors.white,
            shape: CircleBorder(side: BorderSide(color: Colors.transparent)),
            child: Text('OK'),
            onPressed: () => Navigator.of(context).pop(_startDate),
          )
        ],
      ),
      body: ListView(
        children: <Widget>[
          Container(
            color: Colors.white,
            child: ListTile(
              title: Text(
                "${DateFormat.MMMd().format(_startDate)} às ${DateFormat.Hm().format(_startDate)}",
                style: TextStyle(fontWeight: FontWeight.bold, color: Theme.of(context).primaryColor),
              ),
            ),
          ),
          TableCalendar(
            availableGestures: AvailableGestures.none,
            calendarController: _calendarController,
            enabledDayPredicate: (datetime) => datetime.isAfter(yesterday),
            availableCalendarFormats: {CalendarFormat.month: 'mês'},
            initialSelectedDay: _startDate,
            onDaySelected: (selectedDay, list) {
              setState(() {
                _startDate = new DateTime(
                    selectedDay.year, selectedDay.month, selectedDay.day, _startDate.hour, _startDate.minute);
              });
            },
          ),
          Container(
            height: 200,
            child: CupertinoDatePicker(
              mode: CupertinoDatePickerMode.time,
              use24hFormat: true,
              minuteInterval: MINUTE_INTERVAL,
              initialDateTime: _startDate,
              onDateTimeChanged: (DateTime selectedTime) {
                setState(() {
                  _startDate = new DateTime(
                      _startDate.year, _startDate.month, _startDate.day, selectedTime.hour, selectedTime.minute);
                });
              },
            ),
          ),
        ],
      ),
    );
  }
}
