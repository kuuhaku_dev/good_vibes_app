import 'package:flutter/material.dart';
import 'package:good_vibes_app/interfaces/interfaces.dart';
import 'package:good_vibes_app/shared/shared.dart';

class GoodGallery extends StatelessWidget {
  const GoodGallery({Key key, this.iImageable})
      : assert(iImageable != null),
        super(key: key);
  final IImageable iImageable;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Fotos'),
      ),
      body: ListView.separated(
        itemCount: iImageable.extraImagesUrls.length,
        itemBuilder: (BuildContext context, int index) {
          final url = iImageable.extraImagesUrls[index];
          return GoodCachedImage(imageUrl: url);
        },
        separatorBuilder: (BuildContext context, int index) => SizedBox(height: 24),
      ),
    );
  }
}
