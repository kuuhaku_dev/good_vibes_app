import 'package:flutter/material.dart';
import 'package:good_vibes_app/services/notification.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';

class GoodLocationPickerScreen extends StatefulWidget {
  GoodLocationPickerScreen({Key key, this.savedPosition}) : super(key: key);
  final LatLng savedPosition;

  @override
  _GoodLocationPickerScreenState createState() => _GoodLocationPickerScreenState();
}

class _GoodLocationPickerScreenState extends State<GoodLocationPickerScreen> {
  GoogleMapController controller;
  Map<MarkerId, Marker> markers = <MarkerId, Marker>{};
  MarkerId selectedMarker;
  int _markerIdCounter = 0;

  _initLocation() async {
    Location location = new Location();
    bool isPermissionGranted = await location.requestPermission();
    if (isPermissionGranted && controller != null) {
      final savedPosition = widget.savedPosition;
      if (savedPosition != null) {
        _add(savedPosition);
        controller.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
          target: savedPosition,
          zoom: 15,
        )));
      } else {
        LocationData locationData = await location.getLocation();
        LatLng latLng = LatLng(locationData.latitude, locationData.longitude);
        controller.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
          target: latLng,
          zoom: 15,
        )));
      }
    }
  }

  void _onMapCreated(GoogleMapController controller) {
    this.controller = controller;
    _initLocation();
  }

  @override
  void dispose() {
    super.dispose();
  }

  void _onMarkerTapped(MarkerId markerId) {
    final Marker tappedMarker = markers[markerId];
    if (tappedMarker != null) {
      setState(() {
        if (markers.containsKey(selectedMarker)) {
          final Marker resetOld = markers[selectedMarker].copyWith(iconParam: BitmapDescriptor.defaultMarker);
          markers[selectedMarker] = resetOld;
        }
        selectedMarker = markerId;
        final Marker newMarker = tappedMarker.copyWith(
          iconParam: BitmapDescriptor.defaultMarkerWithHue(
            BitmapDescriptor.hueGreen,
          ),
        );
        markers[markerId] = newMarker;
      });
    }
  }

  void _onMarkerDragEnd(MarkerId markerId, LatLng newPosition) async {
    final Marker tappedMarker = markers[markerId];

    setState(() {
      markers[markerId] = tappedMarker.copyWith(
        positionParam: newPosition,
      );
    });
  }

  void _add(LatLng positionToAdd) {
    final int markerCount = markers.length;

    if (markerCount == 1) {
      return;
    }

    final String markerIdVal = 'Nova Localização';
    final MarkerId markerId = MarkerId(_markerIdCounter.toString());
    _markerIdCounter++;

    final Marker marker = Marker(
      markerId: markerId,
      position: positionToAdd,
      infoWindow: InfoWindow(title: markerIdVal),
      draggable: true,
      onTap: () {
        _onMarkerTapped(markerId);
      },
      onDragEnd: (LatLng position) {
        _onMarkerDragEnd(markerId, position);
      },
    );

    setState(() {
      markers[markerId] = marker;
      selectedMarker = markerId;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.close),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text('Localização'),
        actions: <Widget>[
          FlatButton(
            textColor: Colors.white,
            shape: CircleBorder(side: BorderSide(color: Colors.transparent)),
            child: Text('Salvar'),
            onPressed: () {
              Marker marker = markers[selectedMarker];
              if (marker == null) {
                NotificationService.error(
                    context, 'Nenhuma localização', 'Você não pode salvar se não selecionou uma localização');
              } else {
                Navigator.of(context).pop(marker.position);
              }
            },
          )
        ],
      ),
      body: Stack(
        children: <Widget>[
          GoogleMap(
            initialCameraPosition: CameraPosition(
              target: LatLng(-15.79, -47.88),
              zoom: 12,
            ),
            onMapCreated: _onMapCreated,
            myLocationEnabled: true,
            mapType: MapType.normal,
            myLocationButtonEnabled: true,
            compassEnabled: true,
            onLongPress: (LatLng position) => _add(position),
            markers: Set<Marker>.of(markers.values),
          )
        ],
      ),
    );
  }
}
