import 'package:flutter/material.dart';
import 'package:good_vibes_app/screens/screens.dart';

class NavigationService {
  NavigationService._privateConstructor();

  static final NavigationService instance = new NavigationService._privateConstructor();

  final GlobalKey<NavigatorState> navigatorKey = new GlobalKey<NavigatorState>();
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();

  Future<dynamic> push(Route route) {
    return navigatorKey.currentState.push(route);
  }

  Future<dynamic> pushNamed(String routeName) {
    return navigatorKey.currentState.pushNamed(routeName);
  }

  Future<dynamic> resetToRoot() {
    return navigatorKey.currentState.pushReplacementNamed('/');
  }

  Future<dynamic> resetToRegistration() {
    return navigatorKey.currentState.pushReplacementNamed('registration');
  }

  Future<dynamic> navigateToWalkthrough() {
    return navigatorKey.currentState.push(MaterialPageRoute(builder: (context) => OnboardingScreen()));
  }

  Future<dynamic> navigateToNewsDetail(String uid) {
    navigatorKey.currentState.popUntil((Route<dynamic> route) => route.isFirst);
    return navigatorKey.currentState.push(MaterialPageRoute(builder: (_) => InfoScreen(uid: uid)));
  }
}
