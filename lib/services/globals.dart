import 'package:good_vibes_app/interfaces/typeable.dart';
import 'package:good_vibes_app/models/good_service.dart';
import 'package:good_vibes_app/models/models.dart';

import 'services.dart';
import 'package:firebase_analytics/firebase_analytics.dart';

/// Static global state. Immutable services that do not care about build context.
class Global {
  // App Data
  static final String title = 'GoodVibes';
  static const String HERO_PREFIX = 'main-';
  static const String EXTRA_PREFIX = 'extra-';
  static const String THUMB_64 = 'thumb@64_';
  static const String THUMB_1024 = 'thumb@1024_';
  static const BUCKET_NAME = 'quantico-uni.appspot.com';
  static const BASE_DOWNLOAD_URL = 'https://storage.googleapis.com/$BUCKET_NAME';

  // Services
  static final FirebaseAnalytics analytics = FirebaseAnalytics();

  // Data Models
  static final Map models = {
    GoodService: (data) => GoodService.fromMap(data),
    GoodUser: (data) => GoodUser.fromMap(data),
    GoodEvent: (data) => GoodEvent.fromMap(data),
    GoodInfo: (data) => GoodInfo.fromMap(data),
    GoodComment: (data) => GoodComment.fromMap(data),
    GoodReview: (data) => GoodReview.fromMap(data),
    GoodInfoType: (data) => GoodInfoType.fromMap(data),
    GoodServiceType: (data) => GoodServiceType.fromMap(data),
    GoodEventType: (data) => GoodEventType.fromMap(data),
  };

  // Firestore References for Writes
  static final Collection<GoodService> servicesRef = Collection<GoodService>(path: GoodService.COL_PATH);
  static final Collection<GoodEvent> eventsRef = Collection<GoodEvent>(path: GoodEvent.COL_PATH);
  static final Collection<GoodInfo> infosRef = Collection<GoodInfo>(path: GoodInfo.COL_PATH);
  static final Collection<GoodInfoType> infoTypesRef = Collection<GoodInfoType>(path: ITypeable.COL_PATH);
  static final Collection<GoodServiceType> serviceTypesRef = Collection<GoodServiceType>(path: ITypeable.COL_PATH);
  static final Collection<GoodEventType> eventTypesRef = Collection<GoodEventType>(path: ITypeable.COL_PATH);
}
