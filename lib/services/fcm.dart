import 'dart:async';
import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:good_vibes_app/models/models.dart';
import 'package:good_vibes_app/services/services.dart';

class FCMService {
  final Firestore _db = Firestore.instance;
  final AuthService _auth = AuthService();
  final FirebaseMessaging _fcm = FirebaseMessaging();

  StreamSubscription _iosSubscription;
  Stream get iosSubscription => _fcm.onIosSettingsRegistered;

  FCMService._privateConstructor() {
    _fcm.configure(
      onMessage: (Map<String, dynamic> message) async {
        print("onMessage: $message");
        String title = message.containsKey('aps') ? message['aps']['alert']['title'] : message['notification']['title'];
        String body = message.containsKey('aps') ? message['aps']['alert']['body'] : message['notification']['body'];
        NotificationService.showGlobalNotification(title, body);
      },
      onLaunch: (Map<String, dynamic> message) async {
        print("onLaunch: $message");
        if (message.containsKey('data') && message['data']['newsId'] != null) {
          NavigationService.instance.navigateToNewsDetail(message['data']['newsId']);
        }
      },
      onResume: (Map<String, dynamic> message) async {
        print("onResume: $message");
        if (message.containsKey('data') && message['data']['newsId'] != null) {
          NavigationService.instance.navigateToNewsDetail(message['data']['newsId']);
        }
      },
    );
  }

  static final FCMService instance = new FCMService._privateConstructor();

  Future<void> subscribeToTopic(String topic) async {
    GoodUser currentUser = await _auth.getUser;
    if (currentUser == null) {
      return _fcm.subscribeToTopic(topic);
    } else {
      return Future.wait([_fcm.subscribeToTopic(topic), _auth.toggleNotifications(currentUser, true)]);
    }
  }

  Future<void> unsubscribeFromTopic(String topic) async {
    GoodUser currentUser = await _auth.getUser;
    if (currentUser == null) {
      return _fcm.unsubscribeFromTopic(topic);
    } else {
      return Future.wait([_fcm.unsubscribeFromTopic(topic), _auth.toggleNotifications(currentUser, false)]);
    }
  }

  Future<void> saveDeviceToken() async {
    if (Platform.isIOS) {
      _iosSubscription = _fcm.onIosSettingsRegistered.listen((data) {
        // save the token  OR subscribe to a topic here
        _getAndSaveDeviceToken();
        subscribeToTopic('general');
      });

      _fcm.requestNotificationPermissions(IosNotificationSettings());
    } else {
      _getAndSaveDeviceToken();
      subscribeToTopic('general');
    }
  }

  Future<void> _getAndSaveDeviceToken() async {
    // Get the current user
    GoodUser user = await _auth.getUser;

    if (user == null) {
      return;
    }

    CollectionReference tokensRef = _db.collection('users').document(user.uid).collection('tokens');

    // Get the token for this device
    String token = await _fcm.getToken();

    // Save it to Firestore
    if (token != null) {
      return await tokensRef.document(token).setData({
        'token': token,
        'createdAt': FieldValue.serverTimestamp(), // optional
        'platform': Platform.operatingSystem // optional
      });
    }
  }
}
