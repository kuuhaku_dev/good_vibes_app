import 'package:apple_sign_in/apple_sign_in.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:good_vibes_app/models/models.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:provider/provider.dart';
import 'package:rxdart/rxdart.dart';

import 'notification.dart';

class AuthService {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final Firestore _db = Firestore.instance;
  final GoogleSignIn _googleSignIn = GoogleSignIn();

  Future<GoodUser> get getUser => _auth.currentUser().then((firebaseUser) {
        if (firebaseUser != null) {
          return _db.collection('users').document(firebaseUser.uid).get().then((v) => GoodUser.fromMap(v.data));
        } else {
          return new Future.value(null);
        }
      });

  Stream<GoodUser> get user => Observable(_auth.onAuthStateChanged).switchMap((mapper) {
        if (mapper != null) {
          return _db.collection('users').document(mapper.uid).snapshots().map((v) => GoodUser.fromMap(v.data));
        } else {
          return Observable.empty();
        }
      });

  static bool isUserRegistered(BuildContext context) {
    GoodUser user = Provider.of<GoodUser>(context);
    if (user == null) {
      NotificationService.registerDialog(context);
      return false;
    } else {
      return true;
    }
  }

  Future<FirebaseUser> anonLogin() async {
    FirebaseUser user = (await _auth.signInAnonymously()).user;
    updateUserData(GoodUser.fromFirebaseUser(user));
    return user;
  }

  Future<GoodUser> googleSignIn() async {
    try {
      GoogleSignInAccount googleSignInAccount = await _googleSignIn.signIn();

      GoogleSignInAuthentication googleAuth = await googleSignInAccount.authentication;

      final AuthCredential credential = GoogleAuthProvider.getCredential(
        accessToken: googleAuth.accessToken,
        idToken: googleAuth.idToken,
      );

      FirebaseUser user = (await _auth.signInWithCredential(credential)).user;
      GoodUser goodUser = GoodUser.fromFirebaseUser(user);
      goodUser.uid = user.uid;
      updateUserData(goodUser);

      return goodUser;
    } catch (error) {
      print(error);
      return null;
    }
  }

  Future<GoodUser> appleSignIn() async {
    final AuthorizationResult result = await AppleSignIn.performRequests([
      AppleIdRequest(requestedScopes: [Scope.email, Scope.fullName])
    ]);

    switch (result.status) {
      case AuthorizationStatus.authorized:
        final AppleIdCredential appleIdCredential = result.credential;
        final OAuthProvider oAuthProvider = OAuthProvider(providerId: 'apple.com');
        final AuthCredential credential = oAuthProvider.getCredential(
          idToken: String.fromCharCodes(appleIdCredential.identityToken),
          accessToken: String.fromCharCodes(appleIdCredential.authorizationCode),
        );

        FirebaseUser user = (await _auth.signInWithCredential(credential)).user;
        GoodUser goodUser = GoodUser.fromFirebaseUser(user);
        goodUser.uid = user.uid;
        updateUserData(goodUser);

        return goodUser;
      case AuthorizationStatus.error:
        print("Sign in failed: ${result.error.localizedDescription}");
        break;

      case AuthorizationStatus.cancelled:
        print('User cancelled');
        break;
    }

    return null;
  }

  Future<GoodUser> signIn(String email, String password) async {
    FirebaseUser firebaseUser = (await _auth.signInWithEmailAndPassword(email: email, password: password)).user;

    DocumentSnapshot snap = await _db.collection('users').document(firebaseUser.uid).get();
    GoodUser user = GoodUser.fromMap(snap.data);
    print(snap.data);
    user.uid = firebaseUser.uid;
    updateUserData(user);
    return user;
  }

  Future<GoodUser> signUp(GoodUser user, String password) async {
    FirebaseUser firebaseUser =
        (await _auth.createUserWithEmailAndPassword(email: user.email, password: password)).user;
    user.uid = firebaseUser.uid;
    updateUserData(user);
    return user;
  }

  Future<void> updateUserData(GoodUser user) {
    DocumentReference userRef = _db.collection('users').document(user.uid);

    Map data = user.toMap();
    data['lastActivity'] = DateTime.now();

    return userRef.setData(data, merge: true);
  }

  Future<void> toggleNotifications(GoodUser user, bool receiveNotifications) {
    DocumentReference userRef = _db.collection('users').document(user.uid);

    Map data = user.toMap();
    data['receiveNotifications'] = receiveNotifications;

    return userRef.setData(data, merge: true);
  }

  Future<void> signOut() => _auth.signOut();
}

class AppleSignInAvailable {
  AppleSignInAvailable(this.isAvailable);
  final bool isAvailable;

  static Future<AppleSignInAvailable> check() async {
    return AppleSignInAvailable(await AppleSignIn.isAvailable());
  }
}
