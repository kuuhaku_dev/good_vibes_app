// TODO: download service

import 'dart:io';
import 'package:firebase_storage/firebase_storage.dart';

class DownloadService {
  final FirebaseStorage _storage = FirebaseStorage(storageBucket: 'gs://quantico-uni.appspot.com');
}

// TODO: Call method on download finish

class UploaderService {
  static final FirebaseStorage _storage = FirebaseStorage(storageBucket: 'gs://quantico-uni.appspot.com');
  final File file;
  final List<int> imageData;
  StorageUploadTask uploadTask;
  Function onEvent;
  UploaderService({this.file, this.imageData}) : assert(file != null || imageData != null);

  Future<StorageTaskSnapshot> uploadFileFuture(String filePath) async {
    var ref = _storage.ref().child(filePath);
    this.uploadTask = file != null ? ref.putFile(file) : ref.putData(imageData);
    return await this.uploadTask.onComplete;
  }

  get fileReference {
    if (this.uploadTask.isSuccessful && this.uploadTask.isComplete) {
      return this.uploadTask.lastSnapshot.ref;
    } else {
      return null;
    }
  }

  get fileMetadata async {
    if (this.uploadTask.isSuccessful && this.uploadTask.isComplete) {
      return await this.uploadTask.lastSnapshot.ref.getMetadata();
    } else {
      return null;
    }
  }

  get downloadUrl async {
    if (this.uploadTask.isSuccessful && this.uploadTask.isComplete) {
      return await this.uploadTask.lastSnapshot.ref.getDownloadURL();
    } else {
      return null;
    }
  }
}
