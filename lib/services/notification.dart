import 'package:flutter/material.dart';
import 'package:good_vibes_app/services/services.dart';
import 'package:good_vibes_app/shared/shared.dart';
import 'package:oktoast/oktoast.dart';

class NotificationService {
  static Future<bool> confirm(BuildContext context, String title, String body) async {
    return showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext innerContext) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10),
            ),
            title: Text(title),
            content: Text(body),
            actions: <Widget>[
              FlatButton(
                onPressed: () => Navigator.of(innerContext).pop(false),
                child: Text('Cancelar'),
              ),
              FlatButton(
                color: Theme.of(innerContext).errorColor,
                onPressed: () => Navigator.of(innerContext).pop(true),
                child: Text('Apagar', style: TextStyle(color: Colors.white)),
              ),
            ],
          );
        });
  }

  static showSnackBar(String title) {
    NavigationService.instance.scaffoldKey.currentState.showSnackBar(SnackBar(content: Text(title)));
  }

  static Future<void> error(BuildContext context, String title, String body) async {
    return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
          ),
          title: Text(title),
          content: Text(body),
          actions: <Widget>[
            FlatButton(
              onPressed: () => Navigator.of(context).pop(),
              child: Text('Ok'),
            ),
          ],
        );
      },
    );
  }

  static Future<void> registerDialog(BuildContext context) async {
    return showDialog(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
          ),
          title: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Image.asset(
                'assets/icon/icon-colored.png',
                height: 50,
              ),
              SizedBox(height: 16),
              Text(
                'Acesse o melhor do Universo Quântico',
                textAlign: TextAlign.center,
              ),
            ],
          ),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text('Crie conteúdo, faça avaliações, receba alertas daquilo que você gosta e muito mais'),
              SizedBox(height: 32),
              GoodOutlineButton(
                title: 'Quero me cadastrar!',
                onPressed: () {
                  Navigator.of(context).pop();
                  NavigationService.instance.resetToRegistration();
                },
              )
            ],
          ),
        );
      },
    );
  }

  static showGlobalNotification(String title, String body) {
    showToast(body);
  }
}
