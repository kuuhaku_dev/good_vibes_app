import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:rxdart/rxdart.dart';
import 'globals.dart';

// Generic helper methods
class Document<T> {
  final Firestore _db = Firestore.instance;
  final String path;
  DocumentReference ref;

  Document({this.path}) {
    ref = _db.document(path);
  }

  Future<T> getData() {
    return ref.get().then((v) => Global.models[T]({...v.data, "uid": v.documentID}) as T);
  }

  Stream<T> streamData() {
    return ref.snapshots().map((v) => Global.models[T]({...v.data, "uid": v.documentID}) as T);
  }

  /// Creates or updates a document and return it's uid
  Future<String> upsert(Map data, {bool merge = true}) async {
    // auto-save uid on document
    String documentUid;
    String generatedUid = ref.documentID;
    if (data['uid'] != null) {
      documentUid = data['uid'];
    } else {
      documentUid = generatedUid;
      data["uid"] = generatedUid;
    }

    CollectionReference parentCollection = ref.parent();
    await parentCollection.document(documentUid).setData(Map<String, dynamic>.from(data), merge: true);
    return Future.value(documentUid);
  }

  Future<void> delete() async {
    return ref.delete();
  }
}

class Collection<T> {
  final Firestore _db = Firestore.instance;
  final String path;
  CollectionReference ref;

  Collection({this.path}) {
    ref = _db.collection(path);
  }

  Future<List<T>> getData({Query query}) async {
    final currentRef = query != null ? query : ref;
    var snapshots = await currentRef.getDocuments();
    return snapshots.documents.map((doc) => Global.models[T]({...doc.data, "uid": doc.documentID}) as T).toList();
  }

  Stream<List<T>> streamData({Query query}) {
    final currentRef = query != null ? query : ref;
    return currentRef.snapshots().map(
        (list) => list.documents.map((doc) => Global.models[T]({...doc.data, "uid": doc.documentID}) as T).toList());
  }

  /// Creates or updates a document and return it's uid
  Future<String> upsert(Map data) async {
    String documentUid;
    String generatedUid = ref.document().documentID;
    if (data['uid'] != null) {
      documentUid = data['uid'];
    } else {
      documentUid = generatedUid;
      data["uid"] = generatedUid;
    }

    await ref.document(documentUid).setData(Map<String, dynamic>.from(data), merge: true);
    return Future.value(documentUid);
  }
}

class UserData<T> {
  final Firestore _db = Firestore.instance;
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final String collection;

  UserData({this.collection});

  Stream<T> get documentStream {
    // This is meant to retrieve user specific collections, checks if user is actually logged in
    return Observable(_auth.onAuthStateChanged).switchMap((user) {
      if (user != null) {
        Document<T> doc = Document<T>(path: '$collection/${user.uid}');
        return doc.streamData();
      } else {
        return Observable<T>.just(null);
      }
    });
  }

  Future<T> getDocument() async {
    FirebaseUser user = await _auth.currentUser();

    if (user != null) {
      Document doc = Document<T>(path: '$collection/${user.uid}');
      return doc.getData();
    } else {
      return null;
    }
  }
}
