import 'dart:async';

import 'package:good_vibes_app/interfaces/interfaces.dart';
import 'package:good_vibes_app/models/models.dart';
import 'package:location/location.dart';

class GeolocationService {
  final Location _locationService = Location();
  final StreamController<ILocationable> _streamController = StreamController<ILocationable>.broadcast();

  get streamLocation => _streamController.stream;

  GeolocationService._privateConstructor() {}

  static final GeolocationService instance = new GeolocationService._privateConstructor();

  Future<ILocationable> getCurrentLocation() async {
    if (await _locationService.hasPermission() == false) {
      if (await _locationService.requestPermission()) {
        return _getAndUpdateLocation();
      } else {
        return null;
      }
    } else {
      return _getAndUpdateLocation();
    }
  }

  Future<ILocationable> _getAndUpdateLocation() async {
    LocationData data = await _locationService.getLocation();
    ILocationable iLocationable = GoodPosition(latitude: data.latitude, longitude: data.longitude);
    _streamController.add(iLocationable);
    return iLocationable;
  }
}
