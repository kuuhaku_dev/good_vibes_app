import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:good_vibes_app/services/services.dart';
import 'package:good_vibes_app/shared/shared.dart';
import 'package:url_launcher/url_launcher.dart';

class AboutUsScreen extends StatefulWidget {
  const AboutUsScreen({Key key}) : super(key: key);

  @override
  _AboutUsScreenState createState() => _AboutUsScreenState();
}

class _AboutUsScreenState extends State<AboutUsScreen> {
  ScrollController _scrollController;

  @override
  void initState() {
    super.initState();
    _scrollController = ScrollController();
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  _launchURL(BuildContext context, String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      NotificationService.error(context, 'Erro', 'Não foi possível abrir este link');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: CustomScrollView(
        controller: _scrollController,
        slivers: <Widget>[
          SliverPersistentHeader(
            pinned: true,
            delegate: ItemHeaderDelegate(
              title: 'Quem somos nós',
              collapsedHeight: kToolbarHeight,
              expandedHeight: 300,
              paddingTop: MediaQuery.of(context).padding.top,
              assetImage: 'assets/img/myself.jpeg',
              context: context,
              iImageable: null,
            ),
          ),
          SliverList(
            delegate: SliverChildListDelegate(
              [
                ListView(
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  padding: EdgeInsets.symmetric(horizontal: 16, vertical: 24),
                  children: <Widget>[
                    Text(
                        'Sou Tatiane Leite, thetahealer, bióloga, profissional da área ambiental e idealizadora deste App. Manifestei este aplicativo com o objetivo de compartilhar com você todas as maravilhas que aprendi e estou aprendendo na minha longa e bela jornada de expansão da consciência. Este é um processo que envolve autoconhecimento e reforma íntima que só você pode criar.'),
                    SizedBox(height: 8),
                    Text(
                        'Expandir a consciência é despertar, é tomar para si o controle da sua própria vida, é ver o que está além desse plano físico, é entender que somos criadores da nossa própria realidade. Toda esta descoberta me fez sentir transbordando de uma alegria sem limites e entrando em um fluxo de bem-estar que precisa ser compartilhado com outras pessoas como uma forma de retribuir todas as belezas que tenho experimentado, por isso, o Universo conspirou para a criação do “Universo Quântico”. Nesse Portal você encontrará conteúdo, inspirações, livros, profissionais, terapias, cursos, eventos e tudo mais que possa te ajudar no seu progresso.'),
                    SizedBox(height: 8),
                    Text(
                        'Espero que este App ajude a transformar a sua visão de como o mundo funciona, mostrando o quanto você é mais poderoso do que sabia, inspirando-o a demonstrar o entendimento de que o que você pensa e no que acredita tem um efeito profundo em sua vida. Desejo que você não seja mais a mesma pessoa que era quando começou a sua jornada.'),
                    SizedBox(height: 24),
                    Text('Contato', style: Theme.of(context).textTheme.title),
                    SizedBox(height: 8),
                    Row(
                      children: <Widget>[
                        Icon(
                          FontAwesomeIcons.whatsapp,
                          size: 24,
                          color: Theme.of(context).primaryColor,
                        ),
                        SizedBox(
                          width: 8,
                        ),
                        GestureDetector(
                          onTap: () => _launchURL(context, "tel:+5561982230066"),
                          child: Text(
                            '+55 61 98223-0066',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Theme.of(context).primaryColor,
                            ),
                          ),
                        )
                      ],
                    ),
                    SizedBox(height: 8),
                    Row(
                      children: <Widget>[
                        Icon(
                          FontAwesomeIcons.envelope,
                          size: 24,
                          color: Theme.of(context).primaryColor,
                        ),
                        SizedBox(
                          width: 8,
                        ),
                        GestureDetector(
                          onTap: () => _launchURL(context, "mailto:quantico.uni@gmail.com"),
                          child: Text(
                            'quantico.uni@gmail.com',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Theme.of(context).primaryColor,
                            ),
                          ),
                        )
                      ],
                    ),
                    SizedBox(height: 48),
                    Center(
                      child: Image.asset(
                        'assets/icon/icon-colored.png',
                        width: MediaQuery.of(context).size.width * 0.25,
                      ),
                    )
                  ],
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}

// @override
// Widget build(BuildContext context) {
//   return Scaffold(
//     appBar: AppBar(
//       title: Text('Quem somos nós'),
//     ),
//     body: ListView(
//       padding: EdgeInsets.symmetric(horizontal: 16, vertical: 24),
//       children: <Widget>[
//         Text(
//           'Quem somos nós',
//           style: Theme.of(context).textTheme.headline,
//         ),
//         SizedBox(height: 16),
//         Text(
//             'Sou Tatiane Leite, bióloga, profissional da área ambiental e idealizadora deste App. Manifestei este aplicativo com o objetivo de compartilhar com você todas as maravilhas que aprendi e estou aprendendo na minha longa e bela jornada de expansão da consciência. Este é um processo que envolve autoconhecimento e reforma íntima que só você pode criar.'),
//         SizedBox(height: 8),
//         Text(
//             'Expandir a consciência é despertar, é tomar para si o controle da sua própria vida, é ver o que está além desse plano físico, é entender que somos criadores da nossa própria realidade. Toda esta descoberta me fez sentir transbordando de uma alegria sem limites e entrando em um fluxo de bem-estar que precisa ser compartilhado com outras pessoas como uma forma de retribuir todas as belezas que tenho experimentado, por isso, o Universo conspirou para a criação do “Universo Quântico”. Nesse Portal você encontrará conteúdo, inspirações, livros, profissionais, terapias, cursos, eventos e tudo mais que possa te ajudar no seu progresso.'),
//         SizedBox(height: 8),
//         Text(
//             'Espero que este App ajude a transformar a sua visão de como o mundo funciona, mostrando o quanto você é mais poderoso do que sabia, inspirando-o a demonstrar o entendimento de que o que você pensa e no que acredita tem um efeito profundo em sua vida. Desejo que você não seja mais a mesma pessoa que era quando começou a sua jornada.'),
//         SizedBox(height: 24),
//         Center(
//           child: Image.asset(
//             'assets/icon/icon-colored.png',
//             width: MediaQuery.of(context).size.width * 0.25,
//           ),
//         )
//       ],
//     ),
//   );
// }
