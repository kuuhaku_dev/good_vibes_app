import 'package:flutter/material.dart';
import 'package:good_vibes_app/models/models.dart';
import 'package:good_vibes_app/services/services.dart';
import 'package:good_vibes_app/shared/shared.dart';
import 'package:provider/provider.dart';

class UserServicesScreen extends StatelessWidget {
  const UserServicesScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    GoodUser user = Provider.of<GoodUser>(context);
    Collection<GoodService> servicesRef = Global.servicesRef;

    return Scaffold(
        appBar: AppBar(
          title: Text('Meus Serviços'),
        ),
        body: StreamBuilder(
          stream: servicesRef.streamData(query: servicesRef.ref.where('ownerUid', isEqualTo: user.uid)),
          builder: (BuildContext context, AsyncSnapshot<List<GoodService>> snapshot) {
            if (snapshot.hasError) {
              return Center(
                child: Text('Ocorreu um erro ao carregar seus serviços'),
              );
            }

            switch (snapshot.connectionState) {
              case ConnectionState.none:
                return Center(child: Text('Nenhuma conexão aberta'));
              case ConnectionState.waiting:
                return Center(child: Text('Carregando seus serviços'));
              case ConnectionState.active:
                if (!snapshot.hasData) {
                  return Center(
                    child: Text('Você não possui nenhum serviço'),
                  );
                }

                List<GoodService> services = snapshot.data;
                return GoodUserServiceList(services: services);
              case ConnectionState.done:
                return Center(child: Text('Conexão encerrada'));
            }

            return null; // unreacheable
          },
        ));
  }
}
