import 'dart:async';

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'package:in_app_purchase/in_app_purchase.dart';

final List<String> donationIds = ['donation_tier1', 'donation_tier2', 'donation_tier3'];

class DonationsScreen extends StatefulWidget {
  const DonationsScreen({Key key}) : super(key: key);

  @override
  _DonationsScreenState createState() => _DonationsScreenState();
}

class _DonationsScreenState extends State<DonationsScreen> {
  /// IAP plugin interface
  InAppPurchaseConnection _iap = InAppPurchaseConnection.instance;

  /// Is the API available on this device
  bool _available = true;

  /// Products for sale
  List<ProductDetails> _products = [];

  /// Past purchases
  List<PurchaseDetails> _purchases = [];

  /// Updates to purchases
  StreamSubscription _subscription;

  @override
  void initState() {
    super.initState();
    _initialize();
  }

  @override
  void dispose() {
    _subscription.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Apoie-nos'),
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Card(
                color: Colors.white,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    ClipRRect(
                      borderRadius: BorderRadius.vertical(top: Radius.circular(8)),
                      child: Image.asset('assets/img/donation.jpg'),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(16),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text(
                                'Doações',
                                style: Theme.of(context)
                                    .textTheme
                                    .headline
                                    .copyWith(color: Theme.of(context).primaryColor),
                              ),
                            ],
                          ),
                          SizedBox(height: 16),
                          Text('Sua doação será usada para:'),
                          SizedBox(height: 8),
                          Row(
                            children: <Widget>[
                              Icon(FontAwesomeIcons.ban, size: 16),
                              SizedBox(width: 12),
                              Expanded(
                                child: Text('Versão livre de anúncios por toda a vida;'),
                              ),
                            ],
                          ),
                          SizedBox(height: 8),
                          Row(
                            children: <Widget>[
                              Icon(FontAwesomeIcons.dolly, size: 16),
                              SizedBox(width: 12),
                              Expanded(
                                child: Text('Desenvolver recursos de software e atualizações;'),
                              ),
                            ],
                          ),
                          SizedBox(height: 8),
                          Row(
                            children: <Widget>[
                              Icon(FontAwesomeIcons.globeAmericas, size: 16),
                              SizedBox(width: 12),
                              Expanded(
                                child: Text(
                                    'Traduções de conteúdo objetivando suportar outros idiomas e alcançar cada vez mais pessoas.'),
                              ),
                            ],
                          ),
                          SizedBox(height: 8),
                          Text(
                              'Sua maravilhosa contribuição nos ajudará a criar mais melhorias neste App ajudando cada vez mais pessoas na busca da expansão da consciência, contribuindo para o despertar da humanidade e para a elevação da consciência universal, segundo Gregg Braden, a  quantidade mínima de pessoas necessárias para “dar partida” a uma mudança de consciência é a raiz quadrada de 1% do total de pessoas da população em causa. Saiba que estamos todos interligados na Matriz Divina, quando eu evoluo, contribuo para a evolução do outro.'),
                          SizedBox(height: 8),
                          Text('Gratidão!'),
                          SizedBox(height: 24),
                          _products.length > 0
                              ? GridView.count(
                                  shrinkWrap: true,
                                  crossAxisSpacing: 10,
                                  mainAxisSpacing: 10,
                                  crossAxisCount: 3,
                                  children: <Widget>[
                                    for (final product in _products)
                                      InkWell(
                                        onTap: () => _buyProduct(product),
                                        child: Container(
                                          padding: const EdgeInsets.all(8),
                                          decoration: BoxDecoration(
                                            border: Border.all(
                                              color: Theme.of(context).primaryColor,
                                              width: 2,
                                            ),
                                            borderRadius: BorderRadius.circular(8),
                                          ),
                                          child: Center(child: Text(product.price)),
                                        ),
                                      ),
                                  ],
                                )
                              : Center(
                                  child: Text('Carregando...'),
                                ),
                        ],
                      ),
                    ),
                  ],
                ))
          ],
        ),
      ),
    );
  }

  /// Initialize data
  void _initialize() async {
    // Check availability of In App Purchases
    _available = await _iap.isAvailable();

    if (_available) {
      await _getProducts();

      // Listen to new purchases
      _subscription = _iap.purchaseUpdatedStream.listen((data) => setState(() {
            print('Nova doação!');
            _purchases.addAll(data);
          }));
    }
  }

  /// Get all products available for sale
  Future<void> _getProducts() async {
    Set<String> ids = Set.from(donationIds);
    ProductDetailsResponse response = await _iap.queryProductDetails(ids);

    setState(() {
      _products = response.productDetails;
    });
  }

  /// Purchase a product
  void _buyProduct(ProductDetails prod) {
    final PurchaseParam purchaseParam = PurchaseParam(productDetails: prod);
    // _iap.buyNonConsumable(purchaseParam: purchaseParam);
    _iap.buyConsumable(purchaseParam: purchaseParam, autoConsume: true);
  }
}
