import 'package:flutter/material.dart';
import 'package:good_vibes_app/models/models.dart';
import 'package:good_vibes_app/services/services.dart';
import 'package:good_vibes_app/shared/shared.dart';
import 'package:provider/provider.dart';

class UserEventsScreen extends StatelessWidget {
  const UserEventsScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    GoodUser user = Provider.of<GoodUser>(context);
    Collection<GoodEvent> eventsRef = Global.eventsRef;

    return Scaffold(
        appBar: AppBar(
          title: Text('Meus Eventos'),
        ),
        body: StreamBuilder(
          stream: eventsRef.streamData(query: eventsRef.ref.where('ownerUid', isEqualTo: user.uid)),
          builder: (BuildContext context, AsyncSnapshot<List<GoodEvent>> snapshot) {
            if (snapshot.hasError) {
              return Center(
                child: Text('Ocorreu um erro ao carregar seus eventos'),
              );
            }

            switch (snapshot.connectionState) {
              case ConnectionState.none:
                return Center(child: Text('Nenhuma conexão aberta'));
              case ConnectionState.waiting:
                return Center(child: Text('Carregando seus eventos'));
              case ConnectionState.active:
                if (!snapshot.hasData) {
                  return Center(
                    child: Text('Você não possui nenhum evento'),
                  );
                }

                List<GoodEvent> events = snapshot.data;
                return GoodEventCardSection(events: events, isScrollable: true, isEditable: true);
              case ConnectionState.done:
                return Center(child: Text('Conexão encerrada'));
            }

            return null; // unreacheable
          },
        ));
  }
}
