import 'dart:io';

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:good_vibes_app/models/models.dart';
import 'package:good_vibes_app/services/services.dart';
import 'package:good_vibes_app/shared/shared.dart';
import 'package:launch_review/launch_review.dart';
import 'package:provider/provider.dart';
import 'package:share/share.dart';

class ProfileScreen extends StatelessWidget {
  const ProfileScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    AuthService _auth = AuthService();
    GoodUser user = Provider.of<GoodUser>(context);
    final GoodPackageInfo packageInfo = Provider.of<GoodPackageInfo>(context);

    return Scaffold(
      appBar: AppBar(
        title: Text('Perfil'),
      ),
      body: ListView(
        padding: EdgeInsets.symmetric(vertical: 16),
        children: <Widget>[
          GoodUserTile(user: user),
          SizedBox(height: 16),
          GoodProfileSection(
            children: <Widget>[
              GoodProfileItemTile(
                leading: Icon(FontAwesomeIcons.bell),
                title: 'Ativar Notificações',
                trailing: Switch.adaptive(
                  value: user != null ? user.receiveNotifications : true,
                  onChanged: (v) => v
                      ? FCMService.instance.subscribeToTopic('general')
                      : FCMService.instance.unsubscribeFromTopic('general'),
                ),
              ),
              GoodProfileItemTile(
                leading: Icon(FontAwesomeIcons.users),
                title: 'Meus eventos',
                trailing: Icon(Icons.chevron_right),
                onTap: () {
                  if (AuthService.isUserRegistered(context)) {
                    Navigator.of(context).pushNamed('/user_events');
                  }
                },
              ),
              GoodProfileItemTile(
                leading: Icon(FontAwesomeIcons.dna),
                title: 'Meus Serviços',
                trailing: Icon(Icons.chevron_right),
                onTap: () {
                  if (AuthService.isUserRegistered(context)) {
                    Navigator.of(context).pushNamed('/user_services');
                  }
                },
              ),
              GoodProfileItemTile(
                leading: Icon(FontAwesomeIcons.handHoldingHeart),
                title: 'Apoie-nos',
                trailing: Icon(Icons.chevron_right),
                onTap: () => Navigator.of(context).pushNamed('/donations'),
              ),
            ],
          ),
          SizedBox(height: 16),
          GoodProfileSection(
            children: <Widget>[
              GoodProfileItemTile(
                leading: Icon(FontAwesomeIcons.graduationCap),
                title: 'Boas-vindas',
                trailing: Icon(Icons.chevron_right),
                onTap: () => NavigationService.instance.navigateToWalkthrough(),
              ),
              GoodProfileItemTile(
                leading: Icon(FontAwesomeIcons.question),
                title: 'Quem somos nós',
                trailing: Icon(Icons.chevron_right),
                onTap: () => Navigator.of(context).pushNamed('/about_us'),
              ),
              GoodProfileItemTile(
                leading: Icon(FontAwesomeIcons.medal),
                title: 'Avalie nosso app',
                trailing: Icon(Icons.chevron_right),
                onTap: () => LaunchReview.launch(),
              ),
              GoodProfileItemTile(
                leading: Icon(FontAwesomeIcons.share),
                title: 'Compartilhe nosso app',
                trailing: Icon(Icons.chevron_right),
                onTap: () => Platform.isAndroid
                    ? Share.share('https://play.google.com/store/apps/details?id=com.quantico.goodvibes')
                    : Share.share('Universo Quântico em breve na App Store'),
              ),
              GoodProfileItemTile(
                leading: Icon(FontAwesomeIcons.stickyNote),
                title: 'Política de privacidade',
                trailing: Icon(Icons.chevron_right),
              ),
              GoodProfileItemTile(
                leading: Icon(FontAwesomeIcons.stickyNote),
                title: 'Termos de uso',
                trailing: Icon(Icons.chevron_right),
              ),
              GoodProfileItemTile(
                leading: Icon(FontAwesomeIcons.stickyNote),
                title: 'Perguntas frequentes',
                trailing: Icon(Icons.chevron_right),
              ),
            ],
          ),
          SizedBox(height: 32),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 64),
            child: user != null
                ? GoodOutlineButton(
                    title: 'Sair',
                    onPressed: () async {
                      await _auth.signOut();
                      NavigationService.instance.resetToRegistration();
                    },
                  )
                : GoodOutlineButton(
                    title: 'Cadastre-se',
                    onPressed: NavigationService.instance.resetToRegistration,
                  ),
          ),
          SizedBox(height: 32),
          Column(
            children: <Widget>[
              Text('Feito com 💜 pela', style: TextStyle(fontSize: 12)),
              SizedBox(height: 4),
              Text(
                'Yellowtree Software',
                style: Theme.of(context).textTheme.title.copyWith(fontSize: 14),
              ),
              SizedBox(height: 16),
              Text(
                'Versão: ${packageInfo.version}',
                style: TextStyle(color: Theme.of(context).textTheme.caption.color),
              ),
              Text(
                'Build: ${packageInfo.buildNumber}',
                style: TextStyle(color: Theme.of(context).textTheme.caption.color),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
