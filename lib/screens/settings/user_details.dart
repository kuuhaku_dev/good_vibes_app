import 'package:flutter/material.dart';
import 'package:good_vibes_app/models/models.dart';
import 'package:good_vibes_app/shared/shared.dart';
import 'package:provider/provider.dart';

class UserDetailsScreen extends StatelessWidget {
  const UserDetailsScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final GoodUser user = Provider.of<GoodUser>(context);

    return Scaffold(
      appBar: AppBar(
        title: Text('Dados pessoais'),
      ),
      body: Container(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 16),
          child: UserForm(user: user),
        ),
      ),
    );
  }
}
