import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:good_vibes_app/screens/screens.dart';
import 'package:good_vibes_app/services/services.dart';

enum TabItem { infos, services, events, profile, donate }

class HomeScreen extends StatefulWidget {
  HomeScreen({Key key}) : super(key: key);

  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  // TAB BARS
  TabItem _currentTab = TabItem.infos;
  Map<TabItem, GlobalKey<NavigatorState>> navigatorKeys = {
    TabItem.infos: GlobalKey<NavigatorState>(),
    TabItem.services: GlobalKey<NavigatorState>(),
    TabItem.events: GlobalKey<NavigatorState>(),
    TabItem.profile: GlobalKey<NavigatorState>(),
    TabItem.donate: GlobalKey<NavigatorState>(),
  };

  @override
  void initState() {
    super.initState();
    GeolocationService.instance.getCurrentLocation();
    FCMService.instance.saveDeviceToken();
  }

  Widget _buildOffstageNavigator(TabItem tabItem, Map<String, WidgetBuilder> routes) {
    return Offstage(
      offstage: _currentTab != tabItem,
      child: Navigator(
        key: navigatorKeys[tabItem],
        initialRoute: '/',
        onGenerateRoute: (settings) {
          if (routes[settings.name] == null) {
            throw Exception('Invalid route: ${settings.name}');
          }
          return MaterialPageRoute(builder: routes[settings.name], settings: settings);
        },
      ),
    );
  }

  void _onItemTapped(int index) {
    setState(() {
      _currentTab = TabItem.values[index];
    });
  }

  @override
  Widget build(BuildContext context) {
    NavigatorState rootNavigator = Navigator.of(context);
    return WillPopScope(
      onWillPop: () async => !await navigatorKeys[_currentTab].currentState.maybePop(),
      child: Scaffold(
        key: NavigationService.instance.scaffoldKey,
        body: Stack(
          children: <Widget>[
            _buildOffstageNavigator(TabItem.infos, {
              '/': (context) => InfosScreen(),
            }),
            _buildOffstageNavigator(TabItem.services, {
              '/': (context) => ServicesScreen(),
              '/new': (context) => ServiceCreateScreen(),
            }),
            _buildOffstageNavigator(TabItem.events, {
              '/': (context) => EventsScreen(),
              '/new': (context) => EventCreateScreen(),
            }),
            _buildOffstageNavigator(TabItem.profile, {
              '/': (context) => ProfileScreen(),
              '/user_events': (context) => UserEventsScreen(),
              '/user_services': (context) => UserServicesScreen(),
              '/donations': (context) => DonationsScreen(),
              '/about_us': (context) => AboutUsScreen(),
            }),
            _buildOffstageNavigator(TabItem.donate, {
              '/': (context) => DonationsScreen(),
            }),
          ],
        ),
        bottomNavigationBar: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(FontAwesomeIcons.atom),
              title: Text('Portal'),
            ),
            BottomNavigationBarItem(
              icon: Icon(FontAwesomeIcons.dna),
              title: Text('Serviços'),
            ),
            BottomNavigationBarItem(
              icon: Icon(FontAwesomeIcons.users),
              title: Text('Eventos'),
            ),
            BottomNavigationBarItem(
              icon: Icon(FontAwesomeIcons.userCircle),
              title: Text('Perfil'),
            ),
            BottomNavigationBarItem(
              icon: Icon(FontAwesomeIcons.handHoldingHeart),
              title: Text('Apoie-nos'),
            ),
          ],
          currentIndex: _currentTab.index,
          onTap: _onItemTapped,
        ),
      ),
    );
  }
}
