import 'package:flutter/material.dart';
import 'package:good_vibes_app/shared/shared.dart';

class EventCreateScreen extends StatelessWidget {
  const EventCreateScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Novo Evento'),
      ),
      body: EventForm(),
    );
  }
}
