import 'package:flutter/material.dart';
import 'package:good_vibes_app/interfaces/interfaces.dart';
import 'package:good_vibes_app/models/good_event.dart';
import 'package:good_vibes_app/services/services.dart';
import 'package:good_vibes_app/shared/shared.dart';
import 'package:provider/provider.dart';

class EventsScreen extends StatelessWidget {
  const EventsScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final events = Provider.of<List<GoodEvent>>(context);
    final types = Provider.of<List<GoodEventType>>(context);
    final currentLocation = Provider.of<ILocationable>(context);

    return Scaffold(
      body: GoodDefaultTabController(
        title: 'Eventos',
        coverImgUrl: 'assets/img/events.jpg',
        data: events,
        types: types,
        builder: (int index) => GoodEventList(
          data: events.where((event) => event.type == types[index].title).toList(),
          currentLocation: currentLocation,
        ),
      ),
      floatingActionButton: FloatingActionButton(
        heroTag: 'eventsFloatingActionButton',
        backgroundColor: Theme.of(context).primaryColor,
        child: Icon(
          Icons.add,
          size: 32,
          color: Colors.white,
        ),
        onPressed: () {
          if (AuthService.isUserRegistered(context)) {
            Navigator.of(context).pushNamed('/new');
          }
        },
      ),
    );

    return Scaffold(
      body: CustomScrollView(
        slivers: <Widget>[
          SliverPersistentHeader(
            pinned: true,
            delegate: GoodCategoryHeaderDelegate(
              title: 'Eventos',
              coverImgUrl: 'assets/img/events.jpg',
              collapsedHeight: kToolbarHeight,
              expandedHeight: 250,
              paddingTop: MediaQuery.of(context).padding.top,
              context: context,
            ),
          ),
          SliverList(
            delegate: SliverChildListDelegate([
              SizedBox(height: 16),
              Container(
                margin: EdgeInsets.zero,
                color: Colors.white,
                child: Container(
                  padding: EdgeInsets.all(16),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        'Explore os arredores',
                        style: TextStyle(
                          fontSize: 24,
                          fontWeight: FontWeight.w900,
                        ),
                      ),
                      SizedBox(height: 16),
                      GoodMapNearbyCard(
                        currentLocation: currentLocation,
                        iLocationables: events,
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(height: 16),
              GoodEventCardSection(events: events),
            ]),
          )
        ],
      ),
      floatingActionButton: FloatingActionButton(
        heroTag: 'eventsFloatingActionButton',
        backgroundColor: Theme.of(context).primaryColor,
        child: Icon(
          Icons.add,
          size: 32,
          color: Colors.white,
        ),
        onPressed: () {
          if (AuthService.isUserRegistered(context)) {
            Navigator.of(context).pushNamed('/new');
          }
        },
      ),
    );
  }
}
