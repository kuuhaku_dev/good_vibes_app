import 'package:flutter/material.dart';
import 'package:good_vibes_app/models/models.dart';
import 'package:good_vibes_app/services/services.dart';
import 'package:good_vibes_app/shared/shared.dart';

class EventEditScreen extends StatelessWidget {
  const EventEditScreen({Key key, @required this.event}) : super(key: key);
  final GoodEvent event;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Editar evento'),
        actions: <Widget>[
          IconButton(
              icon: Icon(Icons.delete_forever),
              onPressed: () async {
                final path = [GoodEvent.COL_PATH, event.uid].join('/');
                final doc = Document<GoodEvent>(path: path);
                final result =
                    await NotificationService.confirm(context, 'Apagar evento', 'Deseja realmente apagar este evento?');
                if (result == true) {
                  doc.delete();
                  Navigator.of(context).pop();
                  NotificationService.showSnackBar('Evento apagado');
                }
              })
        ],
      ),
      body: EventForm(
        event: event,
      ),
    );
  }
}
