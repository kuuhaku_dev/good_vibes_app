import 'package:flutter/material.dart';
import 'package:good_vibes_app/models/models.dart';
import 'package:good_vibes_app/shared/shared.dart';

class EventScreen extends StatefulWidget {
  const EventScreen({Key key, @required this.event}) : super(key: key);
  final GoodEvent event;

  @override
  _EventScreenState createState() => _EventScreenState();
}

class _EventScreenState extends State<EventScreen> {
  ScrollController _scrollController;

  final generalSliverKey = new GlobalKey();
  final photosSliverKey = new GlobalKey();

  @override
  void initState() {
    super.initState();
    _scrollController = ScrollController();
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final event = widget.event;
    return Scaffold(
      backgroundColor: Colors.white,
      body: CustomScrollView(
        controller: _scrollController,
        slivers: <Widget>[
          SliverPersistentHeader(
            pinned: true,
            delegate: ItemHeaderDelegate(
              title: event.title ?? 'Evento',
              collapsedHeight: kToolbarHeight,
              expandedHeight: 300,
              paddingTop: MediaQuery.of(context).padding.top,
              coverImgUrl: event.heroImage,
              context: context,
              iImageable: event,
            ),
          ),
          SliverPersistentHeader(
            pinned: true,
            delegate: ItemHeaderTabDelegate(onTap: (int index) {
              BuildContext contextToShow;
              switch (index) {
                case 0:
                  contextToShow = generalSliverKey.currentContext;
                  break;
                case 1:
                  contextToShow = photosSliverKey.currentContext;
                  break;
                default:
                  contextToShow = generalSliverKey.currentContext;
                  break;
              }
              Scrollable.ensureVisible(contextToShow, duration: kTabScrollDuration, curve: Curves.ease);
            }),
          ),
          GoodItemGeneralSliverList(
            tabKey: generalSliverKey,
            iDescriptionable: event,
            iInformationable: event,
            iLocationable: event,
          ),
          GoodItemPhotosSliverList(tabKey: photosSliverKey, iImageable: event),
        ],
      ),
    );
  }
}
