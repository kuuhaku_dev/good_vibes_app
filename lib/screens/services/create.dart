import 'package:flutter/material.dart';
import 'package:good_vibes_app/shared/shared.dart';

class ServiceCreateScreen extends StatelessWidget {
  const ServiceCreateScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Novo Serviço'),
      ),
      body: ServiceForm(),
    );
  }
}
