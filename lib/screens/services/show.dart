import 'package:flutter/material.dart';
import 'package:good_vibes_app/models/models.dart';
import 'package:good_vibes_app/shared/shared.dart';

class ServiceScreen extends StatefulWidget {
  ServiceScreen({Key key, @required this.service}) : super(key: key);
  final GoodService service;

  @override
  _ServiceScreenState createState() => _ServiceScreenState();
}

class _ServiceScreenState extends State<ServiceScreen> {
  ScrollController _scrollController;

  final generalSliverKey = new GlobalKey();
  final photosSliverKey = new GlobalKey();
  final reviewsSliverKey = new GlobalKey();

  @override
  void initState() {
    super.initState();
    _scrollController = ScrollController();
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final service = widget.service;
    return Scaffold(
      backgroundColor: Colors.white,
      body: CustomScrollView(
        controller: _scrollController,
        slivers: <Widget>[
          SliverPersistentHeader(
            pinned: true,
            delegate: ItemHeaderDelegate(
              title: service?.title ?? 'Loja',
              collapsedHeight: kToolbarHeight,
              expandedHeight: 300,
              paddingTop: MediaQuery.of(context).padding.top,
              coverImgUrl: service.heroImage,
              context: context,
              iImageable: service,
            ),
          ),
          SliverPersistentHeader(
            pinned: true,
            delegate: ItemHeaderTabDelegate(
                hasReviews: true,
                onTap: (int index) {
                  BuildContext contextToShow;
                  switch (index) {
                    case 0:
                      contextToShow = generalSliverKey.currentContext;
                      break;
                    case 1:
                      contextToShow = photosSliverKey.currentContext;
                      break;
                    case 2:
                      contextToShow = reviewsSliverKey.currentContext;
                      break;
                    default:
                      contextToShow = generalSliverKey.currentContext;
                      break;
                  }
                  Scrollable.ensureVisible(contextToShow, duration: kTabScrollDuration, curve: Curves.ease);
                }),
          ),
          GoodItemGeneralSliverList(
            tabKey: generalSliverKey,
            iDescriptionable: service,
            iInformationable: service,
            iLocationable: service,
          ),
          GoodItemPhotosSliverList(tabKey: photosSliverKey, iImageable: service),
          GoodItemReviewsSliverList(tabKey: reviewsSliverKey, iReviewable: service, service: service),
        ],
      ),
    );
  }
}
