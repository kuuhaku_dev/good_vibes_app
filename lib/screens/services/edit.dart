import 'package:flutter/material.dart';
import 'package:good_vibes_app/models/models.dart';
import 'package:good_vibes_app/services/services.dart';
import 'package:good_vibes_app/shared/shared.dart';

class ServiceEditScreen extends StatelessWidget {
  const ServiceEditScreen({Key key, @required this.service}) : super(key: key);
  final GoodService service;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Editar serviço'),
        actions: <Widget>[
          IconButton(
              icon: Icon(Icons.delete_forever),
              onPressed: () async {
                final path = [GoodService.COL_PATH, service.uid].join('/');
                final doc = Document<GoodService>(path: path);
                final result =
                    await NotificationService.confirm(context, 'Apagar serviço', 'Deseja realmente apagar este serviço?');
                if (result == true) {
                  doc.delete();
                  Navigator.of(context).pop();
                  NotificationService.showSnackBar('Serviço apagado');
                }
              })
        ],
      ),
      body: ServiceForm(
        service: service,
      ),
    );
  }
}
