import 'package:flutter/material.dart';
import 'package:good_vibes_app/models/models.dart';
import 'package:good_vibes_app/services/services.dart';
import 'package:good_vibes_app/shared/shared.dart';
import 'package:provider/provider.dart';

class ServicesScreen extends StatelessWidget {
  const ServicesScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final services = Provider.of<List<GoodService>>(context);
    final serviceTypes = Provider.of<List<GoodServiceType>>(context);

    return Scaffold(
      body: GoodDefaultTabController(
        title: 'Serviços',
        coverImgUrl: 'assets/img/stores.png',
        data: services,
        types: serviceTypes,
        builder: (int index) => GoodServiceList(
          services: services.where((service) => service.type == serviceTypes[index].title).toList(),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        heroTag: 'servicesFloatingActionButton',
        backgroundColor: Theme.of(context).primaryColor,
        child: Icon(
          Icons.add,
          size: 32,
          color: Colors.white,
        ),
        onPressed: () {
          if (AuthService.isUserRegistered(context)) {
            Navigator.of(context).pushNamed('/new');
          }
        },
      ),
    );
  }
}
