import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:good_vibes_app/models/models.dart';
import 'package:good_vibes_app/services/services.dart';
import 'package:good_vibes_app/shared/shared.dart';
import 'package:intl/intl.dart';
import 'package:url_launcher/url_launcher.dart';

class InfoScreen extends StatelessWidget {
  const InfoScreen({Key key, this.info, this.uid}) : super(key: key);
  final GoodInfo info;
  final String uid;

  @override
  Widget build(BuildContext context) {
    final Document<GoodInfo> infoRef = uid == null
        ? Document<GoodInfo>(path: '${GoodInfo.COL_PATH}/${info.uid}')
        : Document<GoodInfo>(path: '${GoodInfo.COL_PATH}/$uid');
    return Scaffold(
      appBar: AppBar(
        title: Text(info?.subtype ?? ''),
      ),
      // body:
      body: uid != null
          ? FutureBuilder(
              future: infoRef.getData(),
              builder: (BuildContext context, AsyncSnapshot<GoodInfo> snapshot) {
                if (snapshot.connectionState == ConnectionState.done) {
                  if (snapshot.hasError) {
                    return Center(child: Text('Ocorreu um erro'));
                  }

                  return buildSingleChildScrollView(context, snapshot.data);
                } else {
                  return Center(child: CircularProgressIndicator());
                }
              },
            )
          : buildSingleChildScrollView(context, info),
    );
  }

  SingleChildScrollView buildSingleChildScrollView(BuildContext context, GoodInfo info) {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(height: 24),
            Text(
              info.title,
              style: Theme.of(context).textTheme.headline,
            ),
            SizedBox(height: 16),
            Text(
              "Atualizado em: ${DateFormat.MMMMd().format(info.updatedAt)} às ${DateFormat.Hm().format(info.updatedAt)}",
              style: Theme.of(context).textTheme.caption,
            ),
            SizedBox(height: 16),
            Center(
              child: ClipRRect(
                borderRadius: BorderRadius.circular(16),
                child: GoodCachedImage(imageUrl: info.imageUrl, height: 200),
              ),
            ),
            SizedBox(height: 24),
            Html(
              data: info.content,
              onLinkTap: (url) => launch(url),
            ),
            SizedBox(height: 24),
            Divider(),
            GoodCommentList(
              iCommentable: info,
            )
          ],
        ),
      ),
    );
  }
}
