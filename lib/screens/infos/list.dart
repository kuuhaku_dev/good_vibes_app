import 'package:flutter/material.dart';
import 'package:good_vibes_app/models/models.dart';
import 'package:good_vibes_app/shared/shared.dart';
import 'package:provider/provider.dart';

class InfosScreen extends StatelessWidget {
  const InfosScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final infos = Provider.of<List<GoodInfo>>(context);
    final infoTypes = Provider.of<List<GoodInfoType>>(context);

    return Scaffold(
      body: GoodDefaultTabController(
        title: 'Portal',
        subtitle: 'Expansão da Consciência',
        coverImgUrl: 'assets/img/portal.jpg',
        showSubheader: true,
        data: infos,
        types: infoTypes,
        builder: (int index) => GoodInfoList(
          infos: infos.where((service) => service.type == infoTypes[index].title).toList(),
        ),
      ),
    );
  }
}
