import 'package:flutter/material.dart';
import 'package:intro_slider/dot_animation_enum.dart';
import 'package:intro_slider/intro_slider.dart';
import 'package:intro_slider/slide_object.dart';
import 'package:shared_preferences/shared_preferences.dart';

class OnboardingScreen extends StatefulWidget {
  OnboardingScreen({Key key, this.isFirstTime = false}) : super(key: key);
  final bool isFirstTime;

  _OnboardingScreenState createState() => _OnboardingScreenState();
}

class _OnboardingScreenState extends State<OnboardingScreen> {
  List<Slide> slides = new List();

  Function goToTab;

  _handleSeenOnboarding() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      prefs.setBool('seenOnboarding', true);
    });
  }

  @override
  void initState() {
    super.initState();

    const styleTitle = TextStyle(color: Color(0xFF4D34AB), fontSize: 30.0, fontWeight: FontWeight.bold);
    const styleDescription = TextStyle(fontSize: 20.0, color: Colors.black45);

    slides.add(
      new Slide(
        title: "Universo Quântico",
        styleTitle: styleTitle,
        description: "Seu Portal para expansão da consciência.",
        styleDescription: styleDescription,
        pathImage: 'assets/icon/icon-colored.png',
      ),
    );
    slides.add(
      new Slide(
        title: "Conteúdo",
        styleTitle: styleTitle,
        description:
            "Temos dicas de livros, profissionais, cursos, espaços, lojas, eventos e tudo mais que você precisará na sua jornada de expansão da consciência.",
        styleDescription: styleDescription,
        pathImage: 'assets/icon/icon-colored.png',
      ),
    );
    slides.add(
      new Slide(
        title: "Inspire-se",
        styleTitle: styleTitle,
        description: "\“A mente que se abre a uma nova ideia jamais voltará ao seu tamanho original\”. Albert Einstein",
        styleDescription: styleDescription,
        pathImage: 'assets/icon/icon-colored.png',
      ),
    );
  }

  void onDonePress() async {
    if (widget.isFirstTime) {
      await _handleSeenOnboarding();
      Navigator.of(context).pushNamed('registration/landing'); // Defined at landing.dart
    } else {
      Navigator.of(context).pop();
    }
  }

  void onTabChangeCompleted(index) {
    // Index of current tab is focused
  }

  Widget renderNextBtn() {
    return Icon(
      Icons.navigate_next,
      color: Color(0xFF4D34AB),
      size: 35.0,
    );
  }

  Widget renderDoneBtn() {
    return Icon(
      Icons.done,
      color: Color(0xFF4D34AB),
    );
  }

  Widget renderSkipBtn() {
    return Icon(
      Icons.skip_next,
      color: Color(0xFF4D34AB),
    );
  }

  List<Widget> renderListCustomTabs() {
    List<Widget> tabs = new List();
    for (int i = 0; i < slides.length; i++) {
      Slide currentSlide = slides[i];
      tabs.add(Container(
        width: double.infinity,
        height: double.infinity,
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 20.0, vertical: 60.0),
          child: ListView(
            children: <Widget>[
              GestureDetector(
                  child: Image.asset(
                currentSlide.pathImage,
                width: 200.0,
                height: 200.0,
                fit: BoxFit.contain,
              )),
              Container(
                child: Text(
                  currentSlide.title,
                  style: currentSlide.styleTitle,
                  textAlign: TextAlign.center,
                ),
                margin: EdgeInsets.only(top: 20.0),
              ),
              Container(
                child: Text(
                  currentSlide.description,
                  style: currentSlide.styleDescription,
                  textAlign: TextAlign.center,
                  maxLines: 5,
                  overflow: TextOverflow.ellipsis,
                ),
                margin: EdgeInsets.only(top: 20.0),
              ),
            ],
          ),
        ),
      ));
    }
    return tabs;
  }

  @override
  Widget build(BuildContext context) {
    return new IntroSlider(
      // List slides
      slides: this.slides,

      // Skip button
      renderSkipBtn: this.renderSkipBtn(),
      colorSkipBtn: Color(0x334D34AB),
      highlightColorSkipBtn: Color(0xFF4D34AB),

      // Next button
      renderNextBtn: this.renderNextBtn(),

      // Done button
      renderDoneBtn: this.renderDoneBtn(),
      onDonePress: this.onDonePress,
      colorDoneBtn: Color(0x334D34AB),
      highlightColorDoneBtn: Color(0xFF4D34AB),

      // Dot indicator
      colorDot: Color(0xFF4D34AB),
      sizeDot: 13.0,
      typeDotAnimation: dotSliderAnimation.SIZE_TRANSITION,

      // Tabs
      listCustomTabs: this.renderListCustomTabs(),
      backgroundColorAllSlides: Colors.white,
      refFuncGoToTab: (refFunc) {
        this.goToTab = refFunc;
      },

      // Show or hide status bar
      shouldHideStatusBar: true,

      // On tab change completed
      onTabChangeCompleted: this.onTabChangeCompleted,
    );
  }
}
