import 'package:flutter/material.dart';

import 'registration.dart';

class RegistrationNavigator extends StatelessWidget {
  const RegistrationNavigator({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Navigator(
      initialRoute: 'registration/splash',
      onGenerateRoute: (RouteSettings settings) {
        WidgetBuilder builder;
        switch (settings.name) {
          case 'registration/landing':
            builder = (_) => LandingScreen();
            break;
          case 'registration/onboarding':
            builder = (_) => OnboardingScreen(isFirstTime: true);
            break;
          case 'registration/signin':
            builder = (_) => SignInScreen();
            break;
          case 'registration/signup':
            builder = (_) => SignUpScreen();
            break;
          case 'registration/splash':
            builder = (_) => SplashScreen();
            break;
          default:
            builder = (_) => Scaffold(
                  body: Center(
                    child: Text('Invalid route: ${settings.name}'),
                  ),
                );
        }
        return MaterialPageRoute(builder: builder, settings: settings);
      },
    );
  }
}
