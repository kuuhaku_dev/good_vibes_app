import 'package:flutter/material.dart';
import 'package:good_vibes_app/services/services.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  AuthService _auth = AuthService();

  Future checkFirstSeen() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool _seen = (prefs.getBool('seenOnboarding') ?? false);

    if (_seen) {
      _auth.getUser.then((user) {
        if (user != null) {
          var route = ModalRoute.of(context);
          if ((route != null) && route.isActive) {
            NavigationService.instance.resetToRoot();
          }
        } else {
          Navigator.of(context).pushReplacementNamed('registration/landing');
        }
      });
    } else {
      Navigator.of(context).pushReplacementNamed('registration/onboarding');
    }
  }

  @override
  void initState() {
    super.initState();
    checkFirstSeen();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Color(0xFF000D46),
        child: Center(
          child: Text(
            "Carregando...",
            style: TextStyle(
              color: Colors.white,
              fontSize: 16,
            ),
          ),
        ),
      ),
    );
  }
}
