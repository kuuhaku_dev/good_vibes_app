import 'package:flutter/material.dart';
import 'package:good_vibes_app/services/services.dart';
import 'package:good_vibes_app/shared/shared.dart';

class LandingScreen extends StatelessWidget {
  const LandingScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Column(
          children: <Widget>[
            Expanded(
              flex: 3,
              child: Container(
                padding: EdgeInsets.all(16),
                decoration: BoxDecoration(
                  color: Color(0xFF000D46),
                  borderRadius: BorderRadius.vertical(
                    bottom: Radius.elliptical(
                      MediaQuery.of(context).size.width * 2,
                      MediaQuery.of(context).size.width * 0.25,
                    ),
                  ),
                ),
                child: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Image.asset(
                        'assets/icon/icon-colored.png',
                        height: MediaQuery.of(context).size.width * 0.4,
                      ),
                      SizedBox(height: 16),
                      Text(
                        'Universo Quântico',
                        style: Theme.of(context).textTheme.headline.copyWith(
                              color: Colors.white,
                              fontWeight: FontWeight.w900,
                            ),
                      ),
                      SizedBox(height: 16),
                      Text(
                        'Inserir aqui um texto explicativo ou uma frase de efeito',
                        textAlign: TextAlign.center,
                        style: Theme.of(context).textTheme.subhead.copyWith(
                              color: Colors.white,
                            ),
                      )
                    ],
                  ),
                ),
              ),
            ),
            Expanded(
              flex: 2,
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 32, vertical: 16),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    GoodOutlineButton(
                      title: 'Cadastrar',
                      onPressed: () => Navigator.of(context).pushNamed('registration/signup'),
                    ),
                    SizedBox(height: 32),
                    GoodOutlineButton(
                      title: 'Login',
                      onPressed: () => Navigator.of(context).pushNamed('registration/signin'),
                    ),
                  ],
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.only(bottom: MediaQuery.of(context).padding.bottom * 0.5),
              child: GestureDetector(
                onTap: () => NavigationService.instance.resetToRoot(),
                child: Container(
                  height: 60.0,
                  decoration: BoxDecoration(
                    border: Border(
                      top: BorderSide(),
                    ),
                  ),
                  child: Center(
                    child: Text('Irei me cadastrar mais tarde'),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
