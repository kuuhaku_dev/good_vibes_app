abstract class IDescriptionable {
  String title;
  String description;
  String address;
  DateTime startDate;
}
