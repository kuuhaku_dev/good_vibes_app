import 'package:google_maps_flutter/google_maps_flutter.dart';

abstract class ILocationable {
  String title;

  double latitude;
  double longitude;
  LatLng get position {
    if (latitude != null && longitude != null) {
      return LatLng(latitude, longitude);
    } else {
      return null;
    }
  }
}
