abstract class IInformationable {
  String url;
  String phone;
  String email;
  String address;
}
