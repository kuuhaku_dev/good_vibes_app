import 'package:good_vibes_app/models/models.dart';

abstract class ICommentable {
  String uid;
  List<GoodComment> last5Comments;
}
