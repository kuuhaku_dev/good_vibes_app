import 'package:good_vibes_app/models/models.dart';

const STATS_INITIAL_MAP = {
  'reviewsCount': 0,
  'totalRating': 0,
  'averageRating': 0,
  'star1': 0,
  'star2': 0,
  'star3': 0,
  'star4': 0,
  'star5': 0,
};

abstract class IReviewable {
  String uid;
  Map<String, dynamic> stats = STATS_INITIAL_MAP;
  List<GoodReviewSummary> last5Reviews;

  double get averageRating => (stats['averageRating']).toDouble();
  int get reviewsCount => (stats['reviewsCount'] ?? 1);
}
