abstract class ITypeable {
  static const String COL_PATH = 'categories';

  final CategoryType type;
  final String title;
  final dynamic position;

  static String typeAsString(CategoryType type) => type.toString().split('CategoryType.').last;

  ITypeable({this.type, this.title, this.position});
}

enum CategoryType {
  NEWS,
  SERVICE,
  EVENT,
  EMPTY,
}
