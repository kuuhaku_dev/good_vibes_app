import 'package:good_vibes_app/services/globals.dart';

abstract class IImageable {
  String heroImage;
  List<String> extraImages;

  List<String> get extraImagesUrls {
    return (extraImages ?? []).map((filepath) => '${Global.BASE_DOWNLOAD_URL}/$filepath').toList();
  }

  String get heroImageThumb {
    final filename = heroImage.split('/').last; // Is already full path
    final thumbFilename = Global.THUMB_1024 + filename;
    final thumbFilepath = heroImage.replaceAll(filename, thumbFilename);
    return thumbFilepath;
  }

  List<String> get extraImagesThumbs => (extraImages ?? []).map((filepath) {
        final filename = filepath.split('/').last;
        final thumbFilename = Global.THUMB_64 + filename;
        final thumbFilepath = filepath.replaceAll(filename, thumbFilename);
        return '${Global.BASE_DOWNLOAD_URL}/$thumbFilepath';
      }).toList();
}
