A expansão da consciência é uma longa e bela jornada que cada um deve percorrer, essa expansão nos reconecta com o nosso “Eu Superior”, com a Divindade em nós. Expandir a consciência é despertar para além do que os nossos cinco sentidos podem captar, é ver além desse plano físico, é entender que somos criadores da nossa própria realidade, é entender que somos protagonistas da nossa história.
O objetivo desse Portal é te auxiliar no seu processo de evolução apresentando ferramentas que você poderá utilizar no seu dia-a-dia:
ABA PORTAL
Teremos diversos conteúdos que o ajudarão na busca do autoconhecimento e na expansão da sua consciência. Nesta aba, teremos subtemas como: O Despertar, Terapias Energéticas/Quânticas, Livros, Manifestação, Meditação, Técnicas, Saúde e Inspirações.
ABA SERVIÇOS
Teremos diversas ferramentas que poderá utilizar na sua vida: Cursos, Terapeutas, Lojas, Espaços, Serviços, Vivências e Outros. Você poderá buscar os serviços com geolocalização, avaliar as ferramentas com pontuação e dar um feedback para os profissionais.
ABA EVENTOS
Teremos informações de quando e onde ocorrerão eventos relacionados a temática de expansão da consciência tais como: Congressos, Seminários, Workshops, Palestras, Retiros e Festivais.
ABA PERFIL
Você poderá gerenciar seu cadastro, ver eventos que você cadastrou, visualizar as mensagens de Boas Vindas no App, descobrir Quem Somos Nós e Apoiar-nos para uma versão livre de anúncios e para um alcance maior de pessoas.
Expandir a consciência vai te permitir mudar sua vida, viver no fluxo do bem-estar, da abundância, do amor e de alegria.
Espero que este App ajude a transformar sua visão de como o mundo funciona, mostrando o quanto você é mais poderoso do que sabia, inspirando-o a demonstrar o entendimento de que o que você pensa e no que você acredita tem um efeito profundo em sua vida. Desejo que você não seja mais a mesma pessoa que era quando começou.
