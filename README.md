# good_vibes_app

A new Flutter project.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

## Deploy Android

```bash
flutter build appbundle --target-platform android-arm,android-arm64,android-x64
cd android
fastlane production
```

## Deploy iOS

```bash
flutter build ios --release --no-codesign
cd ios
fastlane beta
```

## Check Android APK signing key

```bash
keytool -list -printcert -jarfile file.apk
```
